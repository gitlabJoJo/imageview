﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;

namespace dcmview.Classes
{
    public class ModelInfo //: INotifyPropertyChanged  //缺陷模型
    {
        public ModelInfo()
        {
            _ID = 0;
            _TypeName = null;
            //_Position_b = new Vector();
            //_Position_e = new Vector();
            _PicName = null;
            _Qthick = null;
            _Ping = null;
            _Qsize = null;
            _Qtype = 0;
           // _Qpos_b = new Vector();
            //_Qpos_e = new Vector();
            _InputA = null;
            _InputB = null;
            _Coordinates = null;
        }
        //public ModelInfo(int iD, string typeName, Vector position_b, Vector position_e, string picName, string qthick, string ping, string qsize)
        //{
        //    _ID = iD;
        //    _TypeName = typeName;
        //    _Position_b = position_b;
        //    _Position_e = position_e;
        //    _PicName = picName;
        //    _Qthick = qthick;
        //    _Ping = ping;
        //    _Qsize = qsize;
        //}
        private int _Qtype; //1直线，2矩形
        public int Qtype
        {
            get { return _Qtype; }
            set { _Qtype = value; }
        }

        //private Vector _Qpos_b;
        //public Vector Qpos_b
        //{
        //    get { return _Qpos_b; }
        //    set { _Qpos_b = value; }
        //}

        //private Vector _Qpos_e;
        //public Vector Qpos_e
        //{
        //    get { return _Qpos_e; }
        //    set { _Qpos_e = value; }
        //}

        private string _Qsize;
        public string Qsize
        {
            get { return _Qsize; }
            set { _Qsize = value; }
        }

        private string _TypeName;
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }
        //private Vector _Position_b;
        //public Vector Position_b
        //{
        //    get { return _Position_b; }
        //    set { _Position_b = value; }
        //}
        //private Vector _Position_e;
        //public Vector Position_e
        //{
        //    get { return _Position_e; }
        //    set { _Position_e = value; }
        //}

        private string _Qthick;
        public string Qthick
        {
            get { return _Qthick; }
            set { _Qthick = value; }
        }

        private string _PicName;
        public string PicName
        {
            get { return _PicName; }
            set { _PicName = value; }
        }
        private int _ID;
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private string _Ping;
        public string Ping
        {
            get { return _Ping; }
            set { _Ping = value; }
        }
        private string _InputA;
        public string InputA
        {
            get { return _InputA; }
            set { _InputA = value; }
        }
        private string _InputB;
        public string InputB
        {
            get { return _InputB; }
            set { _InputB = value; }
        }
        private string _Coordinates;
        public string Coordinates
        {
            get { return _Coordinates; }
            set { _Coordinates = value; }
        }
    }
}
