﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.IO;
using Microsoft.Win32;
using ImageView;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// information.xaml 的交互逻辑
    /// </summary>
    public partial class information : Window
    {
        string[] mess = new string[] { };
        public string[] stringarray = new string[90];
        bool issave = true;
        bool isload = false;
        bool isroot = false;
        //bool isexist= true;

        public information()
        {
            InitializeComponent();            
            LoadTxt(MainWindow.filepath);//尝试载入图片目录文件
            if(!isload)
            {
                LoadTxt(MainWindow.rootpath);//如果图片图片目录不存在信息文件，载入根目录的信息文件
                isroot = true;
            }            
            getstring(stringarray);
            if (isroot)
            {
                c14.Text = null; c15.Text = null; c16.Text = null; c17.Text = null;
            }
            if (string.IsNullOrEmpty(MainWindow.Wdengji) == false)
            {
                c14.Text = MainWindow.Wdengji;
                issave = false;
            }
            if (string.IsNullOrEmpty(MainWindow.Ddengji) == false)
            {
                c15.Text = MainWindow.Ddengji;
                issave = false;
            }
            if (string.IsNullOrEmpty(MainWindow.meangraytrans) == false)
            {
                c16.Text = MainWindow.meangraytrans;
                issave = false;
            }
            if (string.IsNullOrEmpty(MainWindow.snrvalue) == false)
            {
                c17.Text = MainWindow.snrvalue;
                issave = false;
            }            
            d14.Text = MainWindow.pixelsize.ToString();

            this.Closing += Window_Closing;
            
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)//保存提醒
        {
            if (issave == true) { e.Cancel = false; }
            else
            {
                if (MessageBox.Show("部分测量值尚未保存，是否确认关闭？", "确认", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }

            }
        }


        public void getarray()
        {
            string[] tmp = new string[]
            {
            "一、工程信息",
            "工程名称：" + a1.Text,
            "频段：" + a2.Text,
            "业主单位：" + a3.Text,
            "监理单位：" + a4.Text,
            "施工单位：" + a5.Text,
            "检测单位：" + a6.Text,
            "现场检测人员：" + a7.Text,
            "评片人员：" + a8.Text,
            "工程编号：" + a9.Text,
            "施工地点：" + a10.Text,
            "业主代表：" + a11.Text,
            "监理人员：" + a12.Text,
            "施工机组：" + a13.Text,
            "工艺编制人员：" + a14.Text,
            "工艺审核人员：" + a15.Text,
            "数据审核人员：" + a16.Text,
            "",
            "二、工件信息",
            "工件名称："+b1.Text,
            "工件1壁厚："+b2.Text+"mm",
            "工件1材质："+b3.Text,
            "工件规格："+b4.Text+"mm",
            "焊缝余高："+b5.Text+"mm",
            "焊接方式："+b6.Text,
            "检测部位："+b7.Text,
            "管道直径："+b8.Text+"mm",
            "工件2壁厚："+b9.Text+"mm",
            "工件2材质："+b10.Text,
            "焊缝长度："+b11.Text+"mm",
            "衬板厚度："+b12.Text+"mm",
            "坡口形式："+b13.Text,
            "检测时机："+b14.Text,
            "",
            "三、基本要求",
            "检测标准："+c1.Text,
            "单丝像质计型号："+c2.Text,
            "单丝像质计位置："+c3.Text,
            "要求单丝像质计丝号："+c4.Text,
            "要求灰度范围："+c5.Text,
            "合格级别："+c6.Text,
            "透照方式："+c7.Text,
            "双丝像质计型号："+c8.Text,
            "双丝像质计位置："+c9.Text,
            "要求双丝像质计丝号："+c10.Text,
            "要求归一化信噪比："+c11.Text,
            "图像格式："+c12.Text,
            "技术等级："+c13.Text,
            "实际单丝像质计丝号："+c14.Text,
            "实际双丝像质计丝号："+c15.Text,
            "实际灰度平均值："+c16.Text,
            "实际归一化信噪比："+c17.Text,
            "",
            "四、设备信息",
            "系统型号："+d1.Text,
            "焦点尺寸："+d2.Text+"×"+d3.Text+"mm",
            "软件版本："+d4.Text,
            "滤波板材质："+d5.Text,
            "探测器到工件外表面距离："+d6.Text+"mm",
            "探测器型号："+d7.Text,
            "射线源型号："+d8.Text,
            "工装类型："+d9.Text,
            "滤波板厚度："+d10.Text+"mm",
            "探测器编号："+d11.Text,
            "射线源编号："+d12.Text,
            "射线源类型："+d13.Text,
            "像素尺寸："+d14.Text+"um",
            "标记材料："+d15.Text,
            "",
            "五、工艺参数",
            "工艺规程编号："+e1.Text,
            "射线电压："+e2.Text+"KV",
            "焦距F："+e3.Text+"mm",
            "平板应用模式："+e4.Text,
            "平板增益："+e5.Text,
            "图像分辨率："+e6.Text+"×"+e7.Text,
            "自动采集次数："+e8.Text+"次",
            "射线管车步长系数："+e16.Text,
            //"采集前丢弃帧数："+e17.Text+"张",
            "探测器长度："+e9.Text+"mm",
            "射线电流："+e10.Text+"mA",
            "采集帧数："+e11.Text+"张",
            "一张透照长度："+e12.Text+"mm",
            "搭接长度："+e13.Text+"mm",
            "单帧时间："+e14.Text+"ms",
            "平板车步长系数："+e15.Text,
            "图像顺时针旋转角度："+e17.Text+"度",
            "探测器类型："+e18.Text,
            "焊口编号："+e19.Text,
            //"采集后丢弃帧数："+e18.Text+"张",
        };            
            mess = tmp;
        }
        public void getstring(string[] tmp)
        {
            try
            {
                a1.Text = tmp[0];
                a2.Text = tmp[1];
                a3.Text = tmp[2];
                a4.Text = tmp[3];
                a5.Text = tmp[4];
                a6.Text = tmp[5];
                a7.Text = tmp[6];
                a8.Text = tmp[7];
                a9.Text = tmp[8];
                a10.Text = tmp[9];
                a11.Text = tmp[10];
                a12.Text = tmp[11];
                a13.Text = tmp[12];
                a14.Text = tmp[13];
                a15.Text = tmp[14];
                a16.Text = tmp[15];

                b1.Text = tmp[16];
                b2.Text = tmp[17].Substring(0, tmp[17].LastIndexOf("mm"));
                b3.Text = tmp[18];
                b4.Text = tmp[19].Substring(0, tmp[19].LastIndexOf("mm"));
                b5.Text = tmp[20].Substring(0, tmp[20].LastIndexOf("mm"));
                b6.Text = tmp[21];
                b7.Text = tmp[22];
                b8.Text = tmp[23].Substring(0, tmp[23].LastIndexOf("mm"));
                b9.Text = tmp[24].Substring(0, tmp[24].LastIndexOf("mm"));
                b10.Text = tmp[25];
                b11.Text = tmp[26].Substring(0, tmp[26].LastIndexOf("mm"));
                b12.Text = tmp[27].Substring(0, tmp[27].LastIndexOf("mm"));
                b13.Text = tmp[28];
                b14.Text = tmp[29];

                c1.Text = tmp[30];
                c2.Text = tmp[31];
                c3.Text = tmp[32];
                c4.Text = tmp[33];
                c5.Text = tmp[34];
                c6.Text = tmp[35];
                c7.Text = tmp[36];
                c8.Text = tmp[37];
                c9.Text = tmp[38];
                c10.Text = tmp[39];
                c11.Text = tmp[40];
                c12.Text = tmp[41];
                c13.Text = tmp[42];
                c14.Text = tmp[43];
                c15.Text = tmp[44];
                c16.Text = tmp[45];
                c17.Text = tmp[46];

                d1.Text = tmp[47];
                d2.Text = tmp[48].Substring(0, tmp[48].LastIndexOf("×"));
                d3.Text = tmp[48].Substring(tmp[48].LastIndexOf("×") + 1, tmp[48].LastIndexOf("mm") - tmp[48].LastIndexOf("×") - 1);
                d4.Text = tmp[49];
                d5.Text = tmp[50];
                d6.Text = tmp[51].Substring(0, tmp[51].LastIndexOf("mm"));
                d7.Text = tmp[52];
                d8.Text = tmp[53];
                d9.Text = tmp[54];
                d10.Text = tmp[55].Substring(0, tmp[55].LastIndexOf("mm"));
                d11.Text = tmp[56];
                d12.Text = tmp[57];
                d13.Text = tmp[58];
                d14.Text = tmp[59].Substring(0, tmp[59].LastIndexOf("um"));
                d15.Text = tmp[60];

                e1.Text = tmp[61];
                e2.Text = tmp[62].Substring(0, tmp[62].LastIndexOf("KV"));
                e3.Text = tmp[63].Substring(0, tmp[63].LastIndexOf("mm"));
                e4.Text = tmp[64];
                e5.Text = tmp[65];
                e6.Text = tmp[66].Substring(0, tmp[66].LastIndexOf("×"));
                e7.Text = tmp[66].Substring(tmp[66].LastIndexOf("×") + 1);
                e8.Text = tmp[67].Substring(0, tmp[67].LastIndexOf("次"));

                e16.Text = tmp[68];
                //e17.Text = tmp[69].Substring(0, tmp[69].LastIndexOf("张"));

                e9.Text = tmp[69].Substring(0, tmp[69].LastIndexOf("mm"));
                e10.Text = tmp[70].Substring(0, tmp[70].LastIndexOf("mA"));
                e11.Text = tmp[71].Substring(0, tmp[71].LastIndexOf("张"));
                e12.Text = tmp[72].Substring(0, tmp[72].LastIndexOf("mm"));
                e13.Text = tmp[73].Substring(0, tmp[73].LastIndexOf("mm"));
                e14.Text = tmp[74].Substring(0, tmp[74].LastIndexOf("ms"));
                e15.Text = tmp[75];              
                e17.Text = tmp[76];
                e18.Text = tmp[77];
                e19.Text = tmp[78];
                //e18.Text = tmp[77].Substring(0, tmp[77].LastIndexOf("张"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(this,  "数据文件缺失或损坏",ex.Message);
                return;
            }

        }


        private void save1_Click(object sender, RoutedEventArgs e)
        {
            ////getstring(stringarray);
            issave = true;
            getarray();
            MainWindow.Wdengji = null;
            MainWindow.Ddengji = null;
            MainWindow.meangraytrans = null;
            MainWindow.snrvalue = null;
            ////using (StreamWriter writer = new StreamWriter(@"C:\Users\ZX\Desktop\工艺信息.txt"))//保存目录
            //using (StreamWriter writer = new StreamWriter("工艺信息.txt"))//保存目录
            //{
            //    foreach (string s in mess)
            //    {
            //        writer.WriteLine(s);
            //    }
            //    writer.Close();
            //}
            createtxt(MainWindow.filepath);
            createtxt(MainWindow.rootpath);

            if(string.IsNullOrEmpty(d14.Text))
            {
                MainWindow.pixelsize = 120;
            }
            else
            {
                MainWindow.pixelsize = double.Parse(d14.Text);
            }

            MessageBox.Show("已保存！");

        }
        private void createtxt(string a)
        {
            if (!File.Exists(a + "\\工艺信息.txt"))
            {
                FileStream fs1 = new FileStream(a + "\\工艺信息.txt", FileMode.Create, FileAccess.ReadWrite);//创建写入文件 
                StreamWriter sw = new StreamWriter(fs1,Encoding.UTF8);
                foreach (string s in mess)
                {
                    sw.WriteLine(s);
                }
                
                //sw.WriteLine("1");
                sw.Close();
                fs1.Close();
                fs1.Dispose();
            }
            else
            {
                FileStream fs2 = new FileStream(a + "\\工艺信息.txt", FileMode.Open, FileAccess.ReadWrite);//打开已有文件
                StreamWriter sw2 = new StreamWriter(fs2, Encoding.UTF8);
                foreach (string s in mess)
                {
                    sw2.WriteLine(s);
                }
                //sw.WriteLine("1");
                sw2.Close();
                fs2.Close();
                fs2.Dispose();
            }
        }


        public void LoadTxt(string originpath)
        {
            string a;
            if (!MainWindow.DefectView)
            {
                a = originpath.Trim() + "\\";
            }
            else
            {
                a = originpath.Substring(0, originpath.LastIndexOf("评定图像")) + "\\";
            }
            List<string> txt = new List<string>();
            if (!File.Exists(a + "工艺信息.txt"))
            {
                //FileStream fs1 = new FileStream(a + "StateTxt.txt", FileMode.Create, FileAccess.ReadWrite);//创建写入文件 
                //StreamWriter sw = new StreamWriter(fs1);
                //sw.WriteLine("0");//开始写入值
                //sw.WriteLine("0");
                //sw.WriteLine("0");
                //sw.WriteLine("0");
                //sw.WriteLine("0");
                ////sw.WriteLine("1");
                //sw.Close();
                //fs1.Close();
                //fs1.Dispose();
                //MessageBox.Show("当前没有已保存的工艺信息");
                return;
            }
            else
            {
                try
                {
                    //FileStream fs = new FileStream(, FileMode.Open, FileAccess.ReadWrite);
                    FileStream fs2 = new FileStream(a + "工艺信息.txt", FileMode.Open, FileAccess.ReadWrite);
                    using (StreamReader sr = new StreamReader(fs2, Encoding.UTF8))
                    {
                        {
                            int lineCount = 0;
                            while (sr.Peek() > 0)
                            {
                                lineCount++;
                                string tmp = sr.ReadLine();
                                txt.Add(tmp);
                                //MessageBox.Show(tmp);
                            }

                        }
                    }

                    int i = 0;
                    foreach (string b in txt)
                    {
                        if (b.IndexOf("：") != -1)
                        {
                            //stringarray[i] = a.Substring(a.LastIndexOf(":") + 1, a.Length - a.LastIndexOf(":") - 1);
                            stringarray[i] = b.Substring(b.LastIndexOf("：") + 1);

                            i++;
                        }

                    }
                    isload = true;
                }
                catch (Exception )
                {
                    //MessageBox.Show(this, "数据文件已损坏",ex.Message);
                    return;
                } 
                //MessageBox.Show(stringarray[66].ToString() +" "+ stringarray[73].ToString());
                //MessageBox.Show(stringarray[43].ToString() + stringarray[44].ToString() + stringarray[45].ToString() + stringarray[46].ToString());        
            }
        }

        //读取文件
        private void read_Click(object sender, RoutedEventArgs e)
        {

            List<string> txt = new List<string>();
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "文本文件(*.txt)|*.txt|(*.rtf)|*.rtf";
            if (openFile.ShowDialog() == true)
            {
                using (StreamReader sr = new StreamReader(openFile.FileName, Encoding.UTF8))
                {
                    {
                        int lineCount = 0;
                        while (sr.Peek() > 0)
                        {
                            lineCount++;
                            string tmp = sr.ReadLine();
                            txt.Add(tmp);
                        }
                    }
                }

                int i = 0;
                foreach (string a in txt)
                {
                    if (a.IndexOf("：") != -1)
                    {
                        //stringarray[i] = a.Substring(a.LastIndexOf(":") + 1, a.Length - a.LastIndexOf(":") - 1);
                        stringarray[i] = a.Substring(a.LastIndexOf("：") + 1);
                        i++;
                    }
                }
                getstring(stringarray);
            }

        }


    }
}
