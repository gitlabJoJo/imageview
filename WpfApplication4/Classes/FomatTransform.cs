﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media.Imaging;
using Emgu.CV.Structure;

namespace dcmview.Classes
{
    class FomatTransform//用于各类图像格式转换，请将相关方法集成到此类中
    {
        [DllImport("gdi32.dll", SetLastError = true)]
        private static extern bool DeleteObject(IntPtr hObject);//用于转换图片格式
        public BitmapSource ImageToBitmapSource(Emgu.CV.Image<Gray, byte> image)
        {
            System.Drawing.Bitmap source = image.Bitmap;
            IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap
            BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ptr, IntPtr.Zero, Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(ptr); //release the HBitmap
            return bs;
        }
        public BitmapSource ImageToBitmapSource(Emgu.CV.Image<Gray, ushort> image)
        {
            System.Drawing.Bitmap source = image.Bitmap;
            IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap
            BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ptr, IntPtr.Zero, Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(ptr); //release the HBitmap
            return bs;
        }
        public BitmapSource ImageToBitmapSource(Emgu.CV.Image<Bgr, byte> image)
        {
            System.Drawing.Bitmap source = image.Bitmap;
            IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap
            BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ptr, IntPtr.Zero, Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(ptr); //release the HBitmap
            return bs;
        }
        public BitmapSource BitmapToBitmapSource(System.Drawing.Bitmap bitMap)
        {
            //System.Drawing.Bitmap newBitMap = CaptureBitmapFormBitMap(bitMap, new System.Drawing.Rectangle(0, 0, bitMap.Width, bitMap.Height));
            IntPtr ip = bitMap.GetHbitmap();
            BitmapSource bitmapSource = null;
            bitmapSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                ip, IntPtr.Zero, Int32Rect.Empty,
                 System.Windows.Media.Imaging.BitmapSizeOptions.FromWidthAndHeight(bitMap.Width, bitMap.Height));
            DeleteObject(ip);
            return bitmapSource;
        }
        public BitmapSource arrytobitmapsource(double[] imgs, int wi, int hi, int maxi, int mini)//显示提取的双丝图像
        {
            //double maxi = imgs.Max();
            //double mini = imgs.Min();
            Bitmap bitmap = new Bitmap(wi, hi, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            BitmapData bmpData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, wi, hi),
                 ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            int offset = bmpData.Stride - bmpData.Width;        // 计算每行未用空间字节数
            IntPtr ptr = bmpData.Scan0;                         // 获取首地址
            int scanBytes = bmpData.Stride * bmpData.Height;    // 图像字节数 = 扫描字节数 * 高度
            byte[] grayValues = new byte[scanBytes];            // 为图像数据分配内存
            int posSrc = 0, posScan = 0;                        // rawValues和grayValues的索引
            for (int i = 0; i < hi; i++)
            {
                for (int j = 0; j < wi; j++)
                {
                    byte imgt = (byte)((imgs[posSrc++] - mini) / (maxi - mini) * 255);
                    grayValues[posScan++] = imgt;
                }
                // 跳过图像数据每行未用空间的字节，length = stride - width * bytePerPixel
                posScan += offset;
            }
            // 内存解锁
            Marshal.Copy(grayValues, 0, ptr, scanBytes);
            bitmap.UnlockBits(bmpData);  // 解锁内存区域

            // 修改生成位图的索引表，从伪彩修改为灰度
            ColorPalette palette;
            // 获取一个Format8bppIndexed格式图像的Palette对象
            using (Bitmap bmp = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format8bppIndexed))
            {
                palette = bmp.Palette;
            }
            for (int i = 0; i < 256; i++)
            {
                palette.Entries[i] = System.Drawing.Color.FromArgb(i, i, i);
            }
            // 修改生成位图的索引表
            bitmap.Palette = palette;
            BitmapSource bitS = BitmapToBitmapSource(bitmap);
            return bitS;
        }
    }
}
