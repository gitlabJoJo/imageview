﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using Microsoft.Win32;
using System.Threading;
using System.Diagnostics;
using Dicom;
using Dicom.Imaging;
using System.Drawing.Imaging;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using System.Runtime.InteropServices;
using ImageView;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// chart.xaml 的交互逻辑
    /// </summary>
    public partial class chart : Window
    {
        int shuangsi;
        public chart()
        {
            InitializeComponent();
        }
        public void InitChart(double[] datac, double[] ratec, int left, int right)
        {
            //定义图表区域
            chartAttMed.ChartAreas.Clear();
            ChartArea chartArea1 = new ChartArea("C1");
            chartAttMed.ChartAreas.Add(chartArea1);
            string a=null;
            for (int i = 1; i < ratec.Length; i++)
            {
                shuangsi = i;
                shuangsilevel.Text = "D" + i.ToString();
                a = a + ("D" + i.ToString() + ": " + ratec[i-1].ToString() + "%    ");
                if (ratec[i-1] < 20) break;
                //if (ratec[i] != 0)
                //{
                //   shuangsi = j;
                //   shuangsilevel.Text="D"+j.ToString();
                //   a=a+("D"+j.ToString()+": "+ratec[i].ToString()+"%    ");
                //   j++;                   
                //}
            }
            chartAttMed.Titles.Add(a);

            //定义存储和显示点的容器
            chartAttMed.Series.Clear();
            Series series1 = new Series("pixelvalue");
            series1.ChartArea = "C1";
            series1.YValueType = ChartValueType.Int32;
            series1.XValueType = ChartValueType.Int32;


            double[] cut = new double[right - left + 1];
            for (int j = left; j <= right; j++)
            {
                cut[j - left] = datac[j];
            }
            //设置图表显示样式
            series1.ChartType = SeriesChartType.Line;            
            chartAttMed.ChartAreas[0].AxisX.Minimum = left;
            chartAttMed.ChartAreas[0].AxisX.Maximum = right;
            chartAttMed.ChartAreas[0].AxisX.Title = "像素数";
            chartAttMed.ChartAreas[0].AxisY.Minimum = cut.Min();
            chartAttMed.ChartAreas[0].AxisY.Maximum = cut.Max();
            chartAttMed.ChartAreas[0].AxisY.Title = "像素值";

            //chartAttMed.Series[0].ChartType = SeriesChartType.Line;
            //chartAttMed.ChartAreas[0].AxisY.Interval = 1;
            chartAttMed.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartAttMed.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            //设置标题




            series1.Color = System.Drawing.Color.Red;
            for (int i = 0; i < cut.Length - 1; i++)
            {
                series1.Points.AddXY((double)(i + left), cut[i]);
            }
            chartAttMed.Series.Add(series1);                       
      
        }       
        public void showshuangsi(BitmapSource a)
        {
            shuangsicut.Source = a;
            length.Text = a.PixelWidth.ToString();
            width.Text = a.PixelHeight.ToString();
        }

        private void confirm_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(dansilevel.Text.ToString()) == true) { MessageBox.Show("请补充单丝信息"); return; }
            double[] fenb = { 800, 630, 500, 400, 320, 250, 200, 160, 130, 100, 80, 63, 50 };
            MainWindow.Ddengji = shuangsilevel.Text; 
            MainWindow.Wdengji = dansilevel.Text;
            switch (MainWindow.Ddengji)
            {
                case "D1":
                    MainWindow.fenbianlv = fenb[0];
                    break;
                case "D2":
                    MainWindow.fenbianlv = fenb[1];
                    break;
                case "D3":
                    MainWindow.fenbianlv = fenb[2];
                    break;
                case "D4":
                    MainWindow.fenbianlv = fenb[3];
                    break;
                case "D5":
                    MainWindow.fenbianlv = fenb[4];
                    break;
                case "D6":
                    MainWindow.fenbianlv = fenb[5];
                    break;
                case "D7":
                    MainWindow.fenbianlv = fenb[6];
                    break;
                case "D8":
                    MainWindow.fenbianlv = fenb[7];
                    break;
                case "D9":
                    MainWindow.fenbianlv = fenb[8];
                    break;
                case "D10":
                    MainWindow.fenbianlv = fenb[9];
                    break;
                case "D11":
                    MainWindow.fenbianlv = fenb[10];
                    break;
                case "D12":
                    MainWindow.fenbianlv = fenb[11];
                    break;
                case "D13":
                    MainWindow.fenbianlv = fenb[12];
                    break;
                default:
                    break;
            }

            MessageBox.Show("保存成功！");
            this.Close();
           
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }


    }
}
