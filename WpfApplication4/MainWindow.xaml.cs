﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Drawing;
using Microsoft.Win32;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Diagnostics;
using Dicom;
using Dicom.Imaging;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using Emgu.CV.CvEnum;
using System.ComponentModel;
using Aladdin.HASP;
using System.Windows.Threading;
using Dicom.Imaging.Render;
using System.Text.RegularExpressions;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using dcmview;
using dcmview.AssistWindows;
using dcmview.Classes;
using System.Threading.Tasks;

namespace ImageView
{
    public class MyDataObject
    {
        //public int Number { get; set; }
        public Image<Gray,byte> photo { get; set; }//缩略图中的元素
        public int Index { get; set; }
    }
    public class bitmapObject
    {
        //public int Number { get; set; }
        public BitmapSource photo { get; set; }//缩略图中的元素
        public int Index { get; set; }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region 全局变量
        ImgProcess imgProcess = new ImgProcess();//用于实现数字图像处理相关方法，，若有新需求请在该类中添加函数！！
        ImgQualityEvaluation imgQualityEvaluation = new ImgQualityEvaluation();//用于评定图像质量，包括双丝和峰值信噪比以及直方图计算，若有新需求请在该类中添加函数！！
        Encryption encryption = new Encryption();//加密方法类
        Draw drawInstance = new Draw();//用于执行所有的画图操作，若有新需求请在该类中添加函数！！
        FomatTransform imgFormatTransform = new FomatTransform();//用于执行图像格式转换，，若有新需求请在该类中添加函数！！
        List<MyDataObject> imgList;//以下两个链表用来解决多线程显示预览图的问题，因为bitmapsource不能跨线程传值
        List<bitmapObject> bitList;
        DispatcherTimer dispatcherTimer = new DispatcherTimer();//用于定时验证加密狗
        private static ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //双丝和厚度相关变量
        public static double fenbianlv, help, pixelsize = 120, thickns;//fenbianlv暂时不用到，help用于校准，pixelsize用于计算信噪比，仅可在图像信息中修改。最后一项为测量的厚度       
        public static string Ddengji, Wdengji, snrvalue, excelPath, meangray, meangraytrans, filepath, rootpath;//D、W：从子窗口获取图像信息；s、m：局部信噪比和整体灰度均值；f、r：图片文件夹路径和软件文件夹路径
        public static int hi1 = 21;//双丝截取宽度默认值
        public static bool shuangsiCountinue = true, thicknessContinue = true, isThicknessMeasured = false, pintuContinue = false;//用于在弹窗中确认是否继续，用于记录是否完成了厚度测量        

        //缺陷相关变量
        List<ModelInfo> models = new List<ModelInfo>(); //缺陷模型列表
        int dgidx;//缺陷序号
        private bool isQueadd;//判断是添加新缺陷还是修改已有缺陷
        bool modelchange;//用于提示是否保存缺陷变动

        //dcm和图像处理
        private DicomFile dcmFile;
        private DicomDataset dcmDataSet;
        private DicomImage dcmImage;//这三个dicom变量提供图像处理基本数据，除非从文件夹读取单张图片进行显示，否则不得更改
        public Image<Gray, ushort> imgGaryUshort;//基于原始dcm图像进行渲染后的未经处理的16位灰度图，应基于该图进行所有的数字图像处理
        List<string> dcmlocations = new List<string>();//用于对图片名称重新排序
        private double winwidth = 65536, wincenter = 32768;//窗宽窗位默认初始值

        //记录5种基本图像处理的状态
        public  bool isInverse = false;
        private bool isHSwitch = false;
        private bool isVSwitch = false;
        private bool isfudiao = false;
        private int rotate = 0;
        string s1="0", s2, s3, s4="0", s5;//将5种基本图像处理写入本地或从本地读取状态
        private List<string> picName = new List<string>();//用于拼图后寻找对应图片等操作       
        //private string listpicname;        
        private string pictureName;//当前打开的图片名称
        int n0 = 0;//用于拼图时的图片计数
        public static int cutSize = 0;//用于拼图时裁切黑边，以像素为单位
        
        private static readonly SolidColorBrush Red = System.Windows.Media.Brushes.Red;
        private static readonly System.Windows.Media.Brush or = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 127, 39));//注册用到的橘色
        private static readonly SolidColorBrush basecolor = System.Windows.Media.Brushes.Transparent;
        private static readonly SolidColorBrush selectcolor = new SolidColorBrush((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#506495ED"));

        public static Dicom.Imaging.Render.IPixelData iPixelData;//像素数据集，用于获取dcm像素值
        System.Windows.Point lastpoint, currentpoint, relativepoint, scrollpoint, savepoint;//定义相对于不同控件的初始点、实时点、终点坐标
        Line currentline = null;  //对角线线段
        Line vline = null;//双丝测量辅助用的短垂线
        Line Rectline1 = null, Rectline2 = null, Rectline3 = null, Rectline4 = null; //显示控件上构成矩形的四条线段
        TextBox CurrentText = null; //动态文本框        
        double BaseWinWidth;//记录显示控件初始的宽和高，用于计算放缩倍数
        double BaseWinHeight;

        bool showLine = false, showArea = false, winchange = false, pinjie = false;//显示模式标志变量，showline决定鼠标拖动时画线，showarea画框，winchange调窗宽窗位，pinjie为拼图模式
        double correct = 0.25, trans = 1;  //correct为校正系数，默认值0.25（测量值与help有关），用于校准图上距离与实际距离；trans为放缩系数，为当前图像尺寸与原图尺寸比值
        BitmapSource bitSourse, BaseBps;//用于直接显示和还原

        ListBoxItem s;//改变缩略图选中文件的背景色
        byte[] Data;//用于局部放大
        Line xline = new Line();//用于在局部放大图中画红色准心
        Line yline = new Line();
        System.Windows.Point ImgLastPoint;

        bool rec1 = false, rec2 = false, rec3 = false;//矩形标尺控制变量，对应三种规格的评定框   

        Line HRulerLine_Axis = null;//水平标尺上的横轴线段
        Line[] HRulerLine_Ticks = null;//水平标尺上的101根刻度线段
        TextBlock[] HTickNum = null;// 水平标尺上的整十刻度数值

        Line VRulerLine_Axis = null;//纵向标尺上的纵轴线段
        Line[] VRulerLine_Ticks = null;//纵向标尺上的101根刻度线段
        TextBlock[] VTickNum = null;// 纵向标尺上的整十刻度数值


        Line Rectline5 = null, Rectline6 = null, Rectline7 = null, Rectline8 = null; //构成矩形标尺的四条线段
        //Line Rectline9 = null, Rectline10 = null, Rectline11 = null, Rectline12 = null; //缺陷矩形的四条线段
        Line defectline = null;//单独的缺陷线段
        double ra = 10, rb = 20, rc = 30;//三种规格的评定框宽度
        int z = 5;//鼠标与标尺间隙
        double u = 0, t = 0;//矩形标尺边长

        bool round1 = false;//圆形标尺和不同的直径，以及提示文本框
        System.Windows.Shapes.Ellipse e1 = null, e2 = null, e3 = null, e4 = null, e6 = null, e8 = null, e03 = null, e04 = null, e05 = null, e06 = null, e07 = null;
        double r1 = 1, r2 = 2, r3 = 3, r4 = 4, r6 = 6, r8 = 5, r05 = 0.5, r06 = 0.6, r07 = 0.7;
        TextBox DValue = null;

        //用于记录图像信噪比测量值
        public int RegionWidth;
        public int RegionHeight;
        public double GrayAverage;
        public double GrayStandardization;
        public double NormalizedSNRN;

        //缺陷数据保存和绘制
        public static string QueSize, Thick, Ping, Picname, Qtype, InputA, InputB, coords;//用于给缺陷model类的对象赋值
        int LineorRec;//判断缺陷类型
        bool MarkSwitch = false, DefectShow = false;//标记缺陷模式和缺陷数据库浏览模式的标志变量
        public static bool DefectView = false;//缺陷数据库，表示打开的是缺陷图片文件夹
        public static bool issave = true;//用于在填写缺陷信息界面决定是否继续存储
        int defectX1, defectX2, defectY1, defectY2, Parameter, qlength, qwidth, px1, px2, py1, py2, classflag, NumofDefs = 0;
        //d1~d4用于记录在原图上的缺陷位置，p、ql、qw用于记录面积，长度，宽度；p1~p4用于用于记录拼图后的图上缺陷位置；
        //classflag用于记录和读写缺陷类型，N为当前图像上的缺陷个数，用于确定坐标数组写入和读取的起始位置
        int[] defcoords = new int[240];
        //原图缺陷坐标数组，8位记录一个缺陷的全部信息，0~3记录坐标，4~6为面积，长，宽，7为类型标志位，此处一张图片最多可记录30处
        int[] pinjieshow = new int[240];
        //拼图缺陷坐标数组，规则同上（设置该数组的原因是，省去大量转换的步骤）
        string[] coord;//用于读写存储的坐标信息
        bool drawNeeded=false;
        List<DefectInfo> defectInfo=new List<DefectInfo>();
        Image<Gray, byte> pintu;//用于拼图

        //撤销
        string CancelType = null;//确定撤销种类
        List<string> CancelKey = new List<string>();
        //Image<Gray, byte> GrayCancelImg = null;//记录撤销前的图像
        //List<Image<Gray, byte>> CancelImgList = new List<Image<Gray, byte>>();//用于存放撤销图片
        public static string SaveWords = null;        
        List<string> SaveTexts = new List<string>();

        List<System.Drawing.Point> circenter = new List<System.Drawing.Point>();
        List<double> cirrad = new List<double>();

        public Image<Bgr, byte> imgbgrP = null;   //
        public bool bcifProcessFlag = false;
        public bool firstLoadFlag = false;
        public bool ImgProcessFlag = false;
        private int ImageEnhanceNum = 0;

        #endregion
        #region//按键图像绑定参数
        //thickMeasure
        public bool IsTm
        {
            get
            {
                return isTm;
            }
            set
            {
                isTm = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsTm"));
                }
            }
        }
        private bool isTm = false;
        //twinwire
        public bool IsTw
        {
            get
            {
                return isTw;
            }
            set
            {
                isTw = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsTw"));
                }
            }
        }
        private bool isTw = false;
        //GrayMeasure
        public bool IsGm
        {
            get
            {
                return isGm;
            }
            set
            {
                isGm = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsGm"));
                }
            }
        }
        private bool isGm = false;
        //GrayStatic
        public bool IsGs
        {
            get
            {
                return isGs;
            }
            set
            {
                isGs = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsGs"));
                }
            }
        }
        private bool isGs = false;
        // SNR
        public bool IsSnr
        {
            get
            {
                return isSnr;
            }
            set
            {
                isSnr = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsSnr"));
                }
            }
        }
        private bool isSnr = false;

        public bool IsNg
        {
            get
            {
                return isNg;
            }
            set
            {
                isNg = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsNg"));
                }
            }
        }
        private bool isNg = false;

        public int NgSt
        {
            get
            {
                return Ngst;
            }
            set
            {
                Ngst = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("NgSt"));
                }
            }
        }
        private int Ngst = 0;


        public bool IsSc
        {
            get
            {
                return isSc;
            }
            set
            {
                isSc = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsSc"));
                }
            }
        }
        private bool isSc = false;
        public bool IsHla
        {
            get
            {
                return isHla;
            }
            set
            {
                isHla = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsHla"));
                }
            }
        }
        private bool isHla = false;
        public bool IsHlm
        {
            get
            {
                return isHlm;
            }
            set
            {
                isHlm = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsHlm"));
                }
            }
        }
        private bool isHlm = false;
        public bool IsVla
        {
            get
            {
                return isVla;
            }
            set
            {
                isVla = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsVla"));
                }
            }
        }
        private bool isVla = false;
        public bool IsVlm
        {
            get
            {
                return isVlm;
            }
            set
            {
                isVlm = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsVlm"));
                }
            }
        }
        private bool isVlm = false;
        public bool IsAr
        {
            get
            {
                return isAr;
            }
            set
            {
                isAr = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsAr"));
                }
            }
        }
        private bool isAr = false;
        public bool IsAd
        {
            get
            {
                return isAd;
            }
            set
            {
                isAd = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsAd"));
                }
            }
        }
        private bool isAd = false;
        public bool IsSp
        {
            get
            {
                return isSp;
            }
            set
            {
                isSp = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsSp"));
                }
            }
        }
        private bool isSp = false;
        public bool IsSd
        {
            get
            {
                return isSd;
            }
            set
            {
                isSd = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsSd"));
                }
            }
        }
        private bool isSd = false;
        public bool IsAw
        {
            get
            {
                return isAw;
            }
            set
            {
                isAw = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsAw"));
                }
            }
        }
        private bool isAw = false;

        public bool IsIH1
        {
            get
            {
                return isIH1;
            }
            set
            {
                isIH1 = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsIH1"));
                }
            }
        }
        private bool isIH1 = false;
        public bool IsIH2
        {
            get
            {
                return isIH2;
            }
            set
            {
                isIH2 = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsIH2"));
                }
            }
        }
        private bool isIH2 = false;
        public bool IsIH3
        {
            get
            {
                return isIH3;
            }
            set
            {
                isIH3 = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsIH3"));
                }
            }
        }
        private bool isIH3 = false;
        public bool IsIH4
        {
            get
            {
                return isIH4;
            }
            set
            {
                isIH4 = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsIH4"));
                }
            }
        }
        private bool isIH4 = false;
        public bool IsIH5
        {
            get
            {
                return isIH5;
            }
            set
            {
                isIH5 = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsIH5"));
                }
            }
        }
        private bool isIH5 = false;
        public bool IsIH6
        {
            get
            {
                return isIH6;
            }
            set
            {
                isIH6 = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsIH6"));
                }
            }
        }
        private bool isIH6 = false;

        public bool IsAt
        {
            get
            {
                return isAt;
            }
            set
            {
                isAt = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsAt"));
                }
            }
        }
        private bool isAt = false;
        public bool Ishbiao
        {
            get
            {
                return ishbiao;
            }
            set
            {
                ishbiao = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Ishbiao"));
                }
            }
        }
        private bool ishbiao = false;

        public bool Isvbiao
        {
            get
            {
                return isvbiao;
            }
            set
            {
                isvbiao = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Isvbiao"));
                }
            }
        }
        private bool isvbiao = false;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            encryption.jiami();
            this.DataContext = this;
            this.Closing += mainwindow_close;//重载关闭函数，缺陷数据库变化时提醒

            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);//定时查询加密狗
            dispatcherTimer.Interval = new TimeSpan(0, 5, 0);
            dispatcherTimer.Start();

            imgList = new List<MyDataObject>();//缩略图
            bitList = new List<bitmapObject>();
            rootpath = Environment.CurrentDirectory;
            //LoadFenbianv(rootpath);
            LoadCorrect(rootpath);//从软件根目录获取上一次存储的校准信息
            this.KeyDown += ModifyPrice_KeyDown;//设置快捷键
            flagtext.Visibility = Visibility.Collapsed;//折叠图片信息文本框
            flagtext.IsEnabled = false;
            ThreadPool.QueueUserWorkItem(new WaitCallback(ref this.TcpListen));
            //if (DateTime.Now > DateTime.Parse("2022/3/1"))
            //{
            //    MessageBox.Show("试用期已结束！");
            //    this.Close();
            //}
                
        }
        public void TcpListen(object x)
        {
            //Socket tcpSever = new Socket(SocketType.Stream, ProtocolType.Tcp);
            //EndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8888);
            //tcpSever.Bind(endPoint);
            //tcpSever.Listen(1);
            //while (true)
            //{
            //    Socket clientSocket = tcpSever.Accept();//暂停当前线程，直到一个客户端连接过来，之后进行下面的代码。返回的socket为客户端socket
            //    byte[] buffer = new byte[1024];
            //    int length = clientSocket.Receive(buffer);//接受客户端数据
            //    string mes = Encoding.UTF8.GetString(buffer, 0, length);
            //    ExecuteTcpCmd(mes);
            //    clientSocket.Close();
            //}
            

         }
 


        public void ExecuteTcpCmd(string mes)
        {
            //String str = "{\"name\": \"CallImageView\", \"data\": {\"message\": \"C:\\\\Users\\\\lqb\\\\Desktop\\\\毕设\\\\测厚\\\\3-9\\\\14mm钢板1.DICONDE\"}, \"extra\": {\"now\": 1502509602772}}";
            JObject jsonText = (JObject)JsonConvert.DeserializeObject(mes);
            try
            {
                string name = (string)jsonText["name"];
                switch (name)
                {
                    case "CallImageView":
                        this.Dispatcher.Invoke(() => {
                            CallImageView((string)jsonText["data"]["message"]);
                        });
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {

            }
        }

        private static DispatcherOperationCallback
             exitFrameCallback = new DispatcherOperationCallback(ExitFrame);
        public static void DoEvents()
        {
            DispatcherFrame nestedFrame = new DispatcherFrame();
            DispatcherOperation exitOperation =
                Dispatcher.CurrentDispatcher.BeginInvoke(
                DispatcherPriority.Background,
                exitFrameCallback, nestedFrame);
            Dispatcher.PushFrame(nestedFrame);

            if (exitOperation.Status != DispatcherOperationStatus.Completed)
            {
                exitOperation.Abort();
            }
        }
        private static object ExitFrame(object state)
        {
            DispatcherFrame frame = state as DispatcherFrame;
            frame.Continue = false;
            return null;
        }
        public void CallImageView(string path)
        {
            //InitializeComponent();
            encryption.jiami();
            this.DataContext = this;
            this.Closing += mainwindow_close;
            imgList = new List<MyDataObject>();//缩略图
            rootpath = Environment.CurrentDirectory;
            LoadCorrect(rootpath);//从软件根目录获取上一次存储的校准信息
            this.KeyDown += ModifyPrice_KeyDown;//设置快捷键
            flagtext.Visibility = Visibility.Collapsed;//折叠图片信息文本框
            flagtext.IsEnabled = false;

            Ddengji = null; Wdengji = null; snrvalue = null; meangray = null; filepath = null;
            filepath = path.Substring(0, path.LastIndexOf("\\") + 1);

            if (filepath.Contains("评定图像"))
            {
                DefectView = true;
            }
            else
            {
                DefectView = false;
                if (!Directory.Exists(filepath + "评定图像"))
                {
                    Directory.CreateDirectory(filepath + "评定图像");
                }
            }
            

            pictureName = path.Substring(path.LastIndexOf("\\") + 1, path.Length - path.LastIndexOf("\\") - 1);//修改
            LoadTxt(filepath);
            LoadModels(filepath);
            LoadDicomFile(path);
            CopyExcel(filepath);
            //LoadFenbianv(filepath);
            //listpicname = openFileDlg.FileName;            
            SaveTexts.Clear();
            imgProcess.ResetImgProcessArgsPartly();
            ThreadPool.QueueUserWorkItem(new WaitCallback(ref this.SmallPicture));
        }
        private void SmallPicture(object x)
        {

            this.Dispatcher.Invoke
                (new Action
                    (delegate
                        {
                            string[] Files = System.IO.Directory.GetFiles(filepath);
                            imgList = GetAllImagePath(filepath);
                            foreach (MyDataObject img in imgList)
                            {
                                bitmapObject b = new bitmapObject
                                {
                                    photo = imgFormatTransform.ImageToBitmapSource(img.photo),
                                    Index = img.Index
                                };
                                bitList.Add(b);
                            }
                            listbox.ItemsSource = null;
                            listbox.ItemsSource = bitList;
                            logger.Info("path resorted");
                            GC.Collect(0, GCCollectionMode.Forced);
                            int idx = picName.IndexOf(pictureName);
                            s = listbox.ItemContainerGenerator.ContainerFromIndex(idx) as ListBoxItem;
                            s.Background = selectcolor;
                            listbox.ScrollIntoView(listbox.Items[idx]);
                        }
                    )
                );
        }
        private void ModifyPrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)//Esc键  
            {
                if (IsTw)
                {
                    IsTw = false;
                    SetDispalyState(ref showArea, ref winchange);
                    //finddk.Background = basecolor;
                    if (CancelType == "ss")
                    {
                        RefreshImgDisplay(ref dcmImage);
                        CancelType = null;
                    }
                }
                if (IsTm)
                {
                    IsTm = false;
                    SetDispalyState(ref showArea, ref winchange);
                    //finddk.Background = basecolor;
                    if (CancelType == "ss")
                    {
                        RefreshImgDisplay(ref dcmImage);
                        CancelType = null;
                    }
                }
                else if (IsSnr)
                {
                    IsSnr = false;
                    SetDispalyState(ref winchange);
                    clearlines();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
                else if (IsHla)
                {
                    IsHla = false;
                    clearlines();
                    cleardefectline();
                    SetDispalyState(ref showArea, ref winchange);
                }
                else if (IsHlm)
                {
                    IsHlm = false;
                    clearlines();
                    cleardefectline();
                    SetDispalyState(ref showArea, ref winchange);
                }
                else if (IsAr)
                {
                    IsAr = false;
                    clearlines();
                    cleardefectline();
                    defectline = null;
                    SetDispalyState(ref winchange);
                }
                else if (round1)
                {
                    round1 = false;
                    ClearRuler();
                    ClearArea();
                }
                else if (rec1 || rec2 || rec3)
                {
                    rec1 = false;
                    rec2 = false;
                    rec3 = false;
                    ClearRuler();
                    ClearArea();
                }
                else if (MarkSwitch)
                {
                    IsAddDefect(false);
                    ClearRuler();
                    ClearArea();
                    cleardefectline();
                    showArea = false;
                    showLine = false;
                    if (!pinjie) winchange = true;
                }
                else if (IsSp)
                {
                    IsSp = false;
                    pinjie = false;
                    DefectShow = false;
                    clearall();
                    SetDispalyState(ref winchange);
                    if (pintu != null)
                    {
                        pintu.Dispose();
                        pintu = null;
                    }                    
                    meangrayTB.Text = meangray;                                       
                    RefreshImgDisplay(ref dcmImage);
                }
                else if (IsAw)
                {
                    IsAw = false;
                    SetDispalyState(ref showArea, ref winchange);                    
                }
                else if (Ishbiao)
                {
                    Ishbiao = false;
                    SetDispalyState(ref showArea, ref winchange);
                    RefreshImgDisplay(ref dcmImage);
                }
                else if (Isvbiao)
                {
                    Isvbiao = false;
                    SetDispalyState(ref showArea, ref winchange);
                    RefreshImgDisplay(ref dcmImage);
                }
            }
            if (e.Key == Key.Back)
            {
                cancel.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
            if (e.Key == Key.Enter)
            {
                //Save.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                if (!MarkSwitch)
                {
                    MessageBox.Show("未处于标记缺陷状态，功能禁用");
                    return;
                }
                Qtype = null;
                QueSize = null;
                Thick = null;
                Ping = null;
                InputA = null;
                InputB = null;
                Saveq();
            }
        }
        private void SaveCorrect(string a)
        {
            if (!File.Exists(a + "paremeter.txt"))
            {
                FileStream fs1 = new FileStream(a + "paremeter.txt", FileMode.Create, FileAccess.ReadWrite);//创建写入文件 
                StreamWriter sw = new StreamWriter(fs1);
                sw.WriteLine(correct.ToString());//开始写入值                
                //sw.WriteLine("1");
                sw.Close();
                fs1.Close();
                fs1.Dispose();
            }
            else
            {
                try
                {
                    FileStream fs = new FileStream(a + "paremeter.txt", FileMode.Open, FileAccess.ReadWrite);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.WriteLine(correct.ToString());
                    sw.Close();
                    fs.Close();
                    fs.Dispose();
                }
                catch
                {

                }
            }
        }
        private void LoadCorrect(string a)
        {
            if (!File.Exists(a + "paremeter.txt"))
            {
                return;
            }
            else
            {
                try
                {
                    FileStream fs = new FileStream(a + "paremeter.txt", FileMode.Open, FileAccess.ReadWrite);
                    StreamReader sr = new StreamReader(fs);
                    correct = double.Parse(sr.ReadLine());
                    sr.Close();
                    fs.Close();
                    fs.Dispose();
                }
                catch
                {

                }
            }
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            encryption.jiami();//执行查询
        }
        private void mainwindow_close(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (modelchange == false) { e.Cancel = false; }
            else
            {
                if (MessageBox.Show("尚未保存缺陷数据，是否确认关闭？", "确认", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    e.Cancel = false;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        public List<MyDataObject> GetAllImagePath(string path)
        {
            imgList.Clear();
            bitList.Clear();
            dcmlocations.Clear();
            picName.Clear();
            DicomFile dcmFileL;
            DicomDataset dcmDataSetL;
            DicomImage dcmImageL;
            Image<Gray, byte> grayl;
            BitmapSource bitl;
            string[] Files = System.IO.Directory.GetFiles(filepath);

            foreach (string item in Files)
            {
                string fileName = item.Substring(item.LastIndexOf("\\") + 1);
                if (fileName.Contains("dcm") || fileName.Contains("dcn") || fileName.Contains("DICONDE"))
                {
                    dcmlocations.Add(item);
                }
            }
            dcmlocations.Sort((string x1, string x2) => {
                string numPattern = @"(\d+)\.(dcn|dcm|DICONDE)";
                int n1 = -1;
                int n2 = -1;
                var match = Regex.Match(x1.Trim(), numPattern);
                if (match.Success)
                {
                    n1 = int.Parse(match.Groups[1].Value);
                }
                match = Regex.Match(x2.Trim(), numPattern);
                if (match.Success)
                {
                    n2 = int.Parse(match.Groups[1].Value);
                }
                if (n1 < n2)
                {
                    return -1;
                }
                else if (n1 == n2)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            });

            int i = 0;
            string[] a = new string[dcmlocations.Count];
            for (int n = 0; n < a.Length; n++)
            {
                a[n] = null;
            }
            int k = 0;
            foreach (string item in dcmlocations)
            {
                try
                {
                    dcmFileL = DicomFile.Open(item);
                    dcmDataSetL = dcmFileL.Dataset;
                    dcmImageL = new DicomImage(dcmDataSetL);
                    //dcmImageL.WindowCenter = wincenter;
                    //dcmImageL.WindowWidth = winwidth;
                    grayl = new Image<Gray, byte>((Bitmap)dcmImageL.RenderImage());

                    //photos.Add(bitl);
                    i++;
                    imgList.Add(new MyDataObject() { photo = grayl, Index = i });
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                            this,
                            item + ex.Message + "\r\n" + "已跳过该文件",
                            "DICOM文件打开错误");
                    a[k] = item; k++;
                }
            }
            for (int n = 0; n < a.Length; n++)
            {
                if (a[n] == null) break;
                dcmlocations.Remove(a[n]);
            }

            foreach (string item in dcmlocations)
            {
                string fileName = item.Substring(item.LastIndexOf("\\") + 1);
                picName.Add(fileName);
            }
            return imgList;
        }
        #region 文件
        private void btn_Open_Click(object sender, RoutedEventArgs e)
        {
            if (modelchange)//检测缺陷数据是否已经保存
            {
                if (MessageBox.Show("尚未保存缺陷数据，是否确认打开新文件夹？", "确认", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                { }
                else return;
            }
            OpenFileDialog openFileDlg = new OpenFileDialog();
            openFileDlg.Multiselect = true;
            //openFileDlg.Filter = "图片格式(*.dcm;*.DICON;*.dcn)|*.dcm;*.DICON";
            openFileDlg.Filter = "图像文件|*.dcm;*.dcn;*.DICONDE|所有文件|*.*";
            if (openFileDlg.ShowDialog() == true)
            {
                //Debug.WriteLine(openFileDlg.FileName);
                Ddengji = null; Wdengji = null; snrvalue = null; meangray = null; filepath = null; IsVlm = false;
                filepath = openFileDlg.FileName.Substring(0, openFileDlg.FileName.LastIndexOf("\\") + 1);

                if (filepath.Contains("评定图像"))
                {
                    DefectView = true;
                }
                else
                {
                    DefectView = false;
                    if (!Directory.Exists(filepath + "评定图像"))
                    {
                        Directory.CreateDirectory(filepath + "评定图像");
                    }
                }
                if (filepath.Contains("拼图结果"))
                {

                }
                else
                {
                    if (!Directory.Exists(filepath + "拼图结果"))
                    {
                        Directory.CreateDirectory(filepath + "拼图结果");
                    }
                }

                pictureName = openFileDlg.FileName.Substring(openFileDlg.FileName.LastIndexOf("\\") + 1, openFileDlg.FileName.Length - openFileDlg.FileName.LastIndexOf("\\") - 1);//修改
                LoadTxt(filepath);
                LoadModels(filepath);
                Resetdefcoords();
                Resetpinjieshow();
                LoadDicomFile(openFileDlg.FileName);
                CopyExcel(filepath);
                //excelPath = CreateGrayExcel(filepath + pictureName);
                //LoadFenbianv(filepath);
                //listpicname = openFileDlg.FileName;
                imgProcess.ResetImgProcessArgsPartly();                
                SaveTexts.Clear();
                CancelKey.Clear();
                ThreadPool.QueueUserWorkItem(new WaitCallback(ref SmallPicture));//处理缩略图
            }
            
        }
        //public Image<Gray, ushort> imgPinJieResult16;   //将处理完的8位图像结果转为16位，后面用来转存为dcm格式(处理完的结果不需要保存为dcm，只需将拼图结果保存为dcm)
        private void RefreshImgDisplay(ref DicomImage dcmImg)
        {
            Image<Gray, ushort> imgGU,imgGU16 ;
            Image<Gray, byte> imgGU8 = new Image<Gray, byte>(dcmImage.Width, dcmImage.Height);
            imgGU = Utils.DicomImage2UShortEmguImage(dcmImage);
            Image<Bgr, byte> imgbgr;
            //imgbgrP = imgGU.Convert<Bgr, byte>();
            
            if (bcifProcessFlag == true)
            { }
            else
            {
                if (ImgProcessFlag == true)
                {
                    switch (ImageEnhanceNum)
                    {

                        case 1:
                            imgGU8 = imgProcess.ImageEnhance1(ref imgGU);                  //对原图进行Retinex增强
                            break;
                        case 2:
                            imgGU8 = imgProcess.ImageEnhance2(ref imgGU);                  //对原图进行Retinex增强
                            break;
                        case 3:
                            imgGU8 = imgProcess.ImageEnhance3(ref imgGU);                  //对原图进行Retinex增强
                            break;
                        case 4:
                            imgGU8 = imgProcess.ImageEnhance4(ref imgGU);                  //对原图进行Retinex增强
                            break;
                        case 5:
                            imgGU8 = imgProcess.ImageEnhance5(ref imgGU);                  //对原图进行Retinex增强
                            break;
                        case 6:
                            imgGU8 = imgProcess.ImageEnhance6(ref imgGU);                  //对原图进行Retinex增强
                            break;
                    }
                    imgbgr = SetImgDirection8(imgGU8);
                    imgbgrP = imgbgr;
                }
                else
                {
                    imgGU16 = imgProcess.ProcessImgGrayUshort16(imgGU);           //进行16位的图像处理：加载图像、CLAHE、锐化
                    imgbgr = SetImgDirection16(imgGU16);
                    imgbgrP = imgbgr;
                }
            }

            imgbgr = imgProcess.ProcessImgGrayUshort8(imgbgrP, isInverse, isfudiao);
            //imgPinJieResult16 = imgbgrP.Convert<Gray, ushort>().Mul(255);

            if (DefectView && !DefectShow)
            {
                imgbgr = drawdefects(imgbgr, pictureName);
            }
            else
            {
                DrawPicture(ref imgbgr);
            }

            imgbox.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                bitSourse = imgFormatTransform.ImageToBitmapSource(imgbgr);
                BaseBps = bitSourse;
                imgbox.Source = bitSourse;
                AdjustImageBoxSize(bitSourse);
            });

            }
        public Image<Bgr, byte> imgbgrPpj;
        private void RefreshImgDisplayPintu(ref Image<Gray, ushort> img)
        {
            Image<Gray, ushort> imgGU, imgGU16;
            imgGU = img;
            Image<Bgr, byte> imgbgr;

            if (bcifProcessFlag == true)                                  //亮度，对比度，反色，浮雕
            {
                if (imgbgrPpj == null)  imgbgrPpj = img.Convert<Bgr, byte>();
    
            }
            else
            {                
                imgGU16 = imgProcess.ProcessImgGrayUshort16(imgGU);           //进行16位的图像处理：加载图像、CLAHE、锐化
                imgbgr = SetImgDirection16(imgGU16);
                imgbgrPpj = imgbgr;
            }

            imgbgr = imgProcess.ProcessImgGrayUshort8(imgbgrPpj, isInverse, isfudiao);
            //imgPinJieResult16 = imgbgr.Convert<Gray, ushort>().Mul(255);

            if (DefectView && !DefectShow)
            {
                imgbgr = drawdefects(imgbgr, pictureName);
            }
            else
            {
                DrawPicture(ref imgbgr);
            }

            imgbox.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                Image<Bgr, byte> grayd = DrawDefect(new Image<Bgr, byte>(imgbgr.Bitmap), 0);
                bitSourse = imgFormatTransform.ImageToBitmapSource(grayd);
                imgbox.Source = bitSourse;
                AdjustImageBoxSize(bitSourse);
            });

        }


        private Image<Bgr, byte> SetImgDirection16(Image<Gray, ushort> img)
        {
            //处理顺序为：反色->水平->垂直->旋转，其中水平垂直旋转操作都会改变图像的像素点排列，因此这三个操作会互相影响                                       
            
            if (pinjie == false)
            {
                if (isHSwitch) img._Flip(Emgu.CV.CvEnum.FlipType.Horizontal);
                if (isVSwitch) img._Flip(Emgu.CV.CvEnum.FlipType.Vertical);
                if (rotate != 0) img = img.Rotate(rotate, new Gray(0), false);
                BaseBps = imgFormatTransform.ImageToBitmapSource(img);//用来确定拼接图像中每个图像的状态
                //BaseBps =img;//用来确定拼接图像中每个图像的状态
            }
            // new Image<Bgr, byte>(img.Bitmap);

            double maxva = wincenter + winwidth / 2;
            double minva = wincenter - winwidth / 2;
            Image<Gray, ushort> imgs = img.Min(maxva);
            imgs = imgs.Max(minva);

            double ap = 255 / (maxva - minva);
            double shi = -ap * minva;
            Image<Gray, byte> img1 = imgs.ConvertScale<byte>(ap, shi);
            Image<Bgr, byte> img0 = new Image<Bgr, byte>(img1.Bitmap);
            return img0;
        }


        private Image<Bgr, byte> SetImgDirection8(Image<Gray, byte> img)
        {
            //处理顺序为：反色->水平->垂直->旋转，其中水平垂直旋转操作都会改变图像的像素点排列，因此这三个操作会互相影响                                       
            if (isHSwitch) img._Flip(Emgu.CV.CvEnum.FlipType.Horizontal);
            if (isVSwitch) img._Flip(Emgu.CV.CvEnum.FlipType.Vertical);
            if (rotate != 0) img = img.Rotate(rotate, new Gray(0), false);
            if (pinjie == false)
            {
                BaseBps = imgFormatTransform.ImageToBitmapSource(img);//用来确定拼接图像中每个图像的状态
            }

            /*
            double maxva = (wincenter + winwidth / 2)/255;
            double minva = (wincenter - winwidth / 2)/255;
            Image<Gray, byte> imgs = img.Min(maxva);
            imgs = imgs.Max(minva);

            double ap = 255 / (maxva - minva);
            double shi = -ap * minva;
            Image<Gray, byte> img1 = imgs.ConvertScale<byte>(ap, shi);
            Image<Bgr, byte> img0 = new Image<Bgr, byte>(img1.Bitmap);*/
            Image<Bgr, byte> img0 = new Image<Bgr, byte>(img.Bitmap);
            return img0;
        }
        private void DrawPicture(ref Image<Bgr, byte> imgbgr)
        {
            imgbgr = DrawDefect(imgbgr);            
        }
        private void CopyExcel(string DestinyPath)
        {
            string path;
            if (DefectView)
            {
                path = DestinyPath.Trim() + "\\缺陷报表.xlsx";
            }
            else
            {
                path = DestinyPath.Trim() + "\\评定图像\\缺陷报表.xlsx";
            }
            if (File.Exists(path)) return;
            System.IO.File.Copy(rootpath + "\\缺陷报表.xlsx", path);
        }
        private void LoadModels(string originpath)//从txt载入缺陷数据
        {
            modelchange = false;
            models.Clear();
            dataGridModel.ItemsSource = null;
            string path;
            if (DefectView)
            {               
                path = originpath.Trim() + "\\quexian.txt";
            }
            else
            {
                path = originpath.Trim() + "\\评定图像\\quexian.txt";
            }
            if (System.IO.File.Exists(path))
            {
                try
                {
                    string[] strArray = File.ReadAllLines(path);
                    int numque = int.Parse(strArray[0]);
                    int cls = 9;
                    for (int i = 0; i < numque; i++)
                    {
                        ModelInfo modelInfo = new ModelInfo();
                        modelInfo.ID = i + 1;
                        modelInfo.PicName = strArray[i * cls + 1];
                        modelInfo.Ping = strArray[i * cls + 2];
                        modelInfo.Qsize = strArray[i * cls + 3];
                        modelInfo.Qthick = strArray[i * cls + 4];
                        modelInfo.TypeName = strArray[i * cls + 5];
                        modelInfo.Qtype = int.Parse(strArray[i * cls + 6]);
                        modelInfo.InputA = strArray[i * cls + 7];
                        modelInfo.InputB = strArray[i * cls + 8];
                        modelInfo.Coordinates = strArray[i * cls + 9];
                        models.Add(modelInfo);
                        modelInfo = null;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this,
                    ex.Message,
                    "缺陷数据损坏");
                    logger.Error(ex.Message);
                    return;                   
                }
                dataGridModel.ItemsSource = models;
            }
        }

        private void Resetdefcoords()//重置缺陷数组，初始元素值为-1（消除与正数坐标之间的歧义）；同时-1充当检测标志位，不可随意替换
        {
            NumofDefs = 0;
            for (int i = 0; i < defcoords.Length; i++)
            {
                defcoords[i] = -1;
            }
        }
        private void Resetpinjieshow()//重置拼接数组
        {
            NumofDefs = 0;
            for (int i = 0; i < pinjieshow.Length; i++)
            {
                pinjieshow[i] = -1;
            }
        }
        private void LoadCoords(string cs)//将缺陷txt内的string转化为double数组
        {
            Resetdefcoords();
            coord = cs.Split(',');
            for(int i=0;i<coord.Count()-1;i++)
            {
                defcoords[i] = int.Parse(coord[i]);
            }
            for(int j=0;j<defcoords.Length-1;j++)
            {
                if(defcoords[8*j]==-1)
                {
                    NumofDefs = j ;
                    break;
                }
            }
        }
        
        private void LoadTxt(string originpath)//新建或读取已有的状态信息
        {
            string a;
            if (!DefectView)
            {
                a = originpath.Trim() + "\\";
            }
            else
            {
                a  = originpath.Substring(0, originpath.LastIndexOf("评定图像")) + "\\";
            }
            if (!File.Exists(a + "StateTxt.txt"))
            {
                FileStream fs1 = new FileStream(a + "StateTxt.txt", FileMode.Create, FileAccess.ReadWrite);//创建写入文件 
                StreamWriter sw = new StreamWriter(fs1);
                sw.WriteLine("0");//开始写入值
                sw.WriteLine("0");
                sw.WriteLine("0");
                sw.WriteLine("0");
                sw.WriteLine("0");
                sw.WriteLine("32768");
                sw.WriteLine("65536");
                //sw.WriteLine("1");
                sw.Close();
                fs1.Close();
                fs1.Dispose();
            }
            else
            {
                try
                {
                    FileStream fs = new FileStream(a + "StateTxt.txt", FileMode.Open, FileAccess.ReadWrite);
                    StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);

                    s1 = sr.ReadLine();
                    if (s1 == "0") { isInverse = false; }
                    else { isInverse = true; IsNg = true; }

                    s2 = sr.ReadLine();
                    if (s2 == "0") { isHSwitch = false; }
                    else { isHSwitch = true; }

                    s3 = sr.ReadLine();
                    if (s3 == "0") { isVSwitch = false; }
                    else { isVSwitch = true; }

                    s4 = sr.ReadLine();
                    if (s4 == "0") { isfudiao = false; }
                    else { isfudiao = true; IsSc = true; }

                    s5 = sr.ReadLine();
                    rotate = int.Parse(s5);

                    wincenter = double.Parse(sr.ReadLine());
                    winwidth = double.Parse(sr.ReadLine());                   

                    sr.Close();
                    fs.Close();
                    fs.Dispose();
                }
                catch(Exception ex )
                {
                    logger.Error(ex.Message);
                }
            }
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)//修改
        {
            //if (dcmDataSet == null) return;
            //dcmFile.Dataset.Add(DicomTag.WindowWidth, dcmImage.WindowWidth.ToString());
            //dcmFile.Dataset.Add(DicomTag.WindowCenter, dcmImage.WindowCenter.ToString());
            ////string pathName;
            ////if (dcmFile.fileName == "") pathName = "newdicom.dcm";
            ////else pathName = dcmFile.fileName;
            ////dcmFile.Save("a"+pathName);
            ////dcmFile.Save(@"C:\Users\toni\Desktop\ImageView\东检图片1\新建文件夹\test.dcm");
            //SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            //saveFileDialog1.Filter = "DCM(*.dcm)|*.dcm";
            //if (saveFileDialog1.ShowDialog() == true)
            //{
            //    dcmFile.Save(saveFileDialog1.FileName);
            //    MessageBox.Show("保存完毕");
            //}

            //byte[] datas = dcmDataSet.Get<byte[]>(DicomTag.PixelData);
            //MemoryByteBuffer buffer = new MemoryByteBuffer(datas);
            //DicomDataset dataset = new DicomDataset();
            ////FillDataset(dataset);
            //String da = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();
            //String ti = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
            //dataset.Add(DicomTag.PhotometricInterpretation, PhotometricInterpretation.Monochrome2.Value);//PhotometricInterpretation.Monochrome2.Value
            //dataset.Add(DicomTag.Rows, (ushort)dcmImage.Height);
            //dataset.Add(DicomTag.Columns, (ushort)dcmImage.Width);
            //dataset.Add(DicomTag.BitsAllocated, (ushort)16);
            //dataset.Add(DicomTag.SOPClassUID, "1.2.840.10008.5.1.4.1.1.2");
            //dataset.Add(DicomTag.SOPInstanceUID, "1.2.840.10008.5.1.4.1.1.2." + da + ti);
            //dataset.Add(DicomTag.Modality, "DR");
            //dataset.Add(DicomTag.AcquisitionDate, DateTime.Today);
            //dataset.Add(DicomTag.AcquisitionDateTime, da + ti);
            //dataset.Add(DicomTag.AcquisitionTime, ti);
            //DicomPixelData pixelData = DicomPixelData.Create(dataset, true);
            //pixelData.BitsStored = 16;
            ////pixelData.BitsAllocated = 8;
            //pixelData.SamplesPerPixel = 1;
            //pixelData.HighBit = 15;
            //pixelData.PixelRepresentation = 0;
            //pixelData.PlanarConfiguration = 0;
            //pixelData.AddFrame(buffer);           
            //DicomFile dicomfile = new DicomFile(dataset);
            //dicomfile.Save(@"F:\hu\图像\王工-评片软件\东检图片1\新建文件夹\test.dcm");
            if (dcmImage == null) return;

            if (!DefectView)
            {               
                try
                {
                    string path = filepath.Trim() + "\\评定图像\\quexian.txt";
                    FileStream fs = new FileStream(path, FileMode.Create);
                    StreamWriter sw = new StreamWriter(fs);
                    //开始写入
                    sw.WriteLine(models.Count);
                    for (int i = 0; i < models.Count; i++)
                    {
                        sw.WriteLine(models[i].PicName);
                        sw.WriteLine(models[i].Ping);                       
                        sw.WriteLine(models[i].Qsize);
                        sw.WriteLine(models[i].Qthick);
                        sw.WriteLine(models[i].TypeName);
                        sw.WriteLine(models[i].Qtype.ToString());
                        sw.WriteLine(models[i].InputA);
                        sw.WriteLine(models[i].InputB);
                        sw.WriteLine(models[i].Coordinates);
                        
                        try
                        {
                            DicomFile dcmFilec = DicomFile.Open(filepath + models[i].PicName);
                            dcmFilec.Save(@filepath.Trim() + "\\评定图像\\" + models[i].PicName);
                        }
                        catch { }
                        
                    }


                    //清空缓冲区
                    sw.Flush();
                    //关闭流
                    sw.Close();
                    fs.Close();
                    modelchange = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                            this,
                            ex.Message,
                            "无法写入缺陷数据");
                }
                string[] Files = System.IO.Directory.GetFiles(filepath.Trim() + "\\评定图像");
                foreach (string item in Files)
                {
                    string fileName = item.Substring(item.LastIndexOf("\\") + 1);
                    if (fileName.Contains("dcm") || fileName.Contains("dcn")|| fileName.Contains("DICONDE"))
                    {
                        bool delete = true;
                        for(int i = 0; i < models.Count; i++)
                        {
                            if (fileName == models[i].PicName) { delete = false;break; }
                        }
                        if(delete)
                        {
                            File.Delete(item);
                        }                          
                    }
                }
            }
            else
            {
                try
                {
                    string path = filepath.Trim() + "\\quexian.txt";
                    FileStream fs = new FileStream(path, FileMode.Create);
                    StreamWriter sw = new StreamWriter(fs);
                    //开始写入
                    sw.WriteLine(models.Count);
                    for (int i = 0; i < models.Count; i++)
                    {
                        sw.WriteLine(models[i].PicName);
                        sw.WriteLine(models[i].Ping);
                        sw.WriteLine(models[i].Qsize);
                        sw.WriteLine(models[i].Qthick);
                        sw.WriteLine(models[i].TypeName);
                        sw.WriteLine(models[i].Qtype.ToString());
                        sw.WriteLine(models[i].InputA);
                        sw.WriteLine(models[i].InputB);
                        sw.WriteLine(models[i].Coordinates);
                        try
                        {
                            DicomFile dcmFilec = DicomFile.Open(path = filepath.Substring(0, filepath.LastIndexOf("评定图像")) + "\\" + models[i].PicName);
                            dcmFilec.Save(@filepath.Trim() + "\\" + models[i].PicName);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(
                                    this,
                                    ex.Message,
                                    "原始图片丢失，数据损坏");
                        }
                    }
                    //清空缓冲区
                    sw.Flush();
                    //关闭流
                    sw.Close();
                    fs.Close();
                    modelchange = false;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                            this,
                            ex.Message,
                            "无法写入缺陷数据");
                }
                string[] Files = System.IO.Directory.GetFiles(filepath.Trim());
                foreach (string item in Files)
                {
                    string fileName = item.Substring(item.LastIndexOf("\\") + 1);
                    if (fileName.Contains("dcm") || fileName.Contains("dcn")|| fileName.Contains("DICONDE"))
                    {
                        bool delete = true;
                        for (int i = 0; i < models.Count; i++)
                        {
                            if (fileName == models[i].PicName) { delete = false; break; }
                        }
                        if (delete)
                        {
                            File.Delete(item);
                        }
                    }
                }
                //try
                //{
                //    string path = filepath.Substring(0, filepath.LastIndexOf("评定图像")) + "\\quexian.txt";
                //    FileStream fs = new FileStream(path, FileMode.Create);
                //    StreamWriter sw = new StreamWriter(fs);

            }
            SaveLocalExcel();
        }         
        private Image<Bgr, byte> drawdefects(Image<Bgr, byte> imgb, string name)//在彩色图绘制同一幅图片的所有缺陷
        {           
            string[] Files = System.IO.Directory.GetFiles(filepath.Trim());
            for (int i = 0; i < models.Count; i++)
            {
                if (name == models[i].PicName)
                {
                    LoadCoords(models[i].Coordinates);
                    classflag = models[i].Qtype;
                    imgb = DrawDefect(imgb);
                }
            }
            return imgb;
        }       
        private void btn_Save2Other_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "BMP(*.bmp)|*.bmp|TIF(*.tif)|*.tif|RAW(*.raw)|*.raw|JPG(*.jpg)|*.jpg";
            saveFileDialog1.RestoreDirectory = true;

            int a =saveFileDialog1.FilterIndex;
            if (saveFileDialog1.ShowDialog() == true)
            {
                string path = saveFileDialog1.FileName;
                string fileExtension = System.IO.Path.GetExtension(path).ToUpper();
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)this.imgbox.Source));
                using (FileStream stream = new FileStream(saveFileDialog1.FileName, FileMode.Create))
                encoder.Save(stream);

            }



        }
        private void ListBoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (pinjie == false)
                {
                    if (pintu != null) { pintu.Dispose(); pintu = null; }
                    if (s != null) { s.Background = basecolor; }
                    s = sender as ListBoxItem;
                    s.Background = selectcolor;
                    //string path = filepath.Trim() + "\\" + s.ToString().Substring(s.ToString().LastIndexOf(":") + 1).Trim();
                    int i = listbox.SelectedIndex;
                    //LoadDicomFile(filepath + picName[i]);
                    //listpicname = dcmlocations[i];
                    pictureName = picName[i];
                    Resetdefcoords();
                    Resetpinjieshow();
                    LoadDicomFile(dcmlocations[i]);
                    SaveTexts.Clear();
                    CancelKey.Clear();
                    IsVlm = false;
                }
            }
            
            catch
            {
                MessageBox.Show("请重新加载图片");
                
            }
            
        }
        public static int GetAverageGrayOfU16DicImg(DicomImage dicomImage)
        {
            var DataU16 = new GrayscalePixelDataU16(dicomImage.Width, dicomImage.Height, dicomImage.PixelData.BitDepth, dicomImage.PixelData.GetFrame(0));
            GCHandle d = GCHandle.Alloc(DataU16.Data, GCHandleType.Pinned);
            var rImg = new Image<Gray, ushort>(DataU16.Width, DataU16.Height, DataU16.Width*2, d.AddrOfPinnedObject());
            int avgGray = (int)rImg.GetAverage().Intensity;
            // 释放资源
            rImg.Dispose();
            d.Free();
            return avgGray;
        }

        private void LoadDicomFile(string path)
        {
            try
            {
                cirrad.Clear();
                circenter.Clear();                
                try
                { dcmFile = DicomFile.Open(path); }
                catch (Exception ex)
                {
                    MessageBox.Show(
                            this,
                            ex.Message,
                            "DICOM文件打开错误");
                    logger.Error(ex.Message);
                    return;
                }
                dcmDataSet = dcmFile.Dataset;
                dcmImage = new DicomImage(dcmDataSet);                
                iPixelData = dcmImage._pixelData;

                if (!pinjie)
                {                   
                    meangray = GetAverageGrayOfU16DicImg(dcmImage).ToString();
                    meangrayTB.Text = meangray;
                    var information1 = new information();
                    information1.LoadTxt(MainWindow.filepath);
                    string[] stringarray1 = information1.stringarray;
                    if (IsSd)
                    {
                        string datetime1 = null;
                        string seriesnumber = null;
                        string instancenumber = null;
                        try
                        {
                            string datetime0 = dcmDataSet.Get<String>(DicomTag.AcquisitionDateTime);
                            if (datetime0 != null)
                            {
                                datetime1 = datetime0.Substring(0, 4) + "年" + datetime0.Substring(4, 2) + "月" + datetime0.Substring(6, 2) + "日" + datetime0.Substring(8, 2) + "时"
                                        + datetime0.Substring(10, 2) + "分" + datetime0.Substring(12, 2) + "秒";
                            }
                        }

                        catch { }

                        try
                        {
                            seriesnumber = dcmDataSet.Get<String>(DicomTag.SeriesNumber);
                        }
                        catch { }
                        try
                        {
                            instancenumber = dcmDataSet.Get<String>(DicomTag.InstanceNumber);
                        }
                        catch { }
                        string flg = "文件名：" + filepath + pictureName + "\r\n" +
                                      "数据采集时间：" + datetime1 + "\r\n" +
                                      "GPS：" + "\r\n" +
                                      "图上坐标：" + "  " + "x：" + "  " + "y：" + "\r\n" +
                                      "图像尺寸：" + dcmImage.Width.ToString() + "*" + dcmImage.Height.ToString() + "\r\n" +
                                      "当前灰度：" + "\r\n";
                        string flg1 = "用户单位:" + "  " + stringarray1[5] + "\r\n" +
                                      "焊口编号:" + "  " + stringarray1[78] + "\r\n" +
                                      "射线电压:" + "  " + stringarray1[62] + "\r\n" +
                                      "射线电流:" + "  " + stringarray1[70] + "\r\n" +
                                      "厚度:" + "  " + stringarray1[17] + "\r\n" +
                                      "检测器类型:" + "  " + stringarray1[77] + "\r\n" +
                                      "距离:" + "  " + stringarray1[51] + "\r\n" +
                                      "检测类别: DR" + "\r\n" +
                                      "序列号：" + seriesnumber + "\r\n" +
                                      "影像号：" + instancenumber + "\r\n";
                        flagtext.Text = flg;
                        flagtext1.Text = flg1;
                    }
                    else
                    {
                        flagtext.Visibility = Visibility.Collapsed;
                        flagtext.IsEnabled = false;
                        flagtext1.Visibility = Visibility.Collapsed;
                        flagtext1.IsEnabled = false;
                    }
                    information1.Close();
                }
                
                else
                {
                    meangrayTB.Text = null;
                }

                dcmImage.WindowWidth = winwidth;
                dcmImage.WindowCenter = wincenter;
                InitImgBoxSize(ref dcmImage);
                RefreshImgDisplay(ref dcmImage);
                
                trans = 1;
                ResetAllSwitchtoFalseEXCEPT();
                SetDispalyState(ref showArea, ref winchange);
                IsAddDefect(false);                
                pinjie = false;
                //changduTextbox.Text = "";
                //mianjiTextbox.Text = "";
                //jiaozhunTextbox.Text = "50";
                MagImg.Source = null;
                DefectShow = false;
                IsSp = false;
                clearlines();
                cleardefectline();

                defectline = null;
                if (pintu != null)
                {
                    pintu.Dispose();
                    pintu = null;
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

        }       
        #endregion

        #region //虚拟滑动条
        private void WinwidthorWincenterChanged()
        {
            if (dcmImage != null&&pinjie==false)
            {
                dcmImage.WindowWidth = winwidth;
                dcmImage.WindowCenter = wincenter;
                RefreshImgDisplay(ref dcmImage );
            }
            else
            {
                dcmImage.WindowWidth = winwidth;
                dcmImage.WindowCenter = wincenter;
                RefreshImgDisplayPintu(ref imgPinJieResult16);
            }
           
        }

        #endregion

        #region 图像查看
        private void btn_Ruihua_Click(object sender, RoutedEventArgs e)
        {


            if (pinjie == true)
            {
                //sharpRatioPJ++;
                //if (dcmImage == null) return;
                ////imgProcess.AdjustSharpnessRatio(+1);
                //CancelKey.Add("sharpen");
                ////if (sharpRatio != 0) imgProcess.SharpnessAdjust(ref img, sharpRatio);
                //Image<Gray, ushort> imgPinJieResultU16 = imgPinJieResult8.Convert<Gray, ushort>().Mul(255);
                //if (sharpRatioPJ != 0) imgProcess.SharpnessAdjust(ref imgPinJieResultU16, sharpRatioPJ);
                //bitSourse = imgFormatTransform.ImageToBitmapSource(imgPinJieResultU16);
                //imgbox.Source = bitSourse;

                imgProcess.AdjustSharpnessRatio(+1);
                CancelKey.Add("sharpen");
                RefreshImgDisplayPintu(ref imgPinJieResult16);
            }
            else
            {
                if (dcmImage == null) return;
                imgProcess.AdjustSharpnessRatio(+1);
                CancelKey.Add("sharpen");
                RefreshImgDisplay(ref dcmImage);
            }

        }
        private void btn_Inverse_Click(object sender, RoutedEventArgs e)
        {
            bcifProcessFlag = true;
            if (pinjie == true)
            {
                IsNg = !IsNg;
                if (dcmImage == null) return;
                isInverse = !isInverse;
                if (isInverse == true) { s1 = "1"; }
                else s1 = "0";
                imgProcess.SwitchOnInverse(isInverse);
                RefreshImgDisplayPintu(ref imgPinJieResult16);
                CancelKey.Add("inverse");
                SaveTxt();
                bcifProcessFlag = false;
            }
            else
            {
                IsNg = !IsNg;
                if (dcmImage == null) return;
                isInverse = !isInverse;
                if (isInverse == true) { s1 = "1"; }
                else s1 = "0";
                imgProcess.SwitchOnInverse(isInverse);
                RefreshImgDisplay(ref dcmImage);
                CancelKey.Add("inverse");
                SaveTxt();
                bcifProcessFlag = false;
            }
            
        }

        private void btn_fudiao_Click(object sender, RoutedEventArgs e)
        {
            bcifProcessFlag = true;
            if (pinjie == true)
            {
                //MessageBox.Show("拼接后不允许修改！");
                //return;
                IsSc = !IsSc;
                if (dcmImage == null) return;
                isfudiao = !isfudiao;
                if (isfudiao == true) { s4 = "1"; }
                else s4 = "0";
                imgProcess.SwitchOnFudiao(isfudiao);
                RefreshImgDisplayPintu(ref imgPinJieResult16);
                CancelKey.Add("fudiao");
                SaveTxt();
                bcifProcessFlag = false;
            }
            else
            {
                IsSc = !IsSc;
                if (dcmImage == null) return;
                isfudiao = !isfudiao;
                if (isfudiao == true) { s4 = "1"; }
                else s4 = "0";
                imgProcess.SwitchOnFudiao(isfudiao);
                RefreshImgDisplay(ref dcmImage);
                CancelKey.Add("fudiao");
                SaveTxt();
                bcifProcessFlag = false;
            }

        }

        private void btn_HSwitch_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }

            if (dcmImage == null) return;
            if (rotate % 180 == 0)
            {
                isHSwitch = !isHSwitch;
            }
            else
            {
                isVSwitch = !isVSwitch;
            }
            if (isHSwitch) { s2 = "1"; }
            else { s2 = "0"; }
            if (isVSwitch) { s3 = "1"; }
            else { s3 = "0"; }
            RefreshImgDisplay(ref dcmImage);          
            SaveTxt();
            CancelKey.Add("Hswitch");
        }

        private void btn_VSwitch_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (dcmImage == null) return;
            if (rotate % 180 == 0)
            {
                isVSwitch = !isVSwitch;
            }
            else
            {
                isHSwitch = !isHSwitch;
            }
            if (isHSwitch) { s2 = "1"; }
            else { s2 = "0"; }
            if (isVSwitch) { s3 = "1"; }
            else { s3 = "0"; }
            RefreshImgDisplay(ref dcmImage);
            SaveTxt();
            CancelKey.Add("Vswitch");
        }

        private void btn_RotateLeft_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (dcmImage == null) return;
            rotate -= 90;
            if (rotate <= -90) rotate = 270;
            RefreshImgDisplay(ref dcmImage);
            s5 = rotate.ToString();
            SaveTxt();
            CancelKey.Add("rotateLeft");
        }

        private void btn_RotateRight_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (dcmImage == null) return;
            rotate += 90;
            if (rotate >= 360) rotate = 0;
            RefreshImgDisplay(ref dcmImage);            
            s5 = rotate.ToString();
            SaveTxt();
            CancelKey.Add("rotateRight");
        }

        private Image<Bgr, byte> DrawDefect(Image<Bgr, byte> imgbgr)
        {
            var color = System.Drawing.Color.Red;
            var font = new Font("黑体", 25, System.Drawing.FontStyle.Bold);
            int k = 0;
            //画缺陷
            for (int i = 0;i<30; i++)
            {
                if (defcoords[8 * i] == -1)
                {
                    k = i;
                    break;
                }
            }
            for (int i = 0; i < k; i++)
            {
                defectX1 = defcoords[8 * i]; defectY1 = defcoords[8 * i + 1]; defectX2 = defcoords[8 * i + 2]; defectY2 = defcoords[8 * i + 3];
                Parameter = defcoords[8 * i + 4]; qlength = defcoords[8 * i + 5]; qwidth = defcoords[8 * i + 6]; classflag = defcoords[8 * i + 7];
                if (classflag <= 0)
                {
                    int x3, y3, x4, y4;
                    x3 = TransformXY(defectX1, defectY1, out y3);
                    x4 = TransformXY(defectX2, defectY2, out y4);
                    if (x3 > x4)
                    {
                        int x;
                        x = x3;
                        x3 = x4;
                        x4 = x;
                    }
                    if (y3 > y4)
                    {
                        int y;
                        y = y3;
                        y3 = y4;
                        y4 = y;
                    }
                    int wi = (x4 - x3);
                    int hi = (y4 - y3);
                    drawInstance.DrawRectangle(ref imgbgr, color, x3, y3, wi, hi);
                    if (dcmImage.Width - x4 > 250&& dcmImage.Height - y4 > 250)
                        drawInstance.DrawText(ref imgbgr, color, font,
                        "面积：" + Parameter.ToString() + "mm2" + "\r\n" + "长度：" + qlength.ToString() + "mm" + "\r\n" + "宽度：" + qwidth.ToString() + "mm" + "\r\n",
                        x4, y4);
                    else if(dcmImage.Width - x4 < 250 && dcmImage.Height - y4 > 250)
                        drawInstance.DrawText(ref imgbgr, color, font, 
                        "面积：" + Parameter.ToString() + "mm2" + "\r\n" + "长度：" + qlength.ToString() + "mm" + "\r\n" + "宽度：" + qwidth.ToString() + "mm" + "\r\n", x4 - wi, y4);
                    else if(dcmImage.Width - x4 < 250 && dcmImage.Height - y4 < 250)
                        drawInstance.DrawText(ref imgbgr, color, font,
                        "面积：" + Parameter.ToString() + "mm2" + "\r\n" + "长度：" + qlength.ToString() + "mm" + "\r\n" + "宽度：" + qwidth.ToString() + "mm" + "\r\n", x4 - wi - 250, y4 - hi);
                    else
                        drawInstance.DrawText(ref imgbgr, color, font,
                        "面积：" + Parameter.ToString() + "mm2" + "\r\n" + "长度：" + qlength.ToString() + "mm" + "\r\n" + "宽度：" + qwidth.ToString() + "mm" + "\r\n", x4 , y4 - hi);
                }
                if (classflag == 1)
                {
                    int x3, y3, x4, y4;
                    x3 = TransformXY(defectX1, defectY1, out y3);
                    x4 = TransformXY(defectX2, defectY2, out y4);
                    drawInstance.DrawLine(ref imgbgr, color, x3, y3, x4, y4);
                    drawInstance.DrawText(ref imgbgr, color, font, "长度：" + Parameter.ToString() + "mm", x4, y4);
                }

            }
            //画字
            if (SaveTexts.Count != 0)
            {
                int n = SaveTexts.Count / 3;
                int xt, yt;
                string text;
                for (int i = 0; i < n; i++)
                {
                    xt = int.Parse(SaveTexts.ElementAt(3 * i));
                    yt = int.Parse(SaveTexts.ElementAt(3 * i + 1));
                    text = SaveTexts.ElementAt(3 * i + 2);
                    int x3, y3;
                    x3 = TransformXY(xt, yt, out y3);
                    drawInstance.DrawText(ref imgbgr, color, font, text, x3, y3);
                }
            }
            //处理临时画图需求
            if (drawNeeded)
            {
                drawNeeded = false;
                foreach(DefectInfo Info in defectInfo)
                {
                    if (Info.GetDefectClass() == 0)
                    {
                        int x3, y3, x4, y4;
                        x3 = TransformXY(Info.Getx1(), Info.Gety1(), out y3);
                        x4 = TransformXY(Info.Getx2(), Info.Gety2(), out y4);
                        drawInstance.DrawLine(ref imgbgr, color, x3, y3, x4, y4);                       
                    }
                    if (Info.GetDefectClass() == 2)
                    {
                        int x3, y3;
                        x3 = TransformXY(Info.Getx1(), Info.Gety1(), out y3);
                        drawInstance.DrawText(ref imgbgr, color, font, Info.GetText(), x3, y3);
                    }
                    if (Info.GetDefectClass() == 3)
                    {
                        int x3, y3;
                        x3 = TransformXY(Info.Getx1(), Info.Gety1(), out y3);
                        drawInstance.DrawCircle(ref imgbgr, color, x3, y3, Info.Getx2(), 2);
                    }
                }
                defectInfo.Clear();
            }
            return imgbgr;            
        }

        private Image<Bgr, byte> DrawDefect(Image<Bgr, byte> imgbgr, int n)
        {
            var color = System.Drawing.Color.Red;
            var font = new Font("黑体", 25, System.Drawing.FontStyle.Bold);
            int x3, x4, y3, y4;
            int k = 0;
            for (int i = 0;i<30 ; i++)
            {
                if (pinjieshow[8 * i] == -1)
                {
                    k = i;
                    break;
                }
            }
            for (int i = 0; i < k; i++)
            {
                x3 = pinjieshow[8 * i];y3 = pinjieshow[8 * i + 1];x4 = pinjieshow[8 * i + 2];y4 = pinjieshow[8 * i + 3];
                Parameter = pinjieshow[8 * i + 4]; qlength = pinjieshow[8 * i + 5]; qwidth = pinjieshow[8 * i + 6]; classflag = pinjieshow[8 * i + 7];                  
                if (classflag <= 0)
                {
                    if (x3 > x4)
                    {
                        int x;
                        x = x3;
                        x3 = x4;
                        x4 = x;
                    }
                    if (y3 > y4)
                    {
                        int y;
                        y = y3;
                        y3 = y4;
                        y4 = y;
                    }
                    int wi = (x4 - x3);
                    int hi = (y4 - y3);
                    drawInstance.DrawRectangle(ref imgbgr,color, x3, y3, wi, hi);
                    //if (imgbgr.Width - x4 > 250 && imgbgr.Height - y4 > 250)
                    //    drawInstance.DrawText(ref imgbgr, color, font,
                    //    "面积：" + Parameter.ToString() + "mm2" + "\r\n" + "长度：" + qlength.ToString() + "mm" + "\r\n" + "宽度：" + qwidth.ToString() + "mm" + "\r\n",
                    //    x4, y4);
                    //else if (imgbgr.Width - x4 < 250 && imgbgr.Height - y4 > 250)
                    //    drawInstance.DrawText(ref imgbgr, color, font,
                    //    "面积：" + Parameter.ToString() + "mm2" + "\r\n" + "长度：" + qlength.ToString() + "mm" + "\r\n" + "宽度：" + qwidth.ToString() + "mm" + "\r\n", x4 - wi, y4);
                    //else if (imgbgr.Width - x4 < 250 && imgbgr.Height - y4 < 250)
                    //    drawInstance.DrawText(ref imgbgr, color, font,
                    //    "面积：" + Parameter.ToString() + "mm2" + "\r\n" + "长度：" + qlength.ToString() + "mm" + "\r\n" + "宽度：" + qwidth.ToString() + "mm" + "\r\n", x4 - wi - 250, y4 - hi);
                    //else
                    //    drawInstance.DrawText(ref imgbgr, color, font,
                    //    "面积：" + Parameter.ToString() + "mm2" + "\r\n" + "长度：" + qlength.ToString() + "mm" + "\r\n" + "宽度：" + qwidth.ToString() + "mm" + "\r\n", x4, y4 - hi);
                    drawInstance.DrawText(ref imgbgr, color, font, "面积：" + Parameter.ToString() + "mm2" + "\r\n" + "长度：" + qlength.ToString() + "mm" + "\r\n" + "宽度：", x4, y4);

                }
                if (classflag == 1)
                {
                    drawInstance.DrawLine(ref imgbgr, color, x3, y3, x4, y4);
                    drawInstance.DrawText(ref imgbgr, color,font,"长度：" + Parameter.ToString() + "mm", x4, y4);
                }
            }
            return imgbgr;
        }
  
        private Bitmap Inverse(Bitmap bmp)
        {
            BitmapData srcdat = bmp.LockBits(new System.Drawing.Rectangle(System.Drawing.Point.Empty, bmp.Size), ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb); // 锁定位图
            unsafe
            {
                byte* pix = (byte*)srcdat.Scan0; // 像素首地址
                for (int i = 0; i < srcdat.Stride * srcdat.Height; i++)
                {
                    pix[i] = (byte)(255 - pix[i]);
                }
                bmp.UnlockBits(srcdat); // 解锁
                return bmp;
            }
        }

           
        
        private void SaveTxt()//保存图像处理的变动
        {
            string a;
            if (!DefectView)
            {
                a = filepath.Trim() + "\\";
            }
            else
            {
                a = filepath.Substring(0, filepath.LastIndexOf("评定图像")) + "\\";
            }
            FileStream fs2 = new FileStream(a + "StateTxt.txt", FileMode.Open, FileAccess.ReadWrite);//创建写入文件 
            StreamWriter sw2 = new StreamWriter(fs2);
            sw2.WriteLine(s1);//开始写入值
            sw2.WriteLine(s2);
            sw2.WriteLine(s3);
            sw2.WriteLine(s4);
            sw2.WriteLine(s5);
            sw2.WriteLine(dcmImage.WindowCenter.ToString());
            sw2.WriteLine(dcmImage.WindowWidth.ToString());
            //sw.WriteLine("1");
            sw2.Close();
            fs2.Close();
            fs2.Dispose();
        }

        private Image<Gray, ushort> fudiao(Bitmap myBitmap)
        {
            BitmapData srcdat = myBitmap.LockBits(new System.Drawing.Rectangle(System.Drawing.Point.Empty, myBitmap.Size), ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format8bppIndexed); // 锁定位图
            unsafe
            {
                byte* pix = (byte*)srcdat.Scan0; // 像素首地址
                for (int i = 0; i < (srcdat.Stride) * (srcdat.Height - 4) - 4; i++)
                {

                    int n = pix[i] + pix[i + srcdat.Stride + 1] - pix[i + srcdat.Stride * 2 + 2] - pix[i + srcdat.Stride * 3 + 3] + 128;
                    if (n < 0)
                    {
                        pix[i] = 0;
                    }
                    else if (n > 255)
                    {
                        pix[i] = 255;
                    }
                    else
                    {
                        pix[i] = (byte)n;
                    }


                }
                myBitmap.UnlockBits(srcdat); // 解锁
            }
            Image<Gray, ushort> img;
            img = new Image<Gray, ushort>(myBitmap);
            return img;
        }

        private void InitImgBoxSize(ref DicomImage dcmimg)
        {
            int Width = dcmimg.Width;
            int Height = dcmimg.Height ;

               Scroll.ScrollToHome();
               tlt.X = 0;
               tlt.Y = 0;
               if (Width > Height)
               {
                   imgbox.Width = 512 * (double)Width / (double)Height;
                   imgbox.Height = 512;
               }
               else
               {
                   imgbox.Width = 512;
                   imgbox.Height = 512 * (double)Height / (double)Width;
               }
               BaseWinWidth = imgbox.Width;
               BaseWinHeight = imgbox.Height;

        }
        public void AdjustImageBoxSize(BitmapSource bps)
        {

                Scroll.ScrollToHome();
                //tlt.X = 0;      //屏蔽掉以后可以保证每次调用refreshdisplay时图像不会重新刷新到最左边
                //tlt.Y = 0;


            //Action actionZ1 = () =>
            //{

            Debug.WriteLine((imgbox.Height / bps.Height - imgbox.Width / bps.Width).ToString() + " " + (imgbox.Width / bps.Height - imgbox.Height / bps.Width).ToString());
            //};
            //bps.Dispatcher.BeginInvoke(actionZ1);




            //Action actionY = () =>
            //{
                if (bps.Width > bps.Height)
                {
                    //设立阈值是应为double在放缩运算后存在误差
                    if (Math.Abs(imgbox.Height / bps.Height - imgbox.Width / bps.Width) < 0.01 || Math.Abs(imgbox.Width / bps.Height - imgbox.Height / bps.Width) < 0.01)
                    {
                        if (imgbox.Height > imgbox.Width)
                        {
                            double tmp = imgbox.Width;
                            imgbox.Width = imgbox.Height;
                            imgbox.Height = tmp;
                            if (BaseWinHeight > BaseWinWidth)
                            {
                                double t = BaseWinHeight;
                                BaseWinHeight = BaseWinWidth;
                                BaseWinWidth = t;
                            }
                        }
                    }
                    else//用于拼图后imagbox比例的的恢复
                    {
                        imgbox.Width = 512 * bps.PixelWidth / bps.PixelHeight;
                        imgbox.Height = 512;
                        BaseWinWidth = imgbox.Width;
                        BaseWinHeight = imgbox.Height;
                    }
                }
                else
                {
                    //设立阈值是应为double在放缩运算后存在误差
                    if (Math.Abs(imgbox.Height / bps.Height - imgbox.Width / bps.Width) < 0.01 || Math.Abs(imgbox.Width / bps.Height - imgbox.Height / bps.Width) < 0.01)
                    {
                        if (imgbox.Width > imgbox.Height)
                        {
                            double tmp = imgbox.Width;
                            imgbox.Width = imgbox.Height;
                            imgbox.Height = tmp;
                            if (BaseWinWidth > BaseWinHeight)
                            {
                                double t = BaseWinHeight;
                                BaseWinHeight = BaseWinWidth;
                                BaseWinWidth = t;
                            }
                        }
                    }
                    else
                    {
                        imgbox.Width = 512;
                        imgbox.Height = 512 * bps.PixelHeight / bps.PixelWidth;
                        BaseWinWidth = imgbox.Width;
                        BaseWinHeight = imgbox.Height;
                    }
                }

            //};
            //bps.Dispatcher.BeginInvoke(actionY);

        }
        public int sharpRatioPJ;
        

        private void showdefect_click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                var information1 = new information();
                information1.LoadTxt(MainWindow.filepath);
                string[] stringarray1 = information1.stringarray;




                if (dcmImage == null) return;
                IsSd = !IsSd;
                if (IsSd)
                {
                    flagtext.IsEnabled = true;
                    flagtext.Visibility = Visibility.Visible;
                    flagtext1.IsEnabled = true;
                    flagtext1.Visibility = Visibility.Visible;
                    //flagtext.Foreground  
                    //System.Windows.Media.Brush br = new SolidColorBrush(System.Windows.Media.Color.FromArgb(40, 0, 0, 0));
                    //flagtext.Background = br;
                    string datetime1 = null;
                    string seriesnumber = null;
                    string instancenumber = null;
                    try
                    {
                        string datetime0 = dcmDataSet.Get<String>(DicomTag.AcquisitionDateTime);
                        if (datetime0 != null)
                        {
                            datetime1 = datetime0.Substring(0, 4) + "年" + datetime0.Substring(4, 2) + "月" + datetime0.Substring(6, 2) + "日" + datetime0.Substring(8, 2) + "时"
                                    + datetime0.Substring(10, 2) + "分" + datetime0.Substring(12, 2) + "秒";
                        }
                    }

                    catch { }

                    try
                    {
                        seriesnumber = dcmDataSet.Get<String>(DicomTag.SeriesNumber);
                    }
                    catch { }
                    try
                    {
                        instancenumber = dcmDataSet.Get<String>(DicomTag.InstanceNumber);
                    }
                    catch { }
                    string flg = "文件名：" + filepath + pictureName + "\r\n" +
                                  "数据采集时间：" + datetime1 + "\r\n" +
                                  "GPS：" + "\r\n" +
                                  "图上坐标：" + "  " + "x：" + "  " + "y：" + "\r\n" +
                                  "图像尺寸：" + dcmImage.Width.ToString() + "*" + dcmImage.Height.ToString() + "\r\n" +
                                  "当前灰度：" + "\r\n";
                    string flg1 = "用户单位:" + "  " + stringarray1[5] + "\r\n" +
                                  "焊口编号:" + "  " + stringarray1[78] + "\r\n" +
                                  "射线电压:" + "  " + stringarray1[62] + "\r\n" +
                                  "射线电流:" + "  " + stringarray1[70] + "\r\n" +
                                  "厚度:" + "  " + stringarray1[17] + "\r\n" +
                                  "检测器类型:" + "  " + stringarray1[77] + "\r\n" +
                                  "距离:" + "  " + stringarray1[51] + "\r\n" +
                                  "检测类别: DR" + "\r\n" +
                                  "序列号：" + seriesnumber + "\r\n" +
                                  "影像号：" + instancenumber + "\r\n";
                    flagtext.Text = flg;
                    flagtext1.Text = flg1;
                }
                else
                {
                    flagtext.Visibility = Visibility.Collapsed;
                    flagtext.IsEnabled = false;
                    flagtext1.Visibility = Visibility.Collapsed;
                    flagtext1.IsEnabled = false;
                }
                information1.Close();
            }
        
            else
            {
                var information1 = new information();
                information1.LoadTxt(MainWindow.filepath);
                string[] stringarray1 = information1.stringarray;




                if (dcmImage == null) return;
                IsSd = !IsSd;
                if (IsSd)
                {
                    flagtext.IsEnabled = true;
                    flagtext.Visibility = Visibility.Visible;
                    flagtext1.IsEnabled = true;
                    flagtext1.Visibility = Visibility.Visible;
                    //flagtext.Foreground  
                    //System.Windows.Media.Brush br = new SolidColorBrush(System.Windows.Media.Color.FromArgb(40, 0, 0, 0));
                    //flagtext.Background = br;
                    string datetime1 = null;
                    string seriesnumber = null;
                    string instancenumber = null;
                    try
                    {
                        string datetime0 = dcmDataSet.Get<String>(DicomTag.AcquisitionDateTime);
                        if (datetime0 != null)
                        {
                            datetime1 = datetime0.Substring(0, 4) + "年" + datetime0.Substring(4, 2) + "月" + datetime0.Substring(6, 2) + "日" + datetime0.Substring(8, 2) + "时"
                                    + datetime0.Substring(10, 2) + "分" + datetime0.Substring(12, 2) + "秒";
                        }
                    }

                    catch { }

                    try
                    {
                        seriesnumber = dcmDataSet.Get<String>(DicomTag.SeriesNumber);
                    }
                    catch { }
                    try
                    {
                        instancenumber = dcmDataSet.Get<String>(DicomTag.InstanceNumber);
                    }
                    catch { }
                    string flg = "文件名：" + filepath + pictureName + "\r\n" +
                                  "数据采集时间：" + datetime1 + "\r\n" +
                                  "GPS：" + "\r\n" +
                                  "图上坐标：" + "  " + "x：" + "  " + "y：" + "\r\n" +
                                  "图像尺寸：" + dcmImage.Width.ToString() + "*" + dcmImage.Height.ToString() + "\r\n" +
                                  "当前灰度：" + "\r\n";
                    string flg1 = "用户单位:" + "  " + stringarray1[5] + "\r\n" +
                                  "焊口编号:" + "  " + stringarray1[78] + "\r\n" +
                                  "射线电压:" + "  " + stringarray1[62] + "\r\n" +
                                  "射线电流:" + "  " + stringarray1[70] + "\r\n" +
                                  "厚度:" + "  " + stringarray1[17] + "\r\n" +
                                  "检测器类型:" + "  " + stringarray1[77] + "\r\n" +
                                  "距离:" + "  " + stringarray1[51] + "\r\n" +
                                  "检测类别: DR" + "\r\n" +
                                  "序列号：" + seriesnumber + "\r\n" +
                                  "影像号：" + instancenumber + "\r\n";
                    flagtext.Text = flg;
                    flagtext1.Text = flg1;
                }
                else
                {
                    flagtext.Visibility = Visibility.Collapsed;
                    flagtext.IsEnabled = false;
                    flagtext1.Visibility = Visibility.Collapsed;
                    flagtext1.IsEnabled = false;
                }
                information1.Close();
            }

            
        }
        private void btn_EqualizeHist_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (dcmImage == null) return;
            imgProcess.SwitchOnHistEqualize(true);
            CancelKey.Add("histEqualize");
            RefreshImgDisplay(ref dcmImage);
        }       
        private void Btn_contrastdown_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (dcmImage == null) return;
            imgProcess.AdjustContrastRatio(-1);
            CancelKey.Add("contrastDown");
            RefreshImgDisplay(ref dcmImage);
        }

        private void Btn_contrastup_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (dcmImage == null) return;
            imgProcess.AdjustContrastRatio(+1);
            CancelKey.Add("contrastUp");
            RefreshImgDisplay(ref dcmImage);            
        }       

        private void Btn_brightnessdown_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (dcmImage == null) return;
            imgProcess.AdjustBrightRatio(-1);
            CancelKey.Add("brightDown");
            RefreshImgDisplay(ref dcmImage);
        }

        private void Btn_brightnessup_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (dcmImage == null) return;
            imgProcess.AdjustBrightRatio(+1);
            CancelKey.Add("brightUp");
            RefreshImgDisplay(ref dcmImage);
        }
        #endregion

        #region //image控件操作
        private void Scroll_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {            
        }       
        private void Scroll_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)//记录矩形标定框位置
        {           
            savepoint = e.GetPosition(imgbox);           
            if (rec1 == true || rec2 == true || rec3 == true)
            {
                //clearSaveRect();               
                if (rec1 == true)
                {                   
                    u = ra / correct * trans;
                    t = ra / correct * trans;                  
                }
                if (rec2 == true)
                {                    
                    u = ra / correct * trans;
                    t = rb / correct * trans;                    
                }
                if (rec3 == true)
                {                   
                    u = ra / correct * trans;
                    t = rc / correct * trans;                   
                }
                if (savepoint.X - t - z < 0) savepoint.X = 0;
                if (savepoint.Y - u - z < 0) savepoint.Y = 0;
                if (savepoint.X - z >= imgbox.Width) savepoint.X = imgbox.Width - 0.00001;
                if (savepoint.Y - z >= imgbox.Height) savepoint.Y = imgbox.Height - 0.00001;                
                int x1, y1;
                int x2, y2;
                int n1 = 0, n2 = 0;
                if (pinjie == false)
                {
                    x1 = (int)(bitSourse.PixelWidth / imgbox.Width * (savepoint.X - t - z));
                    y1 = (int)(bitSourse.PixelWidth / imgbox.Width * (savepoint.Y - u - z));
                    x2 = (int)(bitSourse.PixelWidth / imgbox.Width * (savepoint.X - z));
                    y2 = (int)(bitSourse.PixelWidth / imgbox.Width * (savepoint.Y - z));                    
                }
                else
                {                    
                    int xw = picName.Count;
                    //if (xw > 5) xw = 5;
                    n1 = (int)((savepoint.X - t - z) / (imgbox.Width / xw));
                    n2 = (int)((savepoint.X - z) / (imgbox.Width / xw));
                    x1 = (int)(((savepoint.X - t - z) % (imgbox.Width / xw)) * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width / xw)) + cutSize;
                    y1 = (int)((savepoint.Y - u - z) * (BaseBps.PixelHeight - 2 * cutSize) / imgbox.Height) + cutSize;
                    x2 = (int)(((savepoint.X - z) % (imgbox.Width / xw)) * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width / xw)) + cutSize;
                    y2 = (int)((savepoint.Y - z) * (BaseBps.PixelHeight - 2 * cutSize) / imgbox.Height) + cutSize;
                   // Debug.WriteLine("n1:" + n1.ToString() + " singlebox:" + (imgbox.Width / xw).ToString() + " c*t:" + (cutSize * trans).ToString());

                    px1 = (int)((savepoint.X - t - z) * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width / xw));
                    px2 = (int)((savepoint.X - z) * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width / xw));
                    py1 = (int)((savepoint.Y - u - z) * (BaseBps.PixelHeight - 2 * cutSize) / imgbox.Height); 
                    py2 = (int)((savepoint.Y - z) * (BaseBps.PixelHeight - 2 * cutSize) / imgbox.Height);
                    if (n1 != n2)
                    {
                        MessageBox.Show("请在同一张图片上标注！");
                        return;
                    }
                }

                int a = x1, b = y1, c = x2, d = y2;
                x1 = CorrectXY(a, b, out y1);
                x2 = CorrectXY(c, d, out y2);
                defectX1 = x1; defectX2 = x2; defectY1 = y1; defectY2 = y2;
                
                if (MarkSwitch&&(rec1||rec2||rec3)&&!IsAw)//修改 
                {
                    Save.IsEnabled = true;
                    isQueadd = true;
                    
                    if (pinjie == false)
                    {
                        Picname = pictureName;
                        //if (rec1||rec2||rec3)
                        int co = 8 * NumofDefs;
                        defcoords[co] = defectX1; defcoords[co + 1] = defectY1; defcoords[co + 2] = defectX2; defcoords[co + 3] = defectY2; defcoords[co + 7] = 0;
                        if (rec1) { defcoords[co + 4] = 100; defcoords[co + 5] = 10; defcoords[co + 6] = 10; }
                        else if (rec2) { defcoords[co + 4] = 200; defcoords[co + 5] = 20; defcoords[co + 6] = 10; }
                        else if (rec3) { defcoords[co + 4] = 100; defcoords[co + 5] = 30; defcoords[co + 6] = 10; }
                        
                        RefreshImgDisplay(ref dcmImage);                
                        
                    }
                    else
                    {
                        string pn = Picname;
                        Picname = picName[n1 + n0];
                        //if (rec1 || rec2 || rec3)
                        {
                            int cp = 8 * NumofDefs;
                            pinjieshow[cp] = px1; pinjieshow[cp + 1] = py1; pinjieshow[cp + 2] = px2; pinjieshow[cp + 3] = py2;pinjieshow[cp + 7] = 0;
                            if (rec1) { pinjieshow[cp + 4] = 100; pinjieshow[cp + 5] = 10; pinjieshow[cp + 6] = 10; }
                            else if (rec2) { pinjieshow[cp + 4] = 200; pinjieshow[cp + 5] = 20; pinjieshow[cp + 6] = 10; }
                            else if (rec3) { pinjieshow[cp + 4] = 300; pinjieshow[cp + 5] = 30; pinjieshow[cp + 6] = 10; }
                            RefreshImgDisplayPintu(ref imgPinJieResult16);
                            //Image <Bgr, byte> grayd = DrawDefect(new Image<Bgr, byte>(pintu.Bitmap), 0);                            
                            //bitSourse = imgFormatTransform.ImageToBitmapSource(grayd);
                            //imgbox.Source = bitSourse;
                        }
                    }
                    NumofDefs = NumofDefs + 1;
                    if (NumofDefs == 2&& CancelType == "def") { MessageBox.Show("评定框超出标准数量，如属误操作请及时撤销"); return; }
                    CancelType = "def";                    
                }                
            }
        }

        private void Scroll_MouseMove(object sender, MouseEventArgs e)//生成矩形标定框，因为框有面积，所以写在外部控件以完成对图像的覆盖
        {
            #region//显示标尺            
            savepoint = e.GetPosition(imgbox);
            int z = 5;
            scrollpoint = e.GetPosition(BaseCan);
            if (rec1 == true)
            {
                if ((Rectline5 == null) && (Rectline6 == null) && (Rectline7 == null) && (Rectline8 == null))
                {
                    InitRulerRect();
                }
                double u = ra / correct * trans;
                LocateRulerRect((int)(scrollpoint.X - u - z), (int)(scrollpoint.Y - u - z), (int)(scrollpoint.X - z), (int)(scrollpoint.Y - z));
            }
            if (rec2 == true)
            {
                if ((Rectline5 == null) && (Rectline6 == null) && (Rectline7 == null) && (Rectline8 == null))
                {
                    InitRulerRect();
                }
                double u = ra / correct * trans;
                double t = rb / correct * trans;
                LocateRulerRect((int)(scrollpoint.X - t - z), (int)(scrollpoint.Y - u - z), (int)(scrollpoint.X - z), (int)(scrollpoint.Y - z));
            }
            if (rec3 == true)
            {
                if ((Rectline5 == null) && (Rectline6 == null) && (Rectline7 == null) && (Rectline8 == null))
                {
                    InitRulerRect();
                }
                double u = ra / correct * trans;
                double t = rc / correct * trans;
                LocateRulerRect((int)(scrollpoint.X - t - z), (int)(scrollpoint.Y - u - z), (int)(scrollpoint.X - z), (int)(scrollpoint.Y - z));
            }

            if ((round1 == true))
            {
                if ((e1 == null) && (e2 == null) && (e3 == null) && (e4 == null) && (e6 == null) && (e8 == null) && (e03 == null) && (e04 == null) && (e05 == null) && (e06 == null) && (e07 == null))
                {
                    InitEllipse();

                }
                LocateEllipses(2);

                if (DValue == null)
                {
                    DValue = new TextBox();
                    this.BaseCan.Children.Add(DValue);
                    BaseCan.RegisterName("newText1", DValue);
                    System.Windows.Media.Brush br = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
                    DValue.Background = br;
                    DValue.BorderBrush = br;
                    DValue.Foreground = new SolidColorBrush(Colors.OrangeRed); ;
                    DValue.IsReadOnly = true;
                }
                //DValue.SetValue(Canvas.LeftProperty, (double)(scrollpoint.X +30));
                //DValue.SetValue(Canvas.TopProperty, (double)(scrollpoint.Y -80));
                DValue.Margin = new Thickness(scrollpoint.X + 20, scrollpoint.Y - 70, scrollpoint.X + 150, scrollpoint.Y - 10);
                DValue.Text = "   圆形标尺直径" + "\n" + "0.5    0.7" + "\n" + "1      2      3      4" + "\n" + "    5              6  ";

            }
            #endregion

       #region//显示伸缩标尺

            #region//横向标尺
            if (Ishbiao && !ismove)//决定绘制水平标尺的标志变量条件
            {
                #region//横轴
                if (HRulerLine_Axis == null)//未初始化水平标尺横轴线段
                {
                    InitHRulerLine_Axis();     //初始化水平标尺上横轴线段（包括颜色、粗细、不透明度）                   
                }
                double u = 100 / correct * trans;//标尺水平横轴线段长度100mm
                LocateHRulerLine_Axis((int)(scrollpoint.X - u), (int)(scrollpoint.X), (int)(scrollpoint.Y - z));//每次鼠标有操作，就刷新水平标尺上横轴线段的两点坐标
                #endregion

                #region//刻度 
                if (HRulerLine_Ticks == null)//未初始化水平标尺刻度线段数组
                {
                    InitHRulerLine_Ticks();    //初始化水平标尺刻度线段数组及其元素线段（包括颜色、粗细、不透明度）
                }

                double v = 5 / correct * trans;//标尺水平横轴整十数刻度线段长度5mm
                double w = 4 / correct * trans;//标尺水平横轴整五数刻度线段长度4mm
                double s = 3 / correct * trans;//标尺水平横轴非整十整五数刻度线段长度3mm

                int[] x1 = new int[101];//初始化水平标尺刻度线段两端的横坐标数组x1
                int[] y1 = new int[101];//初始化水平标尺刻度线段下端的纵坐标数组y1
                int[] y2 = new int[101];//初始化水平标尺刻度线段上端的纵坐标数组y2

                for (int i = 0; i <= 100; i++)
                {
                    x1[i] = (int)((scrollpoint.X - u) + (i * trans / correct));//(i/100)*u=i*trans/correct,刻度线段距离标尺左端的长度

                    y1[i] = (int)(scrollpoint.Y - z - 2);

                    if ((i == 0) || (i % 10 == 0))//刻度值为整十数的刻度线段(加上第一个刻度)
                    {
                        y2[i] = (int)(scrollpoint.Y - z - 4 - v);//刻度线段长为7
                    }

                    else if ((i % 2 != 0) && (i % 5 == 0))//刻度值为整五数但非整十数的刻度线段
                    {
                        y2[i] = (int)(scrollpoint.Y - z - 4 - w);//刻度线段长为6
                    }

                    else//其余刻度线段
                    {
                        y2[i] = (int)(scrollpoint.Y - z - 4 - s);//刻度线段长为5
                    }
                }

                LocateHRulerLine_Ticks(x1, y1, y2);//每次鼠标有操作，就刷新水平标尺上所有刻度线段的两点坐标  
                #endregion   

                #region//刻度数值文本框
                if (HTickNum == null)//未初始化水平标尺上的刻度数值文本框数组
                {
                    InitHTickNum();//初始化水平标尺上的刻度数值文本框数组及其元素文本框属性
                }

                double[] MarginLeft = new double[11];//刻度数值文本框左边距
                double[] MarginRight = new double[11];//刻度数值文本框右边距
                double MarginTop;//刻度数值文本框上边距
                double MarginBottom;//刻度数值文本框下边距

                MarginLeft[0] = scrollpoint.X - u - 3;
                MarginRight[0] = BaseCan.ActualWidth - (scrollpoint.X - u) + 3;

                for (int mi = 1; mi <= 9; mi++)
                {
                    MarginLeft[mi] = (scrollpoint.X - u) + (mi * 10 * trans / correct) - 4;
                    MarginRight[mi] = BaseCan.ActualWidth - ((scrollpoint.X - u) + (mi * 10 * trans / correct)) + 4;
                }

                MarginLeft[10] = scrollpoint.X - u + (10 * 10 * trans / correct) - 7;
                MarginRight[10] = BaseCan.ActualWidth - ((scrollpoint.X - u) + (10 * 10 * trans / correct)) + 7;

                MarginTop = scrollpoint.Y - z - 4 - v - 17;
                MarginBottom = BaseCan.ActualHeight - scrollpoint.Y + z + 4 + v + 12;

                LocateHTickNum(MarginLeft, MarginTop, MarginRight, MarginBottom);//每次鼠标有操作，就刷新水平标尺刻度数值文本框的边距  
                #endregion  
            }

            #endregion

            #region//纵向标尺
            if (Isvbiao && !ismove)//决定绘制纵向标尺的标志变量条件
            {
                #region//纵轴
                if (VRulerLine_Axis == null)//未初始化纵向标尺纵轴线段
                {
                    InitVRulerLine_Axis();     //初始化纵向标尺上纵轴线段（包括颜色、粗细、不透明度）                   
                }
                double a = 100 / correct * trans;//标尺纵向纵轴线段长度100mm
                LocateVRulerLine_Axis((int)(scrollpoint.X - z), (int)(scrollpoint.Y), (int)(scrollpoint.Y - a));//每次鼠标有操作，就刷新纵向标尺上纵轴线段的两点坐标
                #endregion

                #region//刻度
                if (VRulerLine_Ticks == null)
                {
                    InitVRulerLine_Ticks();    //初始化纵向标尺刻度线段数组及其元素线段（包括颜色、粗细、不透明度）
                }

                double b = 5 / correct * trans;//标尺纵向纵轴整十数刻度线段长度5mm
                double c = 4 / correct * trans;//标尺纵向纵轴整五数刻度线段长度4mm
                double d = 3 / correct * trans;//标尺纵向纵轴非整十整五数刻度线段长度3mm

                int[] x1 = new int[101];//初始化纵向标尺刻度线段两端的横坐标数组x1
                int[] x2 = new int[101];//初始化纵向标尺刻度线段下端的横坐标数组y1
                int[] y1 = new int[101];//初始化纵向标尺刻度线段上端的纵坐标数组y2

                for (int i = 0; i <= 100; i++)
                {
                    y1[i] = (int)(scrollpoint.Y - (i * trans / correct));
                    x2[i] = (int)(scrollpoint.X - z);

                    if ((i == 0) || (i % 10 == 0))////刻度值为整十数的刻度线段(加上第一个刻度)
                    {
                        x1[i] = (int)(scrollpoint.X - z - 4 - b);//刻度线段长为7
                    }

                    else if ((i % 2 != 0) && (i % 5 == 0))//刻度值为整五数但非整十数的刻度线段
                    {
                        x1[i] = (int)(scrollpoint.X - z - 4 - c);//刻度线段长为6
                    }

                    else//其余刻度线段
                    {
                        x1[i] = (int)(scrollpoint.X - z - 4 - d);//刻度线段长为5
                    }
                }

                LocateVRulerLine_Ticks(x1, x2, y1);//每次鼠标有操作，就刷新纵向标尺上所有刻度线段的两点坐标
                #endregion

                #region//刻度数值文本框
                if (VTickNum == null)//未初始化纵向标尺上的刻度数值文本框数组
                {
                    InitVTickNum();//初始化纵向标尺上的刻度数值文本框数组及其元素文本框属性
                }

                double[] MarginTop = new double[11];//刻度数值文本框上边距
                double[] MarginBottom = new double[11];//刻度数值文本框下边距
                double[] MarginLeft = new double[11];//刻度数值文本框左边距
                double[] MarginRight = new double[11];//刻度数值文本框右边距

                MarginTop[0] = scrollpoint.Y - 8;
                MarginBottom[0] = BaseCan.ActualHeight - scrollpoint.Y + 1;

                for (int mi = 1; mi <= 9; mi++)
                {
                    MarginTop[mi] = (scrollpoint.Y) - (mi * 10 * trans / correct) - 8;
                    MarginBottom[mi] = BaseCan.ActualHeight - ((scrollpoint.Y) - (mi * 10 * trans / correct)) + 8;
                }

                MarginTop[10] = scrollpoint.Y - (10 * 10 * trans / correct) - 8;
                MarginBottom[10] = BaseCan.ActualHeight - ((scrollpoint.Y) - (10 * 10 * trans / correct)) + 8;

                for (int mi = 0; mi <= 10; mi++)
                {
                    if (mi == 0)
                    {
                        MarginLeft[mi] = scrollpoint.X - 4 - b - 26;
                        MarginRight[mi] = BaseCan.ActualWidth - scrollpoint.X + 4 + b + 24;
                    }

                    else
                    {
                        MarginLeft[mi] = scrollpoint.X - 4 - b - 22;
                        MarginRight[mi] = BaseCan.ActualWidth - scrollpoint.X + 4 + b + 20;
                    }
                }

                LocateVTickNum(MarginLeft, MarginTop, MarginRight, MarginBottom);//每次鼠标有操作，就刷新纵向标尺刻度数值文本框的边距  
                #endregion

            }
            #endregion
        }

        #region//对HRulerLine_Axis的所有操作

        private void InitHRulerLine_Axis()//初始化水平标尺上水平横轴线段的信息（包括颜色、粗细、不透明度），并添加进ScrollViewer控件里
        {
            HRulerLine_Axis = new Line();
            HRulerLine_Axis.Stroke = new SolidColorBrush(Colors.OrangeRed);
            HRulerLine_Axis.StrokeThickness = 3;
            HRulerLine_Axis.Opacity = 0.9;
            this.BaseCan.Children.Add(HRulerLine_Axis);
            BaseCan.RegisterName("new_HRulerLine_Axis", HRulerLine_Axis);//注册新生成的HRulerLine_Axis名字以便删除
        }

        private void LocateHRulerLine_Axis(int x1, int x2, int y)//定义水平标尺上的水平横轴线段的两端坐标
        {
            HRulerLine_Axis.X1 = x1;
            HRulerLine_Axis.Y1 = y;
            HRulerLine_Axis.X2 = x2;
            HRulerLine_Axis.Y2 = y;
        }

        private void clearHRulerLine_Axis()//移除水平标尺上的横轴线段
        {
            Line HRulerLine_Axis = BaseCan.FindName("new_HRulerLine_Axis") as Line;//找到新添加的HRulerLine_Axis
            if (HRulerLine_Axis != null)
            {
                BaseCan.Children.Remove(HRulerLine_Axis);    //移除对应的HRulerLine_Axis
                BaseCan.UnregisterName("new_HRulerLine_Axis");//注销对应HRulerLine_Axis的名字
            }
        }
        #endregion

        #region//对HRulerLine_Ticks的所有操作
        private void InitHRulerLine_Ticks()//初始化水平标尺上101条刻度线数组及其所有元素线段（包括颜色、粗细、不透明度），并添加进ScrollViewer控件里
        {
            HRulerLine_Ticks = new Line[101]; //初始化水平标尺上101条刻度线段数组

            for (int i = 0; i <= 100; i++)
            {
                HRulerLine_Ticks[i] = new Line();//初始化水平标尺上101条刻度线数组中的所有元素刻度线段
                HRulerLine_Ticks[i].Stroke = new SolidColorBrush(Colors.OrangeRed);//颜色
                HRulerLine_Ticks[i].Opacity = 0.9;//非透明度

                if ((i == 0) || (i % 10 == 0))//刻度值为整十数的刻度线段(还有第一个刻度)i
                {
                    HRulerLine_Ticks[i].StrokeThickness = 1;
                }

                else if ((i % 2 != 0) && (i % 5 == 0))//刻度值为整五数的刻度线段
                {
                    HRulerLine_Ticks[i].StrokeThickness = 1;
                }

                else//其余刻度
                {
                    HRulerLine_Ticks[i].StrokeThickness = 1;
                }

                this.BaseCan.Children.Add(HRulerLine_Ticks[i]);//将新生成的刻度元素线段HRulerLine_Ticks[i]添加进控件里               
                BaseCan.RegisterName("new_HRulerLine_Ticks" + Convert.ToString(i), HRulerLine_Ticks[i]);//注册新生成的刻度元素线段HRulerLine_Ticks[i]以便删除
            }
        }

        private void LocateHRulerLine_Ticks(int[] x1, int[] y1, int[] y2)//定义水平标尺上的所有垂直刻度线段的两端坐标
        {
            for (int i = 0; i <= 100; i++)
            {
                HRulerLine_Ticks[i].X1 = x1[i];
                HRulerLine_Ticks[i].Y1 = y1[i];
                HRulerLine_Ticks[i].X2 = x1[i];
                HRulerLine_Ticks[i].Y2 = y2[i];
            }
        }

        private void clearHRulerLine_Ticks()//移除水平标尺上的101条刻度线段
        {
            HRulerLine_Ticks = new Line[101];

            for (int i = 0; i <= 100; i++)
            {
                HRulerLine_Ticks[i] = BaseCan.FindName("new_HRulerLine_Ticks" + Convert.ToString(i)) as Line;//找到新添加的HRulerLine_Tick[i]
                if (HRulerLine_Ticks[i] != null)
                {
                    BaseCan.Children.Remove(HRulerLine_Ticks[i]);    //移除对应的HRulerLine_Tick[i]
                    BaseCan.UnregisterName("new_HRulerLine_Ticks" + Convert.ToString(i));//注销对应HRulerLine_Tick[i]的名字
                }
            }
        }

        #endregion

        #region//对HTickNum的所有操作
        private void InitHTickNum()//初始化水平标尺上的刻度数值文本框数组及其元素文本框属性
        {
            HTickNum = new TextBlock[11];//实例化水平标尺刻度数值文本框数组

            for (int i = 0; i <= 10; i++)
            {
                HTickNum[i] = new TextBlock();//初始化刻度线段数组中的所有元素刻度线段
                HTickNum[i].Text = Convert.ToString(i);//刻度数值文本
                HTickNum[i].FontSize = 12;////刻度数值文本字号
                HTickNum[i].Foreground = new SolidColorBrush(Colors.OrangeRed);//刻度数值文本颜色
                this.BaseCan.Children.Add(HTickNum[i]);
                BaseCan.RegisterName("HTickNum_" + Convert.ToString(i), HTickNum[i]);
            }
        }

        private void LocateHTickNum(double[] m1, double m2, double[] m3, double m4)
        {
            for (int i = 0; i <= 10; i++)
            {
                HTickNum[i].Margin = new Thickness(m1[i], m2, m3[i], m4);//水平标尺刻度数值文本框的边距
            }
        }

        private void clearHTickNum()
        {
            HTickNum = new TextBlock[11];//再次实例化，否则有BUG

            for (int i = 0; i <= 10; i++)
            {
                HTickNum[i] = BaseCan.FindName("HTickNum_" + Convert.ToString(i)) as TextBlock;
                if (HTickNum[i] != null)
                {
                    BaseCan.Children.Remove(HTickNum[i]);//移除刻度数值文本框
                    BaseCan.UnregisterName("HTickNum_" + Convert.ToString(i));//注销刻度数值文本框名字
                }
            }
        }
        #endregion

        #region//对水平标尺的所有线段及数值文本框逆实例化
        private void clearHRulerLines()
        {
            HRulerLine_Axis = null;//无条件对HRulerLine_Axis逆实例化
            HRulerLine_Ticks = null;//无条件对HRulerLine_Ticks逆实例化           
        }

        private void clearHTextBlock()
        {
            HTickNum = null;//无条件对HTickNum逆实例化
        }
        #endregion

        #region//对VRulerLine_Axis的所有操作
        private void InitVRulerLine_Axis()//初始化纵向标尺上纵向纵轴线段的信息（包括颜色、粗细、不透明度），并添加进ScrollViewer控件里
        {
            VRulerLine_Axis = new Line();
            VRulerLine_Axis.Stroke = new SolidColorBrush(Colors.OrangeRed);
            VRulerLine_Axis.StrokeThickness = 3;
            VRulerLine_Axis.Opacity = 0.9;
            this.BaseCan.Children.Add(VRulerLine_Axis);
            BaseCan.RegisterName("new_VRulerLine_Axis", VRulerLine_Axis);//注册新生成的VRulerLine_Axis名字以便删除
        }

        private void LocateVRulerLine_Axis(int x1, int y1, int y2)//定义纵向标尺上的纵向纵轴线段的两端坐标
        {
            VRulerLine_Axis.X1 = x1;
            VRulerLine_Axis.Y1 = y1;
            VRulerLine_Axis.X2 = x1;
            VRulerLine_Axis.Y2 = y2;
        }

        private void clearVRulerLine_Axis()//移除纵向标尺上的纵轴线段
        {
            Line VRulerLine_Axis = BaseCan.FindName("new_VRulerLine_Axis") as Line;//找到新添加的VRulerLine_Axis
            if (VRulerLine_Axis != null)
            {
                BaseCan.Children.Remove(VRulerLine_Axis);    //移除对应的VRulerLine_Axis
                BaseCan.UnregisterName("new_VRulerLine_Axis");//注销对应VRulerLine_Axis的名字
            }
        }
        #endregion

        #region//对VRulerLine_Ticks的所有操作
        private void InitVRulerLine_Ticks()//初始化纵向标尺上101条刻度线数组及其所有元素线段（包括颜色、粗细、不透明度），并添加进ScrollViewer控件里
        {
            VRulerLine_Ticks = new Line[101]; //初始化纵向标尺上101条刻度线段数组

            for (int i = 0; i <= 100; i++)
            {
                VRulerLine_Ticks[i] = new Line();//初始化纵向标尺上101条刻度线数组中的所有元素刻度线段
                VRulerLine_Ticks[i].Stroke = new SolidColorBrush(Colors.OrangeRed);//颜色
                VRulerLine_Ticks[i].Opacity = 0.9;//非透明度

                if ((i == 0) || (i % 10 == 0))//刻度值为整十数的刻度线段(还有第一个刻度)i
                {
                    VRulerLine_Ticks[i].StrokeThickness = 1;
                }

                else if ((i % 2 != 0) && (i % 5 == 0))//刻度值为整五数的刻度线段
                {
                    VRulerLine_Ticks[i].StrokeThickness = 1;
                }

                else//其余刻度
                {
                    VRulerLine_Ticks[i].StrokeThickness = 1;
                }

                this.BaseCan.Children.Add(VRulerLine_Ticks[i]);//将新生成的刻度元素线段VRulerLine_Ticks[i]添加进控件里               
                BaseCan.RegisterName("new_VRulerLine_Ticks" + Convert.ToString(i), VRulerLine_Ticks[i]);//注册新生成的刻度元素线段VRulerLine_Ticks[i]以便删除
            }
        }

        private void LocateVRulerLine_Ticks(int[] x1, int[] x2, int[] y1)//定义纵向标尺上的所有垂直刻度线段的两端坐标
        {
            for (int i = 0; i <= 100; i++)
            {
                VRulerLine_Ticks[i].X1 = x1[i];
                VRulerLine_Ticks[i].Y1 = y1[i];
                VRulerLine_Ticks[i].X2 = x2[i];
                VRulerLine_Ticks[i].Y2 = y1[i];
            }
        }

        private void clearVRulerLine_Ticks()//移除纵向标尺上的101条刻度线段
        {
            VRulerLine_Ticks = new Line[101];

            for (int i = 0; i <= 100; i++)
            {
                VRulerLine_Ticks[i] = BaseCan.FindName("new_VRulerLine_Ticks" + Convert.ToString(i)) as Line;//找到新添加的VRulerLine_Tick[i]
                if (VRulerLine_Ticks[i] != null)
                {
                    BaseCan.Children.Remove(VRulerLine_Ticks[i]);    //移除对应的VRulerLine_Tick[i]
                    BaseCan.UnregisterName("new_VRulerLine_Ticks" + Convert.ToString(i));//注销对应VRulerLine_Tick[i]的名字
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //isNg = !isNg;
        }

        private void Slider_Contrast_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            bcifProcessFlag = true;
            int i = (int)Slider_Contrast.Value;

            if (pinjie == true)
            {
                //MessageBox.Show("拼接后不允许修改！");
                //return;
                imgProcess.AdjustContrastRatio(i);
                RefreshImgDisplayPintu(ref imgPinJieResult16);
                bcifProcessFlag = false;
            }
            else
            {
                if (dcmImage == null) return;
                imgProcess.AdjustContrastRatio(i);
                RefreshImgDisplay(ref dcmImage);
                bcifProcessFlag = false;
            }
            
        }

        private void Slider_Bright_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            bcifProcessFlag = true;
            if (pinjie == true)
            {
                imgProcess.AdjustBrightRatio((int)Slider_Bright.Value);
                RefreshImgDisplayPintu(ref imgPinJieResult16);
                bcifProcessFlag = false;
            }
            else
            {
                if (dcmImage == null) return;
                imgProcess.AdjustBrightRatio((int)Slider_Bright.Value);
                RefreshImgDisplay(ref dcmImage);
                bcifProcessFlag = false;
            }
            
        }

        private async void imageEnhance1_Click(object sender, RoutedEventArgs e)
        {
            if (IsIH1 == true) { }
            else IsIH1 = !IsIH1;
            IsIH2 = false; IsIH3 = false; IsIH4 = false; IsIH5 = false; IsIH6 = false;
            bcifProcessFlag = false;
            ImageEnhanceNum = 1;
            if (pinjie == true)
            {
                IsIH1 = false;
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            ImgProcessFlag = true;
            if (dcmImage == null) return;
            imgProcess.SetImageEnhance(true);
            await Task.Run(() =>
            {
                RefreshImgDisplay(ref dcmImage);
            });

        }

                
        private async void imageEnhance2_Click(object sender, RoutedEventArgs e)
        {
            if (IsIH2 == true) { }
            else IsIH2 = !IsIH2;
            IsIH1 = false; IsIH3 = false; IsIH4 = false; IsIH5 = false; IsIH6 = false;
            bcifProcessFlag = false;
            ImageEnhanceNum = 2;
            if (pinjie == true)
            {
                IsIH2 = false;
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            ImgProcessFlag = true;
            if (dcmImage == null) return;
            imgProcess.SetImageEnhance(true);
            await Task.Run(() =>
            {
                RefreshImgDisplay(ref dcmImage);
            });
        }
        private async void imageEnhance3_Click(object sender, RoutedEventArgs e)
        {

            if (IsIH3 == true) { }
            else IsIH3 = !IsIH3;

            IsIH2 = false; IsIH1 = false; IsIH4 = false; IsIH5 = false; IsIH6 = false;
            bcifProcessFlag = false;
            ImageEnhanceNum = 3;
            if (pinjie == true)
            {
                IsIH3 = false;
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            ImgProcessFlag = true;
            if (dcmImage == null) return;
            imgProcess.SetImageEnhance(true);
            await Task.Run(() =>
            {
                RefreshImgDisplay(ref dcmImage);
            });
        }

        private async void imageEnhance4_Click(object sender, RoutedEventArgs e)
        {
            if (IsIH4 == true) { }
            else IsIH4 = !IsIH4;
            IsIH2 = false; IsIH3 = false; IsIH1 = false; IsIH5 = false; IsIH6 = false;
            bcifProcessFlag = false;
            ImageEnhanceNum = 4;
            if (pinjie == true)
            {
                IsIH4 = false;
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            ImgProcessFlag = true;
            if (dcmImage == null) return;
            imgProcess.SetImageEnhance(true);
            await Task.Run(() =>
            {
                RefreshImgDisplay(ref dcmImage);
            });
        }
        private async void imageEnhance5_Click(object sender, RoutedEventArgs e)
        {

            if (IsIH5 == true) { }
            else IsIH5 = !IsIH5;
            IsIH2 = false; IsIH3 = false; IsIH4 = false; IsIH1 = false; IsIH6 = false;
            bcifProcessFlag = false;
            ImageEnhanceNum = 5;
            if (pinjie == true)
            {
                IsIH5 = false;
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            ImgProcessFlag = true;
            if (dcmImage == null) return;
            imgProcess.SetImageEnhance(true);
            await Task.Run(() =>
            {
                RefreshImgDisplay(ref dcmImage);
            });

        }



        private async void imageEnhance6_Click(object sender, RoutedEventArgs e)
        {
            var o = new OpenFileDialog();
            o.Multiselect = true;
            o.Filter = "RAW文件|*.raw|所有文件|*.*";
            string fileName = null ;
            Image<Gray, ushort> captureImg;
            Image<Gray, byte> showImg;
            if (o.ShowDialog() == true)
            {
                fileName = o.FileName;
            }
            ushort[] imageData;
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                int width = 2576;
                int height = 2048;
                BinaryReader br = new BinaryReader(fs);
                imageData = new ushort[br.BaseStream.Length/2];

                int nRet = int.Parse(br.BaseStream.Length.ToString());
                for (int i = 0; i < br.BaseStream.Length / 2 - 1; i++)
                {
                    imageData[i] = br.ReadUInt16();
                }
                //imageData = br.ReadBytes(nRet);

                unsafe
                {
                    captureImg = new Image<Gray, ushort>( height, width);
                    ushort* imagePtr = (ushort*)captureImg.Mat.DataPointer;
                    for (int i = 0; i <width*height; i++)
                    {
                        imagePtr[i] = imageData[i];
                    }

                }



            }
            showImg = captureImg.Convert<Gray, byte>();
            //int stride = (1920 * 16 + 7) / 8;
            //BitmapSource bmps = BitmapSource.Create(1920,1080, 96, 96, PixelFormats.Gray16, null, imageData, stride);
            BitmapSource bmps = imgFormatTransform.ImageToBitmapSource(showImg);
            imgbox.Source = bmps;
            AdjustImageBoxSize(bmps);







            ////CvInvoke.cvGetRawData()
            //BinaryReader br = new BinaryReader(File.Open(fileName, FileMode.Open));
            //ushort[] pix16 = new ushort[br.BaseStream.Length];
            ////var r = new Image<Gray, ushort>(DataU16.Width, DataU16.Height);
            //unsafe
            //{

            //    for (int i = 0; i < br.BaseStream.Length/2-1; i++)
            //    {
            //        pix16[i] = br.ReadUInt16();
            //    }
            //}
            //int stride = (2560 * 16 + 7) / 8;
            //BitmapSource bmps = BitmapSource.Create(2560, 2048, 96, 96,PixelFormats.Gray16, null, pix16, stride);
            //imgbox.Source = bmps;
            //AdjustImageBoxSize(bmps);
        }
        //private void DisplayImage16(string fileName)
        //{
        //    // Open a binary reader to read in the pixel data. 
        //    // We cannot use the usual image loading mechanisms since this is raw 
        //    // image data.
        //    try
        //    {
        //        BinaryReader br = new BinaryReader(File.Open(fileName, FileMode.Open));
        //        ushort pixShort;
        //        int i;
        //        long iTotalSize = br.BaseStream.Length;
        //        int iNumberOfPixels = (int)(iTotalSize / 2);

        //        // Get the dimensions of the image from the user
        //        ID = new ImageDimensions(iNumberOfPixels);
        //        if (ID.ShowDialog() == true)
        //        {
        //            width = Convert.ToInt32(ID.tbWidth.Text);
        //            height = Convert.ToInt32(ID.tbHeight.Text);
        //            canvas.Width = width;
        //            canvas.Height = height;
        //            img.Width = width;
        //            img.Height = height;
        //            pix16 = new ushort[iNumberOfPixels];

        //            for (i = 0; i < iNumberOfPixels; ++i)
        //            {
        //                pixShort = (ushort)(br.ReadUInt16());
        //                pix16[i] = pixShort;
        //            }
        //            br.Close();

        //            int bitsPerPixel = 16;
        //            stride = (width * bitsPerPixel + 7) / 8;

        //            // Single step creation of the image
        //            bmps = BitmapSource.Create(width, height, 96, 96,
        //                                PixelFormats.Gray16, null, pix16, stride);
        //            img.Source = bmps;
        //            bnSaveJPG.IsEnabled = true;
        //            bnSavePNG.IsEnabled = true;
        //        }
        //        else
        //        {
        //            br.Close();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Message, "Error",
        //               MessageBoxButton.OK, MessageBoxImage.Error);
        //    }
        //}



        #endregion

        #region//对VTickNum的所有操作
        private void InitVTickNum()//初始化纵向标尺上的刻度数值文本框数组及其元素文本框属性
        {
            VTickNum = new TextBlock[11];//实例化纵向标尺刻度数值文本框数组

            for (int i = 0; i <= 10; i++)
            {
                VTickNum[i] = new TextBlock();//初始化刻度线段数组中的所有元素刻度线段
                VTickNum[i].Text = Convert.ToString(10 - i);//刻度数值文本
                VTickNum[i].FontSize = 12;////刻度数值文本字号
                VTickNum[i].Foreground = new SolidColorBrush(Colors.OrangeRed);//刻度数值文本颜色
                this.BaseCan.Children.Add(VTickNum[i]);
                BaseCan.RegisterName("VTickNum_" + Convert.ToString(i), VTickNum[i]);
            }
        }



        private void LocateVTickNum(double[] m1, double[] m2, double[] m3, double[] m4)
        {
            for (int i = 0; i <= 10; i++)
            {
                VTickNum[i].Margin = new Thickness(m1[i], m2[i], m3[i], m4[i]);//纵向标尺刻度数值文本框的边距
            }
        }

        private void contract_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void currentgrayTB_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        //public async void savedcmfile()
        //{
        //    string path = filepath + pictureName;
        //    path = path + $"(保存的第" + savenumber + $"个副本).dcm";
        //    ushort[] rImg = new ushort[imgGU11.Width * imgGU11.Height];
        //    unsafe
        //    {
        //        ushort* ptr = (ushort*)imgGU11.Mat.DataPointer;
        //        for (int i = 0; i < rImg.Length; i++)
        //        {

        //            rImg[i] = ptr[i];
        //        }
        //    }
        //    await Utils.SaveDicomFile(rImg, path, imgGU11.Width, imgGU11.Height);
        //}


        private void clearVTickNum()
        {
            VTickNum = new TextBlock[11];//再次实例化，否则有BUG

            for (int i = 0; i <= 10; i++)
            {
                VTickNum[i] = BaseCan.FindName("VTickNum_" + Convert.ToString(i)) as TextBlock;
                if (VTickNum[i] != null)
                {
                    BaseCan.Children.Remove(VTickNum[i]);//移除刻度数值文本框
                    BaseCan.UnregisterName("VTickNum_" + Convert.ToString(i));//注销刻度数值文本框名字
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_GrayMeasure_Click(object sender, RoutedEventArgs e)
        {
            if (listbox.ItemsSource == null)
            {
                MessageBox.Show("请导入文件");
                return;
            }
            if (pinjie == true)
            {
                IsGm = !IsGm;
                if (IsGm)
                {
                    ResetAllSwitchtoFalseEXCEPT(ref showLine);
                    IsAddDefect(false);
                    IsGm = true;
                    clearlines();
                    cleardefectline();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
                else
                {
                    SetDispalyState(ref showArea, ref winchange);

                }
            }
            else
            {
                IsGm = !IsGm;
                if (IsGm)
                {
                    ResetAllSwitchtoFalseEXCEPT(ref showLine);
                    IsAddDefect(false);
                    IsGm = true;
                    clearlines();
                    cleardefectline();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
                else
                {
                    SetDispalyState(ref showArea, ref winchange);

                }
            }
        }


        #endregion

        #region//对纵向标尺的所有线段及数值文本框逆实例化
        private void clearVRulerLines()
        {
            VRulerLine_Axis = null;//无条件对VRulerLine_Axis逆实例化
            VRulerLine_Ticks = null;//无条件对VRulerLine_Ticks逆实例化           
        }

        private void btn_GrayStatic_Click(object sender, RoutedEventArgs e)
        {
            if (listbox.ItemsSource == null)
            {
                MessageBox.Show("请导入文件");
                return;
            }
            if (GridGrayStatic.Visibility == Visibility.Visible)
                GridGrayStatic.Visibility = Visibility.Hidden;
            else
                GridGrayStatic.Visibility = Visibility.Visible;
            if (pinjie == true)
            {
                //IsGs = !IsGs;
                //if (IsGs)
                //{
                //    ResetAllSwitchtoFalseEXCEPT(ref showLine);
                //    IsAddDefect(false);
                //    IsGs = true;
                //    clearlines();
                //    cleardefectline();
                //    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                //    CurrentText = null;
                //}
                //else
                //{
                //    SetDispalyState(ref showArea, ref winchange);

                //}
                IsGs = !IsGs;
                if (IsGs)
                {
                    ResetAllSwitchtoFalseEXCEPT(ref showArea);
                    IsAddDefect(false);
                    IsGs = true;
                    clearlines();
                    cleardefectline();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
                else
                {
                    SetDispalyState(ref showArea, ref winchange);

                }
            }
            else
            {
                IsGs = !IsGs;
                if (IsGs)
                {
                    ResetAllSwitchtoFalseEXCEPT(ref showArea);
                    IsAddDefect(false);
                    IsGs = true;
                    clearlines();
                    cleardefectline();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
                else
                {
                    SetDispalyState(ref showArea, ref winchange);

                }
            }
        }

        private void clearVTextBlock()
        {
            VTickNum = null;//无条件对VTickNum逆实例化
        }
        #endregion

        #endregion

        private void InitEllipse()
        {
            e1 = new System.Windows.Shapes.Ellipse();
            e1.Stroke = new SolidColorBrush(Colors.OrangeRed);
            e1.StrokeThickness = 2;
            e1.Opacity = 0.9;
            this.BaseCan.Children.Add(e1);
            BaseCan.RegisterName("newEllipse1", e1);

            e2 = new System.Windows.Shapes.Ellipse();
            e2.Stroke = new SolidColorBrush(Colors.OrangeRed);
            e2.StrokeThickness = 2;
            e2.Opacity = 0.9;
            this.BaseCan.Children.Add(e2);
            BaseCan.RegisterName("newEllipse2", e2);

            e3 = new System.Windows.Shapes.Ellipse();
            e3.Stroke = new SolidColorBrush(Colors.OrangeRed);
            e3.StrokeThickness = 2;
            e3.Opacity = 0.9;
            this.BaseCan.Children.Add(e3);
            BaseCan.RegisterName("newEllipse3", e3);

            e4 = new System.Windows.Shapes.Ellipse();
            e4.Stroke = new SolidColorBrush(Colors.OrangeRed);
            e4.StrokeThickness = 2;
            e4.Opacity = 0.9;
            this.BaseCan.Children.Add(e4);
            BaseCan.RegisterName("newEllipse4", e4);

            e6 = new System.Windows.Shapes.Ellipse();
            e6.Stroke = new SolidColorBrush(Colors.OrangeRed);
            e6.StrokeThickness = 2;
            e6.Opacity = 0.9;
            this.BaseCan.Children.Add(e6);
            BaseCan.RegisterName("newEllipse6", e6);

            e8 = new System.Windows.Shapes.Ellipse();
            e8.Stroke = new SolidColorBrush(Colors.OrangeRed);
            e8.StrokeThickness = 2;
            e8.Opacity = 0.9;
            this.BaseCan.Children.Add(e8);
            BaseCan.RegisterName("newEllipse8", e8);

            //e03 = new System.Windows.Shapes.Ellipse();
            //e03.Stroke = new SolidColorBrush(Colors.OrangeRed);
            //e03.StrokeThickness = 2;
            //e03.Opacity = 0.9;
            //this.BaseCan.Children.Add(e03);
            //BaseCan.RegisterName("newEllipse03", e03);

            //e04 = new System.Windows.Shapes.Ellipse();
            //e04.Stroke = new SolidColorBrush(Colors.OrangeRed);
            //e04.StrokeThickness = 2;
            //e04.Opacity = 0.9;
            //this.BaseCan.Children.Add(e04);
            //BaseCan.RegisterName("newEllipse04", e04);

            e05 = new System.Windows.Shapes.Ellipse();
            e05.Stroke = new SolidColorBrush(Colors.OrangeRed);
            e05.StrokeThickness = 2;
            e05.Opacity = 0.9;
            this.BaseCan.Children.Add(e05);
            BaseCan.RegisterName("newEllipse05", e05);

            //e06 = new System.Windows.Shapes.Ellipse();
            //e06.Stroke = new SolidColorBrush(Colors.OrangeRed);
            //e06.StrokeThickness = 2;
            //e06.Opacity = 0.9;
            //this.BaseCan.Children.Add(e06);
            //BaseCan.RegisterName("newEllipse06", e06);

            e07 = new System.Windows.Shapes.Ellipse();
            e07.Stroke = new SolidColorBrush(Colors.OrangeRed);
            e07.StrokeThickness = 2;
            e07.Opacity = 0.9;
            this.BaseCan.Children.Add(e07);
            BaseCan.RegisterName("newEllipse07", e07);
        }
        private void LocateEllipses(int z)
        {
            e6.SetValue(Canvas.LeftProperty, (double)(scrollpoint.X - r6 / correct * trans));
            e6.SetValue(Canvas.TopProperty, (double)(scrollpoint.Y - r6 / correct * trans));
            e6.Width = r6 / correct * trans;
            e6.Height = r6 / correct * trans;
            
            e8.SetValue(Canvas.LeftProperty, (double)(scrollpoint.X - r6 / correct * trans - r8 / correct * trans - z / correct * trans));
            e8.SetValue(Canvas.TopProperty, (double)(scrollpoint.Y - 0.5 * (r6 + r8) / correct * trans));
            e8.Width = r8 / correct * trans;
            e8.Height = r8 / correct * trans;

            e4.SetValue(Canvas.LeftProperty, (double)(scrollpoint.X - r4 / correct * trans));
            e4.SetValue(Canvas.TopProperty, (double)(scrollpoint.Y - r4 / correct * trans - r8 / correct * trans - z / correct * trans));
            e4.Width = r4 / correct * trans;
            e4.Height = r4 / correct * trans;

            e3.SetValue(Canvas.LeftProperty, (double)(scrollpoint.X - r4 / correct * trans - r3 / correct * trans - z / correct * trans));
            e3.SetValue(Canvas.TopProperty, (double)(scrollpoint.Y - 0.5 * (r3 + r4) / correct * trans - r8 / correct * trans - z / correct * trans));
            e3.Width = r3 / correct * trans;
            e3.Height = r3 / correct * trans;

            e2.SetValue(Canvas.LeftProperty, (double)(scrollpoint.X - r4 / correct * trans - r3 / correct * trans - r2 / correct * trans - 2 * z / correct * trans));
            e2.SetValue(Canvas.TopProperty, (double)(scrollpoint.Y - 0.5 * (r2 + r4) / correct * trans - r8 / correct * trans - z / correct * trans));
            e2.Width = r2 / correct * trans;
            e2.Height = r2 / correct * trans;

            e1.SetValue(Canvas.LeftProperty, (double)(scrollpoint.X - r4 / correct * trans - r3 / correct * trans - r2 / correct * trans - r1 / correct * trans - 3 * z / correct * trans));
            e1.SetValue(Canvas.TopProperty, (double)(scrollpoint.Y - 0.5 * (r1 + r4) / correct * trans - r8 / correct * trans - z / correct * trans));
            e1.Width = r1 / correct * trans;
            e1.Height = r1 / correct * trans;

            e07.SetValue(Canvas.LeftProperty, (double)(scrollpoint.X - r07 / correct * trans));
            e07.SetValue(Canvas.TopProperty, (double)(scrollpoint.Y - r4 / correct * trans - r8 / correct * trans - r07 / correct * trans - 2 * z / correct * trans));
            e07.Width = r07 / correct * trans;
            e07.Height = r07 / correct * trans;
           
            e05.SetValue(Canvas.LeftProperty, (double)(scrollpoint.X - r07 / correct * trans - r06 / correct * trans - r05 / correct * trans - 2 * 3 / correct * trans));
            e05.SetValue(Canvas.TopProperty, (double)(scrollpoint.Y - r4 / correct * trans - r8 / correct * trans - r07 / correct * trans - 2 * z / correct * trans));
            e05.Width = r05 / correct * trans;
            e05.Height = r05 / correct * trans;            

        }      
        //右键实现图片拖拉

        System.Windows.Point previousMousePoint = new System.Windows.Point(0, 0);
        Boolean ismove = false;

        //右键按下记录初始位置
        private void imgbox_MouseRightButtonDown(object sender, MouseButtonEventArgs e)//修改
        {
            //System.Windows.Controls.Image imgbox = sender as System.Windows.Controls.Image; //好像没什么用
            previousMousePoint = e.GetPosition(imgbox);
            ismove = true;
            if (pinjie)
            {
                int n1;
                int xw = picName.Count;
                //if (xw > 5) xw = 5;
                n1 = (int)(previousMousePoint.X / (imgbox.Width / xw));
                if (s != null) { s.Background = basecolor; }
                s = listbox.ItemContainerGenerator.ContainerFromIndex(n0+n1) as ListBoxItem;
                s.Background = selectcolor;
                listbox.ScrollIntoView(listbox.Items[n0 + n1]);
                pictureName = picName[n0 + n1];
            }
            //imgbox.Focus();//鼠标滚轮事件(缩放时)需要picturebox有焦点 //好像不用也可以
        }

        //右键释放
        private void imgbox_MouseRightButtonUp(object sender, MouseButtonEventArgs e)//修改
        {

            ismove = false;
            //if (pinjie)
            //{
            //    var unit = System.Drawing.GraphicsUnit.Pixel;
            //    int j = 0;
            //    int firstwidth = BaseBps.PixelWidth - 2 * cutSize;
            //    int firstheight = BaseBps.PixelHeight - 2 * cutSize;
            //    System.Drawing.Point point = new System.Drawing.Point(cutSize, cutSize);
            //    System.Drawing.Size size = new System.Drawing.Size(firstwidth, firstheight);
            //    System.Drawing.Rectangle cutRec = new System.Drawing.Rectangle(point, size);
            //    while (tlt.X < (Scroll.Width - imgbox.Width) / 2 - imgbox.Width / 10 && n0 < picName.Count - 6)
            //    {
            //        n0++;
            //        tlt.X += imgbox.Width / 5;
            //        j++;
            //    }
            //    if (j > 0)
            //    {
            //        Image<Bgr, byte> resultimg = new Image<Bgr, byte>(firstwidth * 5, firstheight);
            //        System.Drawing.Rectangle rl = new System.Drawing.Rectangle(j * firstwidth, 0, firstwidth * (5 - j), firstheight);
            //        drawInstance.DrawImage(ref resultimg, ref pintu, rl, unit, 0, 0);
            //        for (int i = 0; i < j; i++)
            //        {
            //            Image<Gray, byte> sources = new Image<Gray, byte>(GetImage(picName[n0 + 4 - i]).Bitmap);
            //            int tmpwidth = sources.Bitmap.Width - 2 * cutSize;//当前图像的宽高减去裁切大小
            //            int tmpheight = sources.Bitmap.Height - 2 * cutSize;
            //            if (tmpheight != firstheight | tmpwidth != firstwidth)//当前图像和第一张图像的尺寸不一致则退出
            //            {
            //                MessageBox.Show("图像尺寸不匹配！");
            //                return;
            //            }
            //            //rl = new System.Drawing.Rectangle(0, 0, firstwidth, firstheight);
            //            drawInstance.DrawImage(ref resultimg, ref sources, cutRec, unit, firstwidth * (4 - i), 0);
            //        }
            //        if (pintu != null)
            //        {
            //            pintu.Dispose();
            //            pintu = null;
            //        }
            //        pintu = new Image<Gray, byte>(resultimg.Bitmap);
            //        if (NumofDefs > 0)
            //        {
            //            for (int i = 0; i < NumofDefs; i++)
            //            {
            //                pinjieshow[8 * i] -= j * firstwidth;
            //                pinjieshow[8 * i + 2] -= j * firstwidth;
            //            }
            //            Image<Bgr, byte> grayd = DrawDefect(resultimg, 0);
            //            bitSourse = imgFormatTransform.ImageToBitmapSource(grayd);
            //            imgbox.Source = bitSourse;
            //        }
            //        else
            //        {
            //            bitSourse = imgFormatTransform.ImageToBitmapSource(pintu);
            //            imgbox.Source = bitSourse;
            //        }
            //    }
            //    j = 0;
            //    while (tlt.X > (Scroll.Width - imgbox.Width) / 2 + imgbox.Width / 10 && n0 > 0)
            //    {
            //        n0--;
            //        tlt.X -= imgbox.Width / 5;
            //        j++;
            //    }
            //    if (j > 0)
            //    {
            //        Image<Bgr, byte> resultimg = new Image<Bgr, byte>(firstwidth * 5, firstheight);
            //        System.Drawing.Rectangle rl = new System.Drawing.Rectangle(0, 0, firstwidth * (5 - j), firstheight);
            //        drawInstance.DrawImage(ref resultimg, ref pintu, rl, unit, firstwidth * j, 0);
            //        for (int i = 0; i < j; i++)
            //        {
            //            Image<Gray, ushort> sources = GetImage(picName[n0 + i]);
            //            int tmpwidth = sources.Bitmap.Width - 2 * cutSize;//当前图像的宽高减去裁切大小
            //            int tmpheight = sources.Bitmap.Height - 2 * cutSize;
            //            if (tmpheight != firstheight | tmpwidth != firstwidth)//当前图像和第一张图像的尺寸不一致则退出
            //            {
            //                MessageBox.Show("图像尺寸不匹配！");
            //                return;
            //            }
            //            drawInstance.DrawImage(ref resultimg, ref sources, cutRec, unit, firstwidth * i, 0);
            //        }
            //        if (pintu != null)
            //        {
            //            pintu.Dispose();
            //            pintu = null;
            //        }
            //        pintu = new Image<Gray, byte>(resultimg.Bitmap);
            //        if (NumofDefs > 0)
            //        {

            //            for (int i = 0; i < NumofDefs; i++)
            //            {
            //                pinjieshow[8 * i] += j * firstwidth;
            //                pinjieshow[8 * i + 2] += j * firstheight;
            //            }
            //            Image<Bgr, byte> grayd = DrawDefect(new Image<Bgr, byte>(pintu.Bitmap), 0);
            //            bitSourse = imgFormatTransform.ImageToBitmapSource(grayd);
            //            imgbox.Source = bitSourse;
            //        }
            //        else
            //        {
            //            bitSourse = imgFormatTransform.ImageToBitmapSource(pintu);
            //            imgbox.Source = bitSourse;
            //        }
            //    }
            //}
        }

        private void imgbox_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            //MessageBox.Show(".");
            //初始化各点用于计算和绘图
            lastpoint = e.GetPosition(BaseCan);
            //currentpoint = lastpoint;

            ImgLastPoint = e.GetPosition(imgbox);//修改
            if (pinjie)
            {
                int n1;
                int xw = picName.Count;
                //if (xw > 5) xw = 5;
                n1 = (int)(ImgLastPoint.X / (imgbox.Width / xw));
                if (s != null) { s.Background = basecolor; }
                s = listbox.ItemContainerGenerator.ContainerFromIndex(n0 + n1) as ListBoxItem;
                s.Background = selectcolor;
                listbox.ScrollIntoView(listbox.Items[n0 + n1]);
                pictureName = picName[n0 + n1];

                if (ImgLastPoint.X < 0) ImgLastPoint.X = 0;
                if (ImgLastPoint.Y < 0) ImgLastPoint.Y = 0;
                if (ImgLastPoint.X >= imgbox.Width) ImgLastPoint.X = imgbox.Width - 0.00001;
                if (ImgLastPoint.Y >= imgbox.Height) ImgLastPoint.Y = imgbox.Height - 0.00001;
                //ImgCurrentPoint = e.GetPosition(imgbox);

                currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                CurrentText = null;


                //清除上一次的线段
                clearlines();

                if (element != null)
                {
                    element.CaptureMouse();
                    element.Cursor = Cursors.Hand;
                }

                //点击左键显示图像信噪比弹框
                if (IsSnr)
                {
                    SNR sNR = new SNR();
                    sNR.size.Text = RegionWidth.ToString() + "×" + RegionHeight.ToString();//区域大小
                    sNR.gray_average.Text = meangray;//灰度平均值
                    sNR.gray_standardization.Text = GrayStandardization.ToString();//灰度标准化
                    sNR.normalized_snr.Text = NormalizedSNRN.ToString();//归一化信噪比
                    sNR.Title = "图像信噪比";
                    sNR.ShowDialog();

                }
                if (IsTw || IsTm)
                {
                    RefreshImgDisplayPintu(ref imgPinJieResult16 );
                    CancelType = null;
                }


            }
            else
            {
                if (ImgLastPoint.X < 0) ImgLastPoint.X = 0;
                if (ImgLastPoint.Y < 0) ImgLastPoint.Y = 0;
                if (ImgLastPoint.X >= imgbox.Width) ImgLastPoint.X = imgbox.Width - 0.00001;
                if (ImgLastPoint.Y >= imgbox.Height) ImgLastPoint.Y = imgbox.Height - 0.00001;
                //ImgCurrentPoint = e.GetPosition(imgbox);

                currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                CurrentText = null;


                //清除上一次的线段
                clearlines();

                if (element != null)
                {
                    element.CaptureMouse();
                    element.Cursor = Cursors.Hand;
                }

                //点击左键显示图像信噪比弹框
                if (IsSnr)
                {
                    SNR sNR = new SNR();
                    sNR.size.Text = RegionWidth.ToString() + "×" + RegionHeight.ToString();//区域大小
                    sNR.gray_average.Text = meangray;//灰度平均值
                    sNR.gray_standardization.Text = GrayStandardization.ToString();//灰度标准化
                    sNR.normalized_snr.Text = NormalizedSNRN.ToString();//归一化信噪比
                    sNR.Title = "图像信噪比";
                    sNR.ShowDialog();

                }
                if (IsTw || IsTm)
                {
                    RefreshImgDisplay(ref dcmImage);
                    CancelType = null;
                }
            }
            
        }

        private void imgbox_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            double prw, prh, nw, nh;
            System.Windows.Point position = e.GetPosition(imgbox);
            position.X -= imgbox.Width / 2.0;
            position.Y -= imgbox.Height / 2.0;
            Scroll.ScrollToHome();
            //double prx = tlt.X;
            //double pry = tlt.Y;
            //tlt.X = 0;
            //tlt.Y = 0;
            if (imgbox.Width > Scroll.Width) prw = (imgbox.Width - Scroll.Width) / 2;
            else prw = 0;
            if (imgbox.Height > Scroll.Height) prh = (imgbox.Height - Scroll.Height) / 2;
            else prh = 0;
            double MARrate = 1.2, MaxRate = 10, MinRate = 1;
            //if (sfr.ScaleX >= MinRate * (1 - MARrate) && sfr.ScaleX <= MaxRate && sfr.ScaleY >= MinRate * (1 - MARrate) && sfr.ScaleY <= MaxRate && e.Delta > 0)
            if (trans >= MinRate * (1 / MARrate) && trans <= MaxRate && e.Delta > 0)
            {
                imgbox.Width *= MARrate;
                imgbox.Height *= MARrate;
                if (imgbox.Width > Scroll.Width) nw = (imgbox.Width - Scroll.Width) / 2;
                else nw = 0;
                if (imgbox.Height > Scroll.Height) nh = (imgbox.Height - Scroll.Height) / 2;
                else nh = 0;
                //tlt.X = prx + prw - nw + position.X * (1 - MARrate);
                //tlt.Y = pry + prh - nh + position.Y * (1 - MARrate);               
                tlt.X += prw - nw + position.X * (1 - MARrate);
                tlt.Y += prh - nh + position.Y * (1 - MARrate);


            }
            //if (sfr.ScaleX >= MinRate && sfr.ScaleX <= MaxRate * (1 + MARrate) && sfr.ScaleY >= MinRate && sfr.ScaleY <= MaxRate * (1 + MARrate) && e.Delta < 0)
            if (trans >= MinRate && trans <= MaxRate * MARrate && e.Delta < 0)
            {
                imgbox.Width *= 1 / MARrate;
                imgbox.Height *= 1 / MARrate;
                if (imgbox.Width > Scroll.Width) nw = (imgbox.Width - Scroll.Width) / 2;
                else nw = 0;
                if (imgbox.Height > Scroll.Height) nh = (imgbox.Height - Scroll.Height) / 2;
                else nh = 0;
                //tlt.X = prx + prw - nw + position.X * (1 - 1 / MARrate);
                //tlt.Y = pry + prh - nh + position.Y * (1 - 1 / MARrate);
                tlt.X += prw - nw + position.X * (1 - 1 / MARrate);
                tlt.Y += prh - nh + position.Y * (1 - 1 / MARrate);
            }
            trans = imgbox.Height / BaseWinHeight;
        }
        public int currentXpj, currentYpj;
        private void imgbox_MouseMove(object sender, MouseEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;           
            currentpoint = e.GetPosition(BaseCan);
            
            //ImgCurrentPoint = e.GetPosition(imgbox);

            # region //左键按下，完成计算和画线
            if (e.LeftButton == MouseButtonState.Pressed)
            {

                //计算长度
                if (showLine == true || showArea == true)
                {
                    double a, b, area, length, angle;
                    int m, n;
                    length = Math.Sqrt((currentpoint.X - lastpoint.X) * (currentpoint.X - lastpoint.X) + (currentpoint.Y - lastpoint.Y) * (currentpoint.Y - lastpoint.Y));
                    length = length / trans * correct;
                    angle = (double)Math.Atan2(Math.Abs(currentpoint.Y - lastpoint.Y), Math.Abs(currentpoint.X - lastpoint.X)) * 180.0F / (double)Math.PI;
                    area = Math.Abs((currentpoint.X - lastpoint.X) * (currentpoint.Y - lastpoint.Y));
                    area = area / trans / trans * correct * correct;
                    a = Math.Abs(currentpoint.X - lastpoint.X) / trans * correct;
                    b = Math.Abs(currentpoint.Y - lastpoint.Y) / trans * correct;
                    m = (int)(Math.Abs(currentpoint.X + lastpoint.X) / trans * correct / 2);
                    n = (int)(Math.Abs(currentpoint.Y + lastpoint.Y) / trans * correct / 2);
                    
                    //changduTextbox.Text = Math.Round(length, 3).ToString() + "mm";
                    //mianjiTextbox.Text = Math.Round(area, 3).ToString() + "mm2";
                    //Position.Text = "(" + m.ToString() + "," + n.ToString() + ")";//修改
                    //WidthAndHeight.Text = "(" + ((int)a).ToString() + "," + ((int)b).ToString() + ")";//修改

                    if (CurrentText == null && (IsHlm || IsAr))
                    {
                        CurrentText = new TextBox();
                        this.BaseCan.Children.Add(CurrentText);
                        BaseCan.RegisterName("newText", CurrentText);
                        System.Windows.Media.Brush br = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 255, 255, 255));
                        CurrentText.Background = System.Windows.Media.Brushes.Transparent;
                        CurrentText.BorderBrush = br;
                        CurrentText.Foreground = or;
                        //CurrentText.Foreground = new SolidColorBrush(Colors.Red);
                        CurrentText.IsReadOnly = true;
                        //CurrentText.Cursor = Cursors.None;
                        //CurrentText.BorderThickness.Bottom ;
                    }
                    if (CurrentText != null)
                    {
                        if (currentpoint.X - lastpoint.X > 0)
                        {
                            CurrentText.Margin = new Thickness(currentpoint.X + 10, currentpoint.Y + 5, BaseCan.ActualWidth - currentpoint.X - 150, BaseCan.ActualHeight - currentpoint.Y - 60);
                        }
                        else
                        {
                            CurrentText.Margin = new Thickness(currentpoint.X - 150, currentpoint.Y + 5, BaseCan.ActualWidth - currentpoint.X + 10, BaseCan.ActualHeight - currentpoint.Y - 60);
                        }
                        if (showLine == true)
                        {
                            CurrentText.Text = "长度:" + Math.Round(length, 3).ToString() + "mm" + "\r" + "角度:" + Math.Round(angle, 3).ToString() + "度";
                        }
                        if (showArea == true)
                        {
                            CurrentText.Text = "面积:" + Math.Round(area, 3).ToString() + "mm2" + "\r" + "长度:" + Math.Round(a, 3).ToString() + "mm" + "\r" + "宽度:" + Math.Round(b, 3).ToString() + "mm";
                        }
                    }
                    if (IsTw)
                    {

                    }
                }
                //画线
                if (showLine == true)
                {
                    if (MarkSwitch == false)
                    {

                        if (currentline == null)
                        {
                            currentline = new Line();
                            currentline.Stroke = or;
                            currentline.StrokeThickness = 1;
                            currentline.Opacity = 0.9;

                            this.BaseCan.Children.Add(currentline);
                            BaseCan.RegisterName("newLine", currentline);//注册新生成的控件以便删除

                        }
                            currentline.X1 = lastpoint.X;
                            currentline.Y1 = lastpoint.Y;
                            currentline.X2 = currentpoint.X;
                            currentline.Y2 = currentpoint.Y;

                        if(isTw)
                        {
                            if(vline==null)
                            {
                                
                                vline = new Line();
                                vline.Stroke = or;
                                vline.StrokeThickness = 3;
                                vline.Opacity = 0.9;

                                this.BaseCan.Children.Add(vline);
                                BaseCan.RegisterName("VLine", vline);
                            }
                            double c = 20, vk, xz=(lastpoint.X+ currentpoint.X)/2, yz= (lastpoint.Y + currentpoint.Y) / 2;                                                        
                            if (Math.Abs(lastpoint.X - currentpoint.X) > Math.Abs(lastpoint.Y - currentpoint.Y)) //横轴较长
                            {
                                vk = (lastpoint.Y - currentpoint.Y) / (lastpoint.X - currentpoint.X); //斜率                               
                                vline.X1 = xz - c * vk;
                                vline.Y1 = yz + c;
                                vline.X2 = xz + c * vk;
                                vline.Y2 = yz - c;
                            }
                            else
                            {
                                vk = (lastpoint.X - currentpoint.X)/(lastpoint.Y - currentpoint.Y) ; //斜率
                                vline.X1 = xz + c; //垂线起点坐标
                                vline.Y1 = yz - c * vk;
                                vline.X2 = xz - c; //垂线终点坐标
                                vline.Y2 = yz + c * vk;
                            }                            
                        }
                    }
                    else
                    {
                        if (defectline == null)
                        {
                            defectline = new Line();
                            defectline.Stroke = or;
                            defectline.StrokeThickness = 1;
                            defectline.Opacity = 0.9;

                            this.BaseCan.Children.Add(defectline);
                            BaseCan.RegisterName("defectline", defectline);//注册新生成的控件以便删除
                        }
                        defectline.X1 = lastpoint.X;
                        defectline.Y1 = lastpoint.Y;
                        defectline.X2 = currentpoint.X;
                        defectline.Y2 = currentpoint.Y;
                    }
                }

                //用四条线画矩形
                if (showArea == true||winchange==true)
                {
                    if ((Rectline1 == null) && (Rectline2 == null) && (Rectline3 == null) && (Rectline4 == null))
                    {
                        InitRectLines();
                    }
                    LocateRectLines();
                }
            }
            #endregion
            #region //实时放大
            relativepoint = e.GetPosition(imgbox);//修改
            if (relativepoint.X < 0) relativepoint.X = 0;
            if (relativepoint.Y < 0) relativepoint.Y = 0;
            if (relativepoint.X >= imgbox.Width) relativepoint.X = imgbox.Width - 0.00001;
            if (relativepoint.Y >= imgbox.Height) relativepoint.Y = imgbox.Height - 0.00001;
            int i, j;
            i = (int)(bitSourse.PixelWidth / imgbox.Width * relativepoint.X);//修改
            j = (int)(bitSourse.PixelWidth / imgbox.Width * relativepoint.Y);
            //显示灰度值
            if (!pinjie) 
            {
                if (i < 0 || i > bitSourse.PixelWidth || j < 0 || j > bitSourse.PixelHeight)
                {
                    currentgrayTB.Text = null;
                    //thicknessTB.Text = null;
                }
                else
                {
                    int m, n;
                    m = CorrectXY(i, j, out n);
                    try
                    {
                        currentgrayTB.Text = iPixelData.GetPixel(m, n).ToString();
                        
                        //xordinate = m.ToString(); yordinte = n.ToString();                        
                        string datetime1 = null;
                        try
                        {
                            string datetime0 = dcmDataSet.Get<String>(DicomTag.AcquisitionDateTime);
                            if (datetime0 != null)
                            {
                                datetime1 = datetime0.Substring(0, 4) + "年" + datetime0.Substring(4, 2) + "月" + datetime0.Substring(6, 2) + "日" + datetime0.Substring(8, 2) + "时"
                                        + datetime0.Substring(10, 2) + "分" + datetime0.Substring(12, 2) + "秒";
                            }
                        }
                        catch { }
                        string flg = "文件名：" + filepath + pictureName + "\r\n" +
                                      "数据采集时间：" + datetime1 + "\r\n" +
                                      "GPS：" + "\r\n" +
                                      "图上坐标：" + "  " + "x：" + i.ToString() + "  " + "y：" + j.ToString() + "\r\n" +
                                      "图像尺寸：" + dcmImage.Width.ToString() + "*" + dcmImage.Height.ToString() + "\r\n" +
                                      "当前灰度：" + currentgrayTB.Text;
                        flagtext.Text = flg;
                    }
                    catch
                    { }
                }
            }
            else
            {
                {
                    int m, n;
                    m = CorrectXY(i, j, out n);
                    try
                    {
                        //currentgrayTB.Text = iPixelData.GetPixel(m, n).ToString();
                        if (imgPinJieResult16XY != null)
                            currentgrayTB.Text = imgPinJieResult16XY.Data.GetValue(n, m, 0).ToString();
                        else currentgrayTB.Text = iPixelData.GetPixel(m, n).ToString(); ;
                        currentXpj = m; currentYpj = n;
                        //xordinate = m.ToString(); yordinte = n.ToString();                        
                        string datetime1 = null;
                        try
                        {
                            string datetime0 = dcmDataSet.Get<String>(DicomTag.AcquisitionDateTime);
                            if (datetime0 != null)
                            {
                                datetime1 = datetime0.Substring(0, 4) + "年" + datetime0.Substring(4, 2) + "月" + datetime0.Substring(6, 2) + "日" + datetime0.Substring(8, 2) + "时"
                                        + datetime0.Substring(10, 2) + "分" + datetime0.Substring(12, 2) + "秒";
                            }
                        }
                        catch { }
                        string flg = "文件名：" + filepath + pictureName + "\r\n" +
                                      "数据采集时间：" + datetime1 + "\r\n" +
                                      "GPS：" + "\r\n" +
                                      "图上坐标：" + "  " + "x：" + i.ToString() + "  " + "y：" + j.ToString() + "\r\n" +
                                      "图像尺寸：" + dcmImage.Width.ToString() + "*" + dcmImage.Height.ToString() + "\r\n" +
                                      "当前灰度：" + currentgrayTB.Text;
                        flagtext.Text = flg;
                    }
                    catch
                    {
                        return;
                    }
                }
            }
                
            //显示灰度值结束

            if (i > bitSourse.PixelWidth) i = bitSourse.PixelWidth;
            if (j > bitSourse.PixelHeight) j = bitSourse.PixelHeight;

            int leftc = i - 19, topc = j - 19;
            int widthc = 40, heightc = 40;

            #region //调整左上边缘显示
            if (leftc < 0)
            {
                MagImg.SetValue(Canvas.LeftProperty, (double)((widthc / 2 - i) * MagImg.Width / widthc));
                leftc = 0;
            }
            else
            {
                MagImg.SetValue(Canvas.LeftProperty, (double)0);
            }
            if (topc < 0)
            {
                MagImg.SetValue(Canvas.TopProperty, (double)(heightc / 2 - j) * MagImg.Height / heightc);
                topc = 0;
            }
            else
            {
                MagImg.SetValue(Canvas.TopProperty, (double)0);
            }
            #endregion

            if (leftc + widthc > bitSourse.PixelWidth) { widthc = bitSourse.PixelWidth - leftc; }
            if (topc + heightc > bitSourse.PixelHeight) { heightc = bitSourse.PixelHeight - topc; }

            var cut = new Int32Rect(leftc, topc, widthc, heightc);
            int stride = bitSourse.Format.BitsPerPixel * cut.Width / 8;
            int size = cut.Height * stride;
            Data = new byte[size];
            bitSourse.CopyPixels(cut, Data, stride, 0);
            MagImg.Source = BitmapSource.Create(widthc, heightc, 0, 0, bitSourse.Format, null, Data, stride); //PixelFormats.Gray8
            DrawCrossHair();//画红色准心

            
            #endregion

            #region//实时显示峰值信噪比
            if (IsSnr)
            {
                SnrCalculate(e);
            }
            #endregion





            #region//右键按下，实现拖动图片

            if (e.RightButton == MouseButtonState.Pressed)
            {
                //System.Windows.Controls.Image imgbox = sender as System.Windows.Controls.Image;
                //imgbox.Focus();    //鼠标在picturebox上时才有焦点，此时可以拖动 //修改， 好像不用也可以
                if (ismove)
                {
                    System.Windows.Point position = e.GetPosition(imgbox);
                    tlt.X += position.X - this.previousMousePoint.X;
                    tlt.Y += position.Y - this.previousMousePoint.Y;
                }
            }

            #endregion

            #region//标尺

            if (Ishbiao && !ismove)
            {
                //int x1, y1, x2, y2;
                //System.Windows.Point position = e.GetPosition(imgbox);
                //Image<Bgr, byte> grayd = new Image<Bgr, byte>(imgshowbox.Image.Bitmap);
                //if (DefectView && !DefectShow)
                //{
                //    grayd = drawdefects(grayd, pictureName);
                //}
                //else if (defcoords[0] != -1)
                //{
                //    try
                //    {
                //        grayd = DrawDefect(grayd);
                //    }
                //    catch { }
                //} 
                //Graphics gps = System.Drawing.Graphics.FromImage(grayd.Bitmap);
                //System.Drawing.Pen pen5 = new System.Drawing.Pen(System.Drawing.Color.Red, 5);
                //System.Drawing.Pen pen3 = new System.Drawing.Pen(System.Drawing.Color.Red, 3);
                //System.Drawing.Pen pen2 = new System.Drawing.Pen(System.Drawing.Color.Red, 2);
                //SolidBrush drawBrush = new SolidBrush(System.Drawing.Color.Red);
                //Font drawFont = new Font("黑体", 16);

                //int grayd_w=grayd.Bitmap.Width;
                //int grayd_h=grayd.Bitmap.Height;
                //double you=200 / correct * trans;
                //x1 = (int)(grayd_w / imgbox.Width * (position.X));
                //x2 = (int)(grayd_w / imgbox.Width * (position.X + you));
                //if (x2 > grayd_w) x2 = grayd_w;
                //y1 = (int)(bitSourse.PixelWidth / imgbox.Width * (position.Y));
                //y2 = y1;
                //gps.DrawLine(pen5, x1, y1, x2, y2);
                //for (int yi = 0; yi <= 200; yi++)
                //{
                //    double cyi = yi / correct * trans;
                //    x1 = (int)(grayd_w / imgbox.Width * (position.X + cyi));
                //    if (x1 > grayd_w) break;
                //    x2 = x1;
                //    if (yi%10==0)
                //    {
                //        y2 = y1 - 20;
                //        gps.DrawLine(pen3, x1, y1, x2, y2);
                //        gps.DrawString((yi/10).ToString(), drawFont, drawBrush, (float)(x1-10), (float)(y2 - 30));
                //    }
                //    else if (yi%5==0)
                //    {
                //        y2 = y1 - 16;
                //        gps.DrawLine(pen3, x1, y1, x2, y2);
                //    }
                //    else
                //    {
                //        y2 = y1 - 10;
                //        gps.DrawLine(pen2, x1, y1, x2, y2);
                //    }
                //}
                //bitSourse = imgFormatTransform.ImageToBitmapSource(grayd);
                //imgbox.Source = bitSourse;                                             
            }
            #endregion
        }

        private void imgbox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            element.ReleaseMouseCapture();
            element.Cursor = null;
            //System.Windows.Forms.MessageBox.Show(winchange.ToString());
            System.Windows.Point ImgfinalPoint = e.GetPosition(imgbox);//修改
            if (ImgfinalPoint.X < 0) ImgfinalPoint.X = 0;
            if (ImgfinalPoint.Y < 0) ImgfinalPoint.Y = 0;
            if (ImgfinalPoint.X >= imgbox.Width) ImgfinalPoint.X = imgbox.Width - 0.00001;
            if (ImgfinalPoint.Y >= imgbox.Height) ImgfinalPoint.Y = imgbox.Height - 0.00001;
            int x1, y1;//修改
            int x2, y2;
            int xt, yt;
            int n1 = 0, n2 = 0;
            if (pinjie == false)
            {
                x1 = (int)(bitSourse.PixelWidth / imgbox.Width * ImgLastPoint.X);//求取图上坐标实际大小，此时尚未进行坐标变换
                y1 = (int)(bitSourse.PixelWidth / imgbox.Width * ImgLastPoint.Y);
                x2 = (int)(bitSourse.PixelWidth / imgbox.Width * ImgfinalPoint.X); 
                y2 = (int)(bitSourse.PixelWidth / imgbox.Width * ImgfinalPoint.Y);             
            }
            else
            {
                //int xw = picName.Count;
                //x1 = (int)((ImgLastPoint.X % (imgbox.Width / xw)) * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width / xw)) + cutSize;
                //y1 = (int)(ImgLastPoint.Y * (BaseBps.PixelHeight - 2 * cutSize) / imgbox.Height) + cutSize;
                //x2 = (int)((ImgfinalPoint.X % (imgbox.Width / xw)) * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width / xw)) + cutSize;
                //y2 = (int)(ImgfinalPoint.Y * (BaseBps.PixelHeight - 2 * cutSize) / imgbox.Height) + cutSize;
                //n1 = (int)(ImgLastPoint.X / (imgbox.Width / xw));
                //n2 = (int)(ImgfinalPoint.X / (imgbox.Width / xw));
                //px1 = (int)(ImgLastPoint.X * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width / xw));
                //px2 = (int)(ImgfinalPoint.X * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width / xw));
                //py1 = (int)(ImgLastPoint.Y * (BaseBps.PixelHeight - 2 * cutSize) / imgbox.Height);
                //py2 = (int)(ImgfinalPoint.Y * (BaseBps.PixelHeight - 2 * cutSize) / imgbox.Height);
                x1 = (int)(bitSourse.PixelWidth / imgbox.Width * ImgLastPoint.X);//求取图上坐标实际大小，此时尚未进行坐标变换
                y1 = (int)(bitSourse.PixelWidth / imgbox.Width * ImgLastPoint.Y);
                x2 = (int)(bitSourse.PixelWidth / imgbox.Width * ImgfinalPoint.X);
                y2 = (int)(bitSourse.PixelWidth / imgbox.Width * ImgfinalPoint.Y);


                //px1 = (int)(ImgLastPoint.X * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width));
                //px2 = (int)(ImgfinalPoint.X * (BaseBps.PixelWidth - 2 * cutSize) / (imgbox.Width));
                //py1 = (int)(ImgLastPoint.Y );
                //py2 = (int)(ImgfinalPoint.Y);

                if (n1 != n2)
                {
                    MessageBox.Show("请在同一张图片上标注！");
                    return;
                }
            }

            int a = x1, b = y1, c = x2, d = y2;
            x1 = CorrectXY(a, b, out y1);            //坐标变换
            x2 = CorrectXY(c, d, out y2);
            defectX1 = x1; defectX2 = x2; defectY1 = y1; defectY2 = y2; xt = x2; yt = y2;
            IsVlm = false;
            if (winchange == true && Math.Abs(x2 - x1) > 5 && Math.Abs(y2 - y1) > 5 /*&& pinjie == false*/)//修改
            {
                if(pinjie==false)
                {
                    if (x1 > x2)
                    {
                        int tx = x1;
                        x1 = x2;
                        x2 = tx;
                    }
                    if (y1 > y2)
                    {
                        int ty = y1;
                        y1 = y2;
                        y2 = ty;
                    }
                    int mindicom = 65536, maxdicom = 0;
                    for (int x = x1; x <= x2; x++)
                    {
                        for (int y = y1; y <= y2; y++)
                        {
                            //if(x>=0&&x< iPixelData.Width &&y>=0&& x < iPixelData.Height )
                            {
                                int pxy = (int)iPixelData.GetPixel(x, y);
                                if (pxy > maxdicom) maxdicom = pxy;
                                if (pxy < mindicom) mindicom = pxy;
                            }
                            
                        }
                    }
                    winwidth = maxdicom - mindicom;
                    wincenter = (maxdicom + mindicom) / 2;
                    WinwidthorWincenterChanged();
                    clearlines();
                }
                else
                {
                    
                    if (x1 > x2)
                    {
                        int tx = x1;
                        x1 = x2;
                        x2 = tx;
                    }
                    if (y1 > y2)
                    {
                        int ty = y1;
                        y1 = y2;
                        y2 = ty;
                    }
                    int mindicom = 65536, maxdicom = 0;
                    for (int x = x1; x < x2; x++)
                    {
                        for (int y = y1; y < y2; y++)
                        {
                            //int pxy = (int)iPixelData.GetPixel(x, y);
                            object pxyo = imgPinJieResult16XY.Data.GetValue(y, x, 0);
                            int pxy = Convert.ToInt32(pxyo) ;
                            if (pxy > maxdicom) maxdicom = pxy;
                            if (pxy < mindicom) mindicom = pxy;
                        }
                    }

                    winwidth = maxdicom - mindicom;
                    wincenter = (maxdicom + mindicom) / 2;
                    WinwidthorWincenterChanged();
                    clearlines();


                }

                //winchange = false;

            }
            else if (IsTw)
            {
                Thread ShuangsiThread = new Thread(() => tiqu(x1, x2, y1, y2));
                ShuangsiThread.Start();                                
            }
            else if (IsTm)
            {
                Thread thicknessThread = new Thread(() => thickMs(x1, x2, y1, y2));
                thicknessThread.Start();
            }
            else if(IsGm)
            {
                Thread GrayMeasureThread = new Thread(() => GrayMeasure (x1, x2, y1, y2));
                GrayMeasureThread.Start();
            }
            else if (IsGs)
            {
                Thread GrayStaticThread = new Thread(() => GrayStatic(x1, x2, y1, y2));
                GrayStaticThread.Start();
            }
            else if (IsHla)
            {
                var color = System.Drawing.Color.Red;
                var font = new Font("黑体", 25);
                if (MarkSwitch==true)
                {
                    
                        return;
                }
                double length;
                if (currentline != null)
                {
                    length = Math.Sqrt((currentline.X1 - currentline.X2) * (currentline.X1 - currentline.X2) + (currentline.Y1 - currentline.Y2) * (currentline.Y1 - currentline.Y2));
                }
                else
                {
                    length = Math.Sqrt((currentpoint.X - lastpoint.X) * (currentpoint.X - lastpoint.X) + (currentpoint.Y - lastpoint.Y) * (currentpoint.Y - lastpoint.Y));
                }
                //length = length / trans * correct;
                correct = help / (length / trans);
                SaveCorrect(rootpath);
                MessageBox.Show("校准成功！");
                clearlines();
                drawNeeded = true;
                DefectInfo dInstance = new DefectInfo(0, x1, y1, x2, y2, null);
                defectInfo.Add(dInstance);                                            
                string texts = "长度:" + help.ToString() + "mm";
                dInstance = new DefectInfo(2, x1+10, y1+5, 0, 0, texts);
                defectInfo.Add(dInstance);
                //Thread ShuangsiThread = new Thread(() => tiqu(x1, x2, y1, y2));
                //ShuangsiThread.Start();
                
                RefreshImgDisplay(ref dcmImage);                
                SetDispalyState(ref winchange,ref showArea);
                IsHla = !IsHla;
            }
            else if (MarkSwitch&&(IsHlm||IsAr)&&!IsAw)//修改 
            {
                //if (IsAw) goto AddWords;
                int co = 8 * NumofDefs;
                double length = Math.Sqrt((currentpoint.X - lastpoint.X) * (currentpoint.X - lastpoint.X) + (currentpoint.Y - lastpoint.Y) * (currentpoint.Y - lastpoint.Y));
                double area = Math.Abs((currentpoint.X - lastpoint.X) * (currentpoint.Y - lastpoint.Y));
                area = area / trans / trans * correct * correct;
                if (length < 2) goto jump;
                cleardefectline();
                defectline = null;                
                Save.IsEnabled = true;
                isQueadd = true;
                

                if (pinjie == false)
                {
                    defcoords[co] = defectX1; defcoords[co + 1] = defectY1; defcoords[co + 2] = defectX2; defcoords[co + 3] = defectY2;
                    if (IsHlm) { defcoords[co + 4] = (int)(length / trans * correct); defcoords[co + 5] = 0; defcoords[co + 6] = 0; defcoords[co + 7] = 1; }
                    else if (IsAr)
                    {
                        defcoords[co + 4] = (int)area;
                        defcoords[co + 5] = (int)(Math.Abs(currentpoint.X - lastpoint.X) / trans * correct);
                        defcoords[co + 6] = (int)(Math.Abs(currentpoint.Y - lastpoint.Y) / trans * correct);
                        defcoords[co + 7] = 0;
                    }
                    Picname = pictureName;
                    RefreshImgDisplay(ref dcmImage);
                    
                }
                else
                {
                    Picname = picName[n1 + n0];
                    //if (isHlm || rec1 || rec2 || rec3)
                    {
                        int cp = 8 * NumofDefs;
                        pinjieshow[cp] = px1; pinjieshow[cp + 1] = py1; pinjieshow[cp + 2] = px2; pinjieshow[cp + 3] = py2;
                        //pinjieshow[cp] = 100; pinjieshow[cp + 1] = 100; pinjieshow[cp + 2] = 1000; pinjieshow[cp + 3] = 1000;
                        if (IsHlm) { pinjieshow[cp + 4] = (int)(length / trans * correct); pinjieshow[cp + 5] = 0; pinjieshow[cp + 6] = 0; pinjieshow[co + 7] = 1; }
                        else if (IsAr)
                        {
                            pinjieshow[cp + 4] = (int)area;
                            pinjieshow[cp + 5] = (int)(Math.Abs(currentpoint.X - lastpoint.X) / trans * correct);
                            pinjieshow[cp + 6] = (int)(Math.Abs(currentpoint.Y - lastpoint.Y) / trans * correct);
                            pinjieshow[cp + 7] = 0;
                        }
                        RefreshImgDisplayPintu(ref imgPinJieResult16);
                        //Image<Bgr, byte> grayd = DrawDefect(new Image<Bgr, byte>(imgPinJieResult8.Bitmap), 0);
                        //bitSourse = imgFormatTransform.ImageToBitmapSource(grayd);
                        //imgbox.Source = bitSourse;
                    }
                }
                NumofDefs = NumofDefs + 1;
                CancelType = "def";
                clearlines(); CurrentText = null;
            }
            //AddWords:;
            if(IsAw)
            {
                GetSaveString demo = new GetSaveString();
                demo.Title = "输入文字";
                demo.ShowDialog();
                if (SaveWords==null) goto jump;
                SaveTexts.Add(xt.ToString());
                SaveTexts.Add(yt.ToString());
                SaveTexts.Add(SaveWords);
                CancelType = "text";
                RefreshImgDisplay(ref dcmImage);
            }
            jump:;
            currentline = null;vline = null;
            Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null;
            CurrentText = null;
        }

        public void SnrCalculate(MouseEventArgs e)//计算信噪比
        {
            relativepoint = e.GetPosition(imgbox);//修改
            if (relativepoint.X < 0) relativepoint.X = 0;
            if (relativepoint.Y < 0) relativepoint.Y = 0;
            if (relativepoint.X >= imgbox.Width) relativepoint.X = imgbox.Width - 0.00001;
            if (relativepoint.Y >= imgbox.Height) relativepoint.Y = imgbox.Height - 0.00001;
            System.Windows.Point assistpoint = e.GetPosition(BaseCan);
            int SnrAreaWidth = 55, SnrAreaHeight = 20;
            //int SnrAreaWidth = 50, SnrAreaHeight = 50;
            int SnrAreaWidth2 = SnrAreaWidth / 2, SnrAreaHeight2 = SnrAreaHeight / 2;
            if ((int)relativepoint.X > SnrAreaWidth2 * imgbox.Width / bitSourse.PixelWidth && (int)relativepoint.Y > SnrAreaHeight2 * imgbox.Height / bitSourse.Height && (int)relativepoint.X < imgbox.Width - SnrAreaWidth2 * imgbox.Width / bitSourse.PixelWidth && (int)relativepoint.Y < imgbox.Height - SnrAreaHeight2 * imgbox.Height / bitSourse.Height)
            {
                if ((Rectline1 == null) && (Rectline2 == null) && (Rectline3 == null) && (Rectline4 == null))
                {
                    InitRectLines();
                }
                LocateRectLines((int)assistpoint.X - (int)(SnrAreaWidth2 * imgbox.Width / bitSourse.PixelWidth), (int)assistpoint.Y - (int)(SnrAreaHeight2 * imgbox.Height / bitSourse.Height), (int)assistpoint.X + (int)(SnrAreaWidth2 * imgbox.Width / bitSourse.PixelWidth), (int)assistpoint.Y + (int)(SnrAreaHeight2 * imgbox.Height / bitSourse.Height));
            }
            else return;
            int x1, y1, x2, y2, t1, t2;
            if (!pinjie)
            {
                int i, j;
                i = (int)(bitSourse.PixelWidth / imgbox.Width * relativepoint.X);//修改
                j = (int)(bitSourse.PixelWidth / imgbox.Width * relativepoint.Y);
                //int x1, y1, x2, y2, t1, t2;
                t1 = i - SnrAreaWidth2;
                t2 = j - SnrAreaHeight2;
                x1 = CorrectXY(t1, t2, out y1);
                x2 = CorrectXY(t1 + SnrAreaWidth, t2 + SnrAreaHeight, out y2);
            }
            else
            {
                int i, j;
                i = currentXpj;//修改
                j = currentYpj ;
                
                t1 = i - SnrAreaWidth2;
                t2 = j - SnrAreaHeight2;
                x1 = CorrectXY(t1, t2, out y1);
                x2 = CorrectXY(t1 + SnrAreaWidth, t2 + SnrAreaHeight, out y2);
            }
            if (x1 > x2)
            {
                int tx = x1;
                x1 = x2;
                x2 = tx;
            }
            if (y1 > y2)
            {
                int ty = y1;
                y1 = y2;
                y2 = ty;
            }
            int mindicom = 65536, maxdicom = 0; //在图中要显示最小灰度和最大灰度
            double zp = 0, mp=0, sp=0, snrn=0, snrm=0;
            if(!pinjie)
                imgQualityEvaluation.SNRCalculator(x1, y1, x2, y2, SnrAreaWidth,SnrAreaHeight, ref mp, ref sp, ref snrn, ref maxdicom, ref mindicom);
            else
                imgQualityEvaluation.SNRCalculatorpj(x1, y1, x2, y2, SnrAreaWidth, SnrAreaHeight, ref mp, ref sp, ref snrn, ref maxdicom, ref mindicom);
            //for (int x = x1; x < x2; x++)
            //{
            //    for (int y = y1; y < y2; y++)
            //    {
            //        double pxy = iPixelData.GetPixel(x, y);
            //        if (pxy > maxdicom) maxdicom = (int)pxy;
            //        if (pxy < mindicom) mindicom = (int)pxy;
            //        zp += pxy;
            //    }
            //}
            //mp = zp / SnrAreaWidth / SnrAreaHeight;//在图中要显示该平均灰度
            //zp = 0;
            //for (int x = x1; x < x2; x++)
            //{
            //    for (int y = y1; y < y2; y++)
            //    {
            //        double pxy = iPixelData.GetPixel(x, y);
            //        zp += Math.Pow(pxy - mp, 2);
            //    }
            //}
            //sp = Math.Sqrt(zp / (SnrAreaWidth * SnrAreaHeight - 1));//在图中要显示该灰度方差
            //snrm = mp / sp;
            //snrn = snrm * 88.6 / pixelsize;//在图中要显示该归一化信噪比      
            if (CurrentText == null)
            {
                CurrentText = new TextBox();
                this.BaseCan.Children.Add(CurrentText);
                BaseCan.RegisterName("newText", CurrentText);
                System.Windows.Media.Brush br = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 255, 255, 255));
                System.Windows.Media.Brush or = new SolidColorBrush(System.Windows.Media.Color.FromRgb( 255, 127, 39));
                CurrentText.Background = System.Windows.Media.Brushes.Transparent;
                CurrentText.BorderBrush = br;
                CurrentText.Foreground = or;
                CurrentText.IsReadOnly = true;              
            }
            int SnrWidth = (int)(SnrAreaWidth2 * imgbox.Width / bitSourse.PixelWidth);
            int SnrHeight = (int)(SnrAreaHeight2 * imgbox.Width / bitSourse.PixelWidth);
            CurrentText.Margin = new Thickness(assistpoint.X + SnrWidth + 10, assistpoint.Y + SnrHeight + 5, BaseCan.ActualWidth - SnrWidth - assistpoint.X - 150, BaseCan.ActualHeight - SnrHeight - assistpoint.Y - 60);
            //CurrentText.Text = "x1：" + x1.ToString() +  "\r" + "x2：" + x2.ToString() + "\r" + "y1：" + y1.ToString() + "\r" + "y2：" + y2.ToString();
            CurrentText.Text = "最小灰度：" + mindicom.ToString() + "\r" + "最大灰度：" + maxdicom.ToString() + "\r" + "平均灰度：" + Math.Round(mp, 0).ToString() + "\r" + "灰度方差：" + Math.Round(sp, 3).ToString() + "\r" + "归一化信噪比：" + Math.Round(snrn, 3).ToString();           
            CurrentText.IsReadOnly = true;
            RegionWidth = SnrAreaWidth;
            RegionHeight = SnrAreaHeight;
            GrayAverage = Math.Round(mp, 0);
            GrayStandardization = Math.Round(sp, 3);
            NormalizedSNRN = Math.Round(snrn, 3);
        }
        public int CorrectXY(int x0, int y0, out int y)//将图上坐标变换到dcm原图坐标
        {
            int x;
            if (rotate == 90)
            {
                x = y0;
                y = BaseBps.PixelWidth - x0;
            }
            else if (rotate == 180)
            {
                x = BaseBps.PixelWidth - x0;
                y = BaseBps.PixelHeight - y0;
            }
            else if (rotate == 270)
            {
                x = BaseBps.PixelHeight - y0;
                y = x0;
            }
            else
            {
                x = x0;
                y = y0;
            }
            if (isHSwitch)
            {
                x = dcmImage.Width - x;
            }
            if (isVSwitch)
            {
                y = dcmImage.Height - y;
            }
            return x;
        }

        public int TransformXY(int x0, int y0, out int y)//将dcm原图坐标变换到图上坐标
        {                   
            int x;
            if (isHSwitch)
            {
                x0 = dcmImage.Width - x0;
            }
            if (isVSwitch)
            {
                y0 = dcmImage.Height - y0;
            }
            if (rotate == 90)
            {
                x = dcmImage.Height - y0;
                y = x0;
            }
            else if (rotate == 180)
            {
                x = dcmImage.Width - x0;
                y = dcmImage.Height - y0;
            }
            else if (rotate == 270)
            {
                x = y0;
                y = dcmImage.Width - x0;
            }
            else
            {
                x = x0;
                y = y0;
            }
            return x;
        
        }

        private double clctxy(int x, int n, int y, bool fang)
        {
            double txy = 0;
            double[] imgs = new double[n * 3];
            int K = 0;
            if (fang)//fang控制取点方向，以下条件语句按照选定方向给imgs逐点赋值
            {
                for (int j = -1; j <= 1; j++) 
                {
                    for (int i = 0; i < n; i++) 
                    {
                        imgs[K] = iPixelData.GetPixel(x + i, y + j);
                        K++;
                    }
                }
            }
            else
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int i = 0; i < n; i++)
                    {
                        imgs[K] = iPixelData.GetPixel(x + j, y + i);
                        K++;
                    }
                }
            }
            //showshuansi(imgs, n, 3);
            double[] gx = new double[n - 2];
            double[] gy = new double[n - 2];
            for (int i = 1; i < n - 1; i++)
            {
                gy[i - 1] = imgs[2 * n + i - 1] - imgs[i - 1] + 2 * (imgs[2 * n + i] - imgs[i]) + imgs[2 * n + i + 1] - imgs[i + 1];
                gx[i - 1] = imgs[i + 1] - imgs[i - 1] + 2 * (imgs[i + 1 + n] - imgs[i - 1 + n]) + imgs[i + 1 + 2 * n] - imgs[i - 1 + 2 * n];
            }
            double mx = 0.5 * gx.Max();
            K = 0;
            for (int i = 0; i < n - 2; i++)
            {
                if (gx[i] > mx)
                {
                    K++;
                    txy += gy[i] / gx[i];
                }
            }
            txy /= K;
            return txy;
        }
        public void thickMs(int x1, int x2, int y1, int y2)//提取厚度区域
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (x1 < 11 || x2 < 11 || y1 < 11 || y2 < 11 || x1 > dcmImage.Width - 11 || x2 > dcmImage.Width - 11 || y1 > dcmImage.Height - 11 || y2 > dcmImage.Height - 11)
                {
                    System.Windows.Forms.MessageBox.Show("位置超出边缘");
                    return;
                }
                if (Math.Abs(x1 - x2) < 5 && Math.Abs(y1 - y2) < 5) return;
                setDvalue dwindow = new setDvalue();//获取钢管直径thickns
                dwindow.ShowDialog();
                if (!thicknessContinue) { clearlines(); return; }
                //int a, b, c, d, e, f, g, h;//用于图上画线
                double txy = 0;
                if (x2 == x1) txy = 100000;
                else txy = (y2 - y1) / (x2 - x1);
                int nx, ny, n, whi1=21;
                double[] imgs;
                int hi2 = (whi1 - 1) / 2;//whi1为凹槽截取宽度值，此处固定为21
                nx = Math.Abs(x2 - x1);
                ny = Math.Abs(y2 - y1);
                
                //取双丝灰度曲线
                if (nx > ny) //若横轴较长，按横坐标处理
                {
                    n = nx + 1;
                    if (n < 10) return;
                    imgs = imgQualityEvaluation.GetImgCut(true, n, hi1, x1, y1, x2, y2);
                }
                else//若纵轴较长，按纵坐标处理
                {
                    n = ny + 1;
                    if (n < 10) return;
                    imgs = imgQualityEvaluation.GetImgCut(false, n, hi1, x1, y1, x2, y2);
                }
                BitmapSource thickCut = imgFormatTransform.arrytobitmapsource(imgs, n, whi1, (int)imgs.Max(), (int)imgs.Min());                
                DefectInfo info = new DefectInfo(0,x1,y1,x2,y2,null);
                defectInfo.Add(info);
                drawNeeded = true;
                RefreshImgDisplay(ref dcmImage);                
                imgQualityEvaluation.thicknessCalculate(thickCut, imgs, n, whi1, thickns); //计算厚度
                CancelType = "ss";
                clearlines();
            });
        }
        public void tiqu(int x1, int x2, int y1, int y2)//提取双丝区域
        {
            if(!pinjie)
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (x1 < 11 || x2 < 11 || y1 < 11 || y2 < 11 || x1 > dcmImage.Width - 11 || x2 > dcmImage.Width - 11 || y1 > dcmImage.Height - 11 || y2 > dcmImage.Height - 11)
                    {
                        System.Windows.Forms.MessageBox.Show("位置超出边缘");
                        return;
                    }
                    if (Math.Abs(x1 - x2) < 5 && Math.Abs(y1 - y2) < 5) return;
                    GetShuangsiWidth demo = new GetShuangsiWidth();
                    demo.Title = "宽度选择";
                    demo.ShowDialog();
                    if (!shuangsiCountinue) { clearlines(); return; }
                    int nx, ny, n;
                    double[] imgs;

                    int hi2 = (hi1 - 1) / 2;//hi1为双丝截取宽度值，为全局变量，可在弹窗中设定
                    nx = Math.Abs(x2 - x1);
                    ny = Math.Abs(y2 - y1);
                    //取双丝灰度曲线
                    if (nx > ny) //若横轴较长，按横坐标处理
                    {
                        n = nx + 1;
                        if (n < 10) return;
                        imgs = imgQualityEvaluation.GetImgCut(true, n, hi1, x1, y1, x2, y2);
                    }
                    else//若纵轴较长，按纵坐标处理
                    {
                        n = ny + 1;
                        if (n < 10) return;
                        imgs = imgQualityEvaluation.GetImgCut(false, n, hi1, x1, y1, x2, y2);
                    }

                    double[] temp = new double[imgs.Length];
                    ///判断图像是否前半部分灰度小于后半部分灰度
                    int nd2 = (int)(n / 2);
                    double qian = 0, hou = 0;
                    for (int i = 0; i < nd2; i++)
                    {
                        qian += imgs[i];
                        hou += imgs[i + nd2];
                    }
                    if (qian < hou) //正常顺序
                    {
                        temp = imgs;
                    }
                    else
                    {
                        for (int i = 0; i < imgs.Length; i++)
                        {

                            temp[i] = imgs[imgs.Length - i - 1];
                        }
                    }




                    
                    BitmapSource ShuangsiCut = imgFormatTransform.arrytobitmapsource(temp, n, hi1, (int)imgs.Max(), (int)imgs.Min());
                    DefectInfo info = new DefectInfo(0, x1, y1, x2, y2, null);
                    defectInfo.Add(info);
                    drawNeeded = true;
                    imgQualityEvaluation.shuangsi(ShuangsiCut, imgs, n, hi1); //计算分辨率                 
                    CancelType = "ss";
                    clearlines();
                });
            }
            else
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (x1 < 11 || x2 < 11 || y1 < 11 || y2 < 11 || x1 > imgPinJieResult16XY .Width - 11 || x2 > imgPinJieResult16XY.Width - 11 || y1 > imgPinJieResult16XY.Height - 11 || y2 > imgPinJieResult16XY.Height - 11)
                    {
                        System.Windows.Forms.MessageBox.Show("位置超出边缘");
                        return;
                    }
                    if (Math.Abs(x1 - x2) < 5 && Math.Abs(y1 - y2) < 5) return;
                    GetShuangsiWidth demo = new GetShuangsiWidth();
                    demo.Title = "宽度选择";
                    demo.ShowDialog();
                    if (!shuangsiCountinue) { clearlines(); return; }
                    int nx, ny, n;
                    double[] imgs;

                    int hi2 = (hi1 - 1) / 2;//hi1为双丝截取宽度值，为全局变量，可在弹窗中设定
                    nx = Math.Abs(x2 - x1);
                    ny = Math.Abs(y2 - y1);
                    //取双丝灰度曲线
                    if (nx > ny) //若横轴较长，按横坐标处理
                    {
                        n = nx + 1;
                        if (n < 10) return;
                        imgs = imgQualityEvaluation.GetImgCutpj(true, n, hi1, x1, y1, x2, y2);
                    }
                    else//若纵轴较长，按纵坐标处理
                    {
                        n = ny + 1;
                        if (n < 10) return;
                        imgs = imgQualityEvaluation.GetImgCutpj(false, n, hi1, x1, y1, x2, y2);
                    }

                    double[] temp = new double[imgs.Length];
                    ///判断图像是否前半部分灰度小于后半部分灰度
                    int nd2 = (int)(n / 2);
                    double qian = 0, hou = 0;
                    for (int i = 0; i < nd2; i++)
                    {
                        qian += imgs[i];
                        hou += imgs[i + nd2];
                    }
                    if (qian < hou) //正常顺序
                    {
                        temp = imgs;
                    }
                    else
                    {
                        for (int i = 0; i < imgs.Length; i++)
                        {

                            temp[i] = imgs[imgs.Length - i - 1];
                        }
                    }
                    BitmapSource ShuangsiCut = imgFormatTransform.arrytobitmapsource(temp, n, hi1, (int)imgs.Max(), (int)imgs.Min());
                    DefectInfo info = new DefectInfo(0, x1, y1, x2, y2, null);
                    defectInfo.Add(info);
                    drawNeeded = true;
                    imgQualityEvaluation.shuangsi(ShuangsiCut, imgs, n, hi1); //计算分辨率                 
                    CancelType = "ss";
                    clearlines();
                });
            }
        }
        public void GrayMeasure(int x1, int x2, int y1, int y2)//提取灰度区域
        {
            excelPath = CreateGrayExcel(filepath + pictureName,0);
            if (!pinjie)
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (x1 < 11 || x2 < 11 || y1 < 11 || y2 < 11 || x1 > dcmImage.Width - 11 || x2 > dcmImage.Width - 11 || y1 > dcmImage.Height - 11 || y2 > dcmImage.Height - 11)
                    {
                        System.Windows.Forms.MessageBox.Show("位置超出边缘");
                        return;
                    }

                    if (Math.Abs(x1 - x2) < 5 && Math.Abs(y1 - y2) < 5) return;
                    int nx, ny, n;
                    double[] imgs;

                    hi1 = 21;
                    int hi2 = (hi1 - 1) / 2;//hi1为双丝截取宽度值，为全局变量，可在弹窗中设定
                    nx = Math.Abs(x2 - x1);
                    ny = Math.Abs(y2 - y1);
                    //取双丝灰度曲线
                    if (nx > ny) //若横轴较长，按横坐标处理
                    {
                        n = nx + 1;
                        if (n < 10) return;
                        imgs = imgQualityEvaluation.GetImgCut(true, n, hi1, x1, y1, x2, y2);
                    }
                    else//若纵轴较长，按纵坐标处理
                    {
                        n = ny + 1;
                        if (n < 10) return;
                        imgs = imgQualityEvaluation.GetImgCut(false, n, hi1, x1, y1, x2, y2);
                    }

                    double[] temp = new double[imgs.Length];
                    ///判断图像是否前半部分灰度小于后半部分灰度
                    int nd2 = (int)(n / 2);
                    double qian = 0, hou = 0;
                    for (int i = 0; i < nd2; i++)
                    {
                        qian += imgs[i];
                        hou += imgs[i + nd2];
                    }
                    if (qian < hou) //正常顺序
                    {
                        temp = imgs;
                    }
                    else
                    {
                        temp = imgs;
                        //for (int i = 0; i < imgs.Length; i++)
                        //{

                        //    temp[i] = imgs[imgs.Length - i - 1];
                        //}
                    }

                    BitmapSource ShuangsiCut = imgFormatTransform.arrytobitmapsource(temp, n, hi1, (int)imgs.Max(), (int)imgs.Min());
                    drawNeeded = true;
                    ChartGray demo = new ChartGray();
                    demo.Title = "灰度测量";
                    demo.InitChart(imgs,  0, n - 1);
                    demo.showGray(ShuangsiCut);
                    demo.ShowDialog();
                    //CancelType = "ss";
                    clearlines();
                    Thread saveGMExcel = new Thread(() => SaveGrayExcel(excelPath, imgs, n));
                    saveGMExcel.Start();
                });
            }
            else
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (x1 < 11 || x2 < 11 || y1 < 11 || y2 < 11 || x1 > imgPinJieResult16XY.Width - 11 || x2 > imgPinJieResult16XY.Width - 11 || y1 > imgPinJieResult16XY.Height - 11 || y2 > imgPinJieResult16XY.Height - 11)
                    {
                        System.Windows.Forms.MessageBox.Show("位置超出边缘");
                        return;
                    }
                    if (Math.Abs(x1 - x2) < 5 && Math.Abs(y1 - y2) < 5) return;
                    int nx, ny, n;
                    double[] imgs;

                    hi1 = 1;
                    int hi2 = (hi1 - 1) / 2;//hi1为双丝截取宽度值，为全局变量，可在弹窗中设定
                    nx = Math.Abs(x2 - x1);
                    ny = Math.Abs(y2 - y1);
                    //取双丝灰度曲线
                    if (nx > ny) //若横轴较长，按横坐标处理
                    {
                        n = nx + 1;
                        if (n < 10) return;
                        imgs = imgQualityEvaluation.GetImgCutpj(true, n, hi1, x1, y1, x2, y2);
                    }
                    else//若纵轴较长，按纵坐标处理
                    {
                        n = ny + 1;
                        if (n < 10) return;
                        imgs = imgQualityEvaluation.GetImgCutpj(false, n, hi1, x1, y1, x2, y2);
                    }

                    double[] temp = new double[imgs.Length];
                    ///判断图像是否前半部分灰度小于后半部分灰度
                    int nd2 = (int)(n / 2);
                    double qian = 0, hou = 0;
                    for (int i = 0; i < nd2; i++)
                    {
                        qian += imgs[i];
                        hou += imgs[i + nd2];
                    }
                    if (qian < hou) //正常顺序
                    {
                        temp = imgs;
                    }
                    else
                    {
                        temp = imgs;
                        //for (int i = 0; i < imgs.Length; i++)
                        //{

                        //    temp[i] = imgs[imgs.Length - i - 1];
                        //}
                    }
                    BitmapSource ShuangsiCut = imgFormatTransform.arrytobitmapsource(temp, n, hi1, (int)imgs.Max(), (int)imgs.Min());
                    drawNeeded = true;
                    ChartGray demo = new ChartGray();
                    demo.Title = "灰度测量";
                    demo.InitChart(imgs, 0, n - 1);
                    demo.showGray(ShuangsiCut);
                    demo.ShowDialog();
                    //CancelType = "ss";
                    clearlines();
                    Thread saveGMExcel = new Thread(() => SaveGrayExcel(excelPath, imgs, n));
                    saveGMExcel.Start();
                    
                });
            }
        }
        public void GrayStatic(int x1, int x2, int y1, int y2)//提取灰度区域
        {
            excelPath = CreateGrayExcel(filepath + pictureName, 1);
            if (!pinjie)
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (x1 < 11 || x2 < 11 || y1 < 11 || y2 < 11 || x1 > dcmImage.Width - 11 || x2 > dcmImage.Width - 11 || y1 > dcmImage.Height - 11 || y2 > dcmImage.Height - 11)
                    {
                        System.Windows.Forms.MessageBox.Show("位置超出边缘");
                        return;
                    }
                    int[] grayCounts = new int[65535];
                    for(int i = x1; i < x2; ++i)
                    {
                        for(int j = y1; j < y2; ++j)
                        {
                            grayCounts[(int)iPixelData.GetPixel(i, j)]++;
                        }
                    }
                    int valueCounts = 0;
                    int countsCounts = 0;
                    List<ModelGrayCounts> garymodels = new List<ModelGrayCounts>(); //灰度值列表
                    
                    for (int i = 0; i < 65535; ++i)
                    {
                        if (grayCounts[i] != 0)
                        {
                            valueCounts++;
                            ModelGrayCounts mGC = new ModelGrayCounts();
                            mGC.Value = i;
                            mGC.Counts = grayCounts[i];
                            garymodels.Add(mGC);
                        }
                    }
                    //GrayValue = 99;
                    grayStaticGridModel.ItemsSource = null;
                    grayStaticGridModel.ItemsSource = garymodels;
                    Thread saveGSExcel = new Thread(() => SaveGrayValueandCountsExcel(excelPath, grayCounts, valueCounts));
                    saveGSExcel.Start();
                    clearlines();
                    Thread.Sleep(2000);
                });
            }
            else
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (x1 < 11 || x2 < 11 || y1 < 11 || y2 < 11 || x1 > imgPinJieResult16XY.Width - 11 || x2 > imgPinJieResult16XY.Width - 11 || y1 > imgPinJieResult16XY.Height - 11 || y2 > imgPinJieResult16XY.Height - 11)
                    {
                        System.Windows.Forms.MessageBox.Show("位置超出边缘");
                        return;
                    }
                    if (Math.Abs(x1 - x2) < 5 && Math.Abs(y1 - y2) < 5) return;
                    int[] grayCounts = new int[65535];
                    for (int i = x1; i < x2; ++i)
                    {
                        for (int j = y1; j < y2; ++j)
                        {
                            grayCounts[(int)iPixelData.GetPixel(i, j)]++;
                        }
                    }
                    int valueCounts = 0;
                    int countsCounts = 0;
                    List<ModelGrayCounts> garymodels = new List<ModelGrayCounts>(); //灰度值列表

                    for (int i = 0; i < 65535; ++i)
                    {
                        if (grayCounts[i] != 0)
                        {
                            valueCounts++;
                            ModelGrayCounts mGC = new ModelGrayCounts();
                            mGC.Value = i;
                            mGC.Counts = grayCounts[i];
                            garymodels.Add(mGC);
                        }
                    }
                    //GrayValue = 99;
                    grayStaticGridModel.ItemsSource = null;
                    grayStaticGridModel.ItemsSource = garymodels;
                    Thread saveGSExcel = new Thread(() => SaveGrayValueandCountsExcel(excelPath, grayCounts, valueCounts));
                    saveGSExcel.Start();
                    clearlines();
                    Thread.Sleep(2000);
                });
            }
        }
        private void InitRectLines()
        {
            //System.Windows.Media.Brush or = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 127, 39));
            Rectline1 = new Line();
            Rectline1.Stroke = or;
            Rectline1.StrokeThickness = 1;
            Rectline1.Opacity = 0.9;
            this.BaseCan.Children.Add(Rectline1);
            BaseCan.RegisterName("newLine1", Rectline1);//注册新生成的控件以便删除

            Rectline2 = new Line();
            Rectline2.Stroke = or;
            Rectline2.StrokeThickness = 1;
            Rectline2.Opacity = 0.9;
            this.BaseCan.Children.Add(Rectline2);
            BaseCan.RegisterName("newLine2", Rectline2);//注册新生成的控件以便删除

            Rectline3 = new Line();
            Rectline3.Stroke = or;
            Rectline3.StrokeThickness = 1;
            Rectline3.Opacity = 0.9;
            this.BaseCan.Children.Add(Rectline3);
            BaseCan.RegisterName("newLine3", Rectline3);//注册新生成的控件以便删除

            Rectline4 = new Line();
            Rectline4.Stroke = or;
            Rectline4.StrokeThickness = 1;
            Rectline4.Opacity = 0.9;
            this.BaseCan.Children.Add(Rectline4);
            BaseCan.RegisterName("newLine4", Rectline4);//注册新生成的控件以便删除

        }//普通模式测面积时的矩形框线
        private void LocateRectLines()
        {
            Rectline1.X1 = lastpoint.X;
            Rectline1.Y1 = lastpoint.Y;
            Rectline1.X2 = currentpoint.X;
            Rectline1.Y2 = lastpoint.Y;

            Rectline2.X1 = lastpoint.X;
            Rectline2.Y1 = lastpoint.Y;
            Rectline2.X2 = lastpoint.X;
            Rectline2.Y2 = currentpoint.Y;

            Rectline3.X1 = currentpoint.X;
            Rectline3.Y1 = currentpoint.Y;
            Rectline3.X2 = currentpoint.X;
            Rectline3.Y2 = lastpoint.Y;

            Rectline4.X1 = currentpoint.X;
            Rectline4.Y1 = currentpoint.Y;
            Rectline4.X2 = lastpoint.X;
            Rectline4.Y2 = currentpoint.Y;
        }
        private void LocateRectLines(int x1, int y1, int x2, int y2)
        {
            Rectline1.X1 = x1;
            Rectline1.Y1 = y1;
            Rectline1.X2 = x1;
            Rectline1.Y2 = y2;

            Rectline2.X1 = x1;
            Rectline2.Y1 = y1;
            Rectline2.X2 = x2;
            Rectline2.Y2 = y1;

            Rectline3.X1 = x2;
            Rectline3.Y1 = y1;
            Rectline3.X2 = x2;
            Rectline3.Y2 = y2;

            Rectline4.X1 = x1;
            Rectline4.Y1 = y2;
            Rectline4.X2 = x2;
            Rectline4.Y2 = y2;
        }
        private void cleardefectline()
        {
            Line defectline = BaseCan.FindName("defectline") as Line;//找到新添加的按钮
            if (defectline != null)
            {
                BaseCan.Children.Remove(defectline);//移除对应控件
                BaseCan.UnregisterName("defectline");//注销名字                
            }                       
        }//测缺陷线段时的标识线段
        private void clearlines()
        {
            Line currentline = BaseCan.FindName("newLine") as Line;//找到新添加的按钮
            if (currentline != null)
            {
                BaseCan.Children.Remove(currentline);//移除对应控件
                BaseCan.UnregisterName("newLine");//注销名字
            }
            Line Rectline1 = BaseCan.FindName("newLine1") as Line;//找到新添加的按钮
            if (Rectline1 != null)
            {
                BaseCan.Children.Remove(Rectline1);//移除对应控件
                BaseCan.UnregisterName("newLine1");//注销名字
            }
            Line Rectline2 = BaseCan.FindName("newLine2") as Line;//找到新添加的按钮
            if (Rectline2 != null)
            {
                BaseCan.Children.Remove(Rectline2);//移除对应控件
                BaseCan.UnregisterName("newLine2");//注销名字
            }
            Line Rectline3 = BaseCan.FindName("newLine3") as Line;//找到新添加的按钮
            if (Rectline3 != null)
            {
                BaseCan.Children.Remove(Rectline3);//移除对应控件
                BaseCan.UnregisterName("newLine3");//注销名字
            }
            Line Rectline4 = BaseCan.FindName("newLine4") as Line;//找到新添加的按钮
            if (Rectline4 != null)
            {
                BaseCan.Children.Remove(Rectline4);//移除对应控件
                BaseCan.UnregisterName("newLine4");//注销名字
            }
            TextBox CurrentText = BaseCan.FindName("newText") as TextBox;//找到新添加的按钮
            if (CurrentText != null)
            {
                BaseCan.Children.Remove(CurrentText);//移除对应控件
                BaseCan.UnregisterName("newText");//注销名字
            }
            Line vline = BaseCan.FindName("VLine") as Line;//找到新添加的按钮
            if (vline != null)
            {
                BaseCan.Children.Remove(vline);//移除对应控件
                BaseCan.UnregisterName("VLine");//注销名字
            }
        }
        private void InitRulerRect()
        {
            Rectline5 = new Line();
            Rectline5.Stroke = new SolidColorBrush(Colors.OrangeRed);
            Rectline5.StrokeThickness = 2;
            Rectline5.Opacity = 0.9;
            this.BaseCan.Children.Add(Rectline5);
            BaseCan.RegisterName("newLine5", Rectline5);//注册新生成的控件以便删除

            Rectline6 = new Line();
            Rectline6.Stroke = new SolidColorBrush(Colors.OrangeRed);
            Rectline6.StrokeThickness = 2;
            Rectline6.Opacity = 0.9;
            this.BaseCan.Children.Add(Rectline6);
            BaseCan.RegisterName("newLine6", Rectline6);//注册新生成的控件以便删除

            Rectline7 = new Line();
            Rectline7.Stroke = new SolidColorBrush(Colors.OrangeRed);
            Rectline7.StrokeThickness = 2;
            Rectline7.Opacity = 0.9;
            this.BaseCan.Children.Add(Rectline7);
            BaseCan.RegisterName("newLine7", Rectline7);//注册新生成的控件以便删除

            Rectline8 = new Line();
            Rectline8.Stroke = new SolidColorBrush(Colors.OrangeRed);
            Rectline8.StrokeThickness = 2;
            Rectline8.Opacity = 0.9;
            this.BaseCan.Children.Add(Rectline8);
            BaseCan.RegisterName("newLine8", Rectline8);//注册新生成的控件以便删除
        }//评定框线段
        private void LocateRulerRect(int x1, int y1, int x2, int y2)
        {
            Rectline5.X1 = x1;
            Rectline5.Y1 = y1;
            Rectline5.X2 = x1;
            Rectline5.Y2 = y2;

            Rectline6.X1 = x1;
            Rectline6.Y1 = y1;
            Rectline6.X2 = x2;
            Rectline6.Y2 = y1;

            Rectline7.X1 = x2;
            Rectline7.Y1 = y1;
            Rectline7.X2 = x2;
            Rectline7.Y2 = y2;

            Rectline8.X1 = x1;
            Rectline8.Y1 = y2;
            Rectline8.X2 = x2;
            Rectline8.Y2 = y2;
        }



        private void ClearRuler()
        {
            Line Rectline5 = BaseCan.FindName("newLine5") as Line;//找到新添加的按钮
            if (Rectline5 != null)
            {
                BaseCan.Children.Remove(Rectline5);//移除对应控件
                BaseCan.UnregisterName("newLine5");//注销名字
            }
            Line Rectline6 = BaseCan.FindName("newLine6") as Line;//找到新添加的按钮
            if (Rectline6 != null)
            {
                BaseCan.Children.Remove(Rectline6);//移除对应控件
                BaseCan.UnregisterName("newLine6");//注销名字
            }
            Line Rectline7 = BaseCan.FindName("newLine7") as Line;//找到新添加的按钮
            if (Rectline7 != null)
            {
                BaseCan.Children.Remove(Rectline7);//移除对应控件
                BaseCan.UnregisterName("newLine7");//注销名字
            }
            Line Rectline8 = BaseCan.FindName("newLine8") as Line;//找到新添加的按钮
            if (Rectline8 != null)
            {
                BaseCan.Children.Remove(Rectline8);//移除对应控件
                BaseCan.UnregisterName("newLine8");//注销名字
            }

            System.Windows.Shapes.Ellipse e1 = BaseCan.FindName("newEllipse1") as System.Windows.Shapes.Ellipse;
            if (e1 != null)
            {
                BaseCan.Children.Remove(e1);//移除对应控件
                BaseCan.UnregisterName("newEllipse1");//注销名字
            }
            System.Windows.Shapes.Ellipse e2 = BaseCan.FindName("newEllipse2") as System.Windows.Shapes.Ellipse;
            if (e2 != null)
            {
                BaseCan.Children.Remove(e2);//移除对应控件
                BaseCan.UnregisterName("newEllipse2");//注销名字
            }
            System.Windows.Shapes.Ellipse e3 = BaseCan.FindName("newEllipse3") as System.Windows.Shapes.Ellipse;
            if (e3 != null)
            {
                BaseCan.Children.Remove(e3);//移除对应控件
                BaseCan.UnregisterName("newEllipse3");//注销名字
            }
            System.Windows.Shapes.Ellipse e4 = BaseCan.FindName("newEllipse4") as System.Windows.Shapes.Ellipse;
            if (e4 != null)
            {
                BaseCan.Children.Remove(e4);//移除对应控件
                BaseCan.UnregisterName("newEllipse4");//注销名字
            }
            System.Windows.Shapes.Ellipse e6 = BaseCan.FindName("newEllipse6") as System.Windows.Shapes.Ellipse;
            if (e6 != null)
            {
                BaseCan.Children.Remove(e6);//移除对应控件
                BaseCan.UnregisterName("newEllipse6");//注销名字
            }
            System.Windows.Shapes.Ellipse e8 = BaseCan.FindName("newEllipse8") as System.Windows.Shapes.Ellipse;
            if (e8 != null)
            {
                BaseCan.Children.Remove(e8);//移除对应控件
                BaseCan.UnregisterName("newEllipse8");//注销名字
            }
            System.Windows.Shapes.Ellipse e03 = BaseCan.FindName("newEllipse03") as System.Windows.Shapes.Ellipse;
            if (e03 != null)
            {
                BaseCan.Children.Remove(e03);//移除对应控件
                BaseCan.UnregisterName("newEllipse03");//注销名字
            }
            System.Windows.Shapes.Ellipse e04 = BaseCan.FindName("newEllipse04") as System.Windows.Shapes.Ellipse;
            if (e04 != null)
            {
                BaseCan.Children.Remove(e04);//移除对应控件
                BaseCan.UnregisterName("newEllipse04");//注销名字
            }
            System.Windows.Shapes.Ellipse e05 = BaseCan.FindName("newEllipse05") as System.Windows.Shapes.Ellipse;
            if (e05 != null)
            {
                BaseCan.Children.Remove(e05);//移除对应控件
                BaseCan.UnregisterName("newEllipse05");//注销名字
            }
            System.Windows.Shapes.Ellipse e06 = BaseCan.FindName("newEllipse06") as System.Windows.Shapes.Ellipse;
            if (e06 != null)
            {
                BaseCan.Children.Remove(e06);//移除对应控件
                BaseCan.UnregisterName("newEllipse06");//注销名字
            }
            System.Windows.Shapes.Ellipse e07 = BaseCan.FindName("newEllipse07") as System.Windows.Shapes.Ellipse;
            if (e07 != null)
            {
                BaseCan.Children.Remove(e07);//移除对应控件
                BaseCan.UnregisterName("newEllipse07");//注销名字
            }
            TextBox dvalue = BaseCan.FindName("newText1") as TextBox;//找到新添加的按钮
            if (dvalue != null)
            {
                BaseCan.Children.Remove(dvalue);//移除对应控件
                BaseCan.UnregisterName("newText1");//注销名字
            }
        }


        private void DrawCrossHair()
        {
            xline.Stroke = Red;
            xline.StrokeThickness = 1;
            xline.Opacity = 0.9;
            yline.Stroke = Red;
            yline.StrokeThickness = 1;
            yline.Opacity = 0.9;
            xline.X1 = (int)MagImg.Width / 2 - 10;
            xline.Y1 = (int)MagImg.Height / 2;
            xline.X2 = (int)MagImg.Width / 2 + 10;
            xline.Y2 = (int)MagImg.Height / 2;
            yline.X1 = (int)MagImg.Width / 2;
            yline.Y1 = (int)MagImg.Height / 2 - 10;
            yline.X2 = (int)MagImg.Width / 2;
            yline.Y2 = (int)MagImg.Height / 2 + 10;

            Line xxline = can.FindName("xLine") as Line;
            Line yyline = can.FindName("yLine") as Line;
            if (xxline == null && yyline == null)
            {
                this.can.Children.Add(xline);
                can.RegisterName("xLine", xline);
                this.can.Children.Add(yline);
                can.RegisterName("yLine", yline);//注册新生成的控件以便删除
            }
        }//画局部放大的红色准心

        #endregion

        #region        
        private void jiaozhun_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (MarkSwitch)
            {
                MessageBox.Show("请先退出缺陷标记模式");
                return;
            }            
            IsHla = !IsHla;
            clearlines();
            cleardefectline();
            //if (jiaozhun.Background == basecolor)
            if (IsHla)
            {
                showArea = false;
                showLine = true;
                winchange = false;
                //mianji.Background = basecolor; 
                IsAr = false;
                //changdu.Background = basecolor; 
                IsHlm = false;
                //finddk.Background = basecolor; 
                IsTw = false;
                IsTm = false;
                //snr.Background = basecolor; 
                IsSnr = false;
                //jiaozhun.Background = Yellow; 
                IsAw = false;

                jiaozhun demo = new jiaozhun();
                demo.Title = "长度选定";
                demo.ShowDialog();
                MessageBox.Show("请在图上标示参考线段");
                
            }
            else
            {
                showArea = true;
                showLine = false;
                winchange = true;
                //jiaozhun.Background = basecolor;                
            }

            //Line currentline = BaseCan.FindName("newLine") as Line;//找到新添加的按钮
            //if (currentline != null)
            //{
            //    double length;
            //    length = Math.Sqrt((currentline.X1 - currentline.X2) * (currentline.X1 - currentline.X2) + (currentline.Y1 - currentline.Y2) * (currentline.Y1 - currentline.Y2));
            //    //length = length / trans * correct;
            //    correct = help / (length / trans);
            //    MessageBox.Show("校准成功！");
            //}
            //else
            //{
            //    MessageBox.Show("请先在图上标示参考线段");
            //}
            //TextBox CurrentText = BaseCan.FindName("newText") as TextBox;//找到新添加的按钮
            //if (CurrentText != null)
            //{
            //    double angle = (double)Math.Atan2(Math.Abs(currentpoint.Y - lastpoint.Y), Math.Abs(currentpoint.X - lastpoint.X)) * 180.0F / (double)Math.PI;
            //    CurrentText.Text = "长度:" + jiaozhunTextbox.Text.ToString() + "mm";
            //}

        }
        #endregion

        #region//测量按键
        private void changdu_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            IsHlm = !IsHlm;           
            clearlines();
            cleardefectline();
            defectline = null; 
            if (IsHlm)
            {
                if (MarkSwitch == true)
                {
                    if (defcoords[0] != -1 && (LineorRec != 1))
                    {
                        clearall();
                    }
                    if (rec1 || rec2 || rec3 || round1)
                    {
                        rec1 = false; rec2 = false; rec3 = false; round1 = false;
                        ClearRuler(); ClearArea();
                    }
                }
                LineorRec = 1; classflag = LineorRec;                
                ResetAllSwitchtoFalseEXCEPT(ref showLine);
                IsHlm = true;           
            }
            else
            {
                SetDispalyState(ref showArea,ref winchange);
            }
        }

        private void mianji_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            IsAr = !IsAr;
            clearlines();
            cleardefectline();
            defectline = null;           
            if (IsAr)
            {
                if (MarkSwitch == true)
                {
                    if (defcoords[0] != -1 && (LineorRec != -1))
                    {                       
                        clearall();
                    }
                    if(rec1 || rec2 || rec3 || round1)
                    {
                        rec1 = false; rec2 = false; rec3 = false;round1 = false;
                        ClearRuler();ClearArea();
                    }
                }
                LineorRec = -1;
                classflag = LineorRec;                
                ResetAllSwitchtoFalseEXCEPT(ref showArea);
                IsAr = true;
            }
            else
            {
                SetDispalyState(ref winchange);               
            }
        }

        private void finddk_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                //MessageBox.Show("拼接后不允许修改！");
                //return;
                IsTw = !IsTw;
                if (IsTw)
                {
                    ResetAllSwitchtoFalseEXCEPT(ref showLine);
                    IsAddDefect(false);
                    IsTw = true;
                    clearlines();
                    cleardefectline();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
                else
                {
                    SetDispalyState(ref showArea, ref winchange);
                    if (CancelType == "ss")
                    {
                        RefreshImgDisplayPintu(ref imgPinJieResult16 );
                        CancelType = null;
                    }

                }
            }
            else
            {
                IsTw = !IsTw;
                if (IsTw)
                {
                    ResetAllSwitchtoFalseEXCEPT(ref showLine);
                    IsAddDefect(false);
                    IsTw = true;
                    clearlines();
                    cleardefectline();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
                else
                {
                    SetDispalyState(ref showArea, ref winchange);
                    if (CancelType == "ss")
                    {
                        RefreshImgDisplay(ref dcmImage);
                        CancelType = null;
                    }

                }
            }
            
        }

        private void snr_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                //MessageBox.Show("拼接后不允许修改！");
                //return;
                IsSnr = !IsSnr;
                if (isSnr)
                {
                    if (CancelType == "ss")
                    {
                        RefreshImgDisplayPintu(ref imgPinJieResult16 );
                        CancelType = null;
                    }
                    ResetAllSwitchtoFalseEXCEPT(ref showArea);
                    IsAddDefect(false);
                    IsSnr = true;
                    clearlines();
                    cleardefectline();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
                else
                {
                    SetDispalyState(ref winchange);
                    clearlines();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
            }
            else
            {
                IsSnr = !IsSnr;
                if (isSnr)
                {
                    if (CancelType == "ss")
                    {
                        RefreshImgDisplay(ref dcmImage);
                        CancelType = null;
                    }
                    ResetAllSwitchtoFalseEXCEPT(ref showArea);
                    IsAddDefect(false);
                    IsSnr = true;
                    clearlines();
                    cleardefectline();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
                else
                {
                    SetDispalyState(ref winchange);
                    clearlines();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;
                }
            }
            
        }
        //private void LoadFenbianv(string a)
        //{
        //    double[] fenb = { 800, 630, 500, 400, 320, 250, 200, 160, 130, 100, 80, 63, 50 };
        //    string[] stringarray = new string[90];
        //    List<string> txt = new List<string>();
        //    if (!File.Exists(a + "文件信息.txt"))
        //    {
        //        //MessageBox.Show("当前没有已保存的文件信息");
        //        return;
        //    }
        //    else
        //    {
        //        //FileStream fs = new FileStream(, FileMode.Open, FileAccess.ReadWrite);
        //        FileStream fs2 = new FileStream(a + "文件信息.txt", FileMode.Open, FileAccess.ReadWrite);
        //        using (StreamReader sr = new StreamReader(fs2))
        //        {
        //            {
        //                int lineCount = 0;
        //                while (sr.Peek() > 0)
        //                {
        //                    lineCount++;
        //                    string tmp = sr.ReadLine();
        //                    txt.Add(tmp);
        //                    //MessageBox.Show(tmp);
        //                }

        //            }
        //        }

        //        int i = 0;
        //        foreach (string b in txt)
        //        {
        //            if (b.IndexOf("：") != -1)
        //            {
        //                //stringarray[i] = a.Substring(a.LastIndexOf(":") + 1, a.Length - a.LastIndexOf(":") - 1);
        //                stringarray[i] = b.Substring(b.LastIndexOf("：") + 1);
        //                i++;
        //            }
        //        }
        //        if (string.IsNullOrEmpty(stringarray[44])) { return; }
        //        else
        //        {
        //            int k = int.Parse(stringarray[44].Substring(1));
        //            fenbianlv = fenb[k - 1];
        //        }
        //    }
        //}

        private void Rectangle1_Click(object sender, RoutedEventArgs e)
        {
            if (!MarkSwitch)
            {
                MessageBox.Show("未处于标记缺陷状态，功能禁用");
                return;
            }
            //ClearRuler();
            //ClearArea();
            rec1 = !rec1;          
            if (!rec1)
            {
                ClearRuler();
                ClearArea();
            }
            else
            {
                if (defcoords[0] != -1 && (LineorRec !=0))
                {
                    clearall();
                }
                LineorRec = 0; classflag = LineorRec;
                SetDispalyState();
                IsHlm = false;               
                IsAw = false;
                Ishbiao = false;
                Isvbiao = false;
                ClearRuler();
                ClearArea();
                rec1 = true;               
            }
        }
        private void Rectangle2_Click(object sender, RoutedEventArgs e)
        {
            if (!MarkSwitch)
            {
                MessageBox.Show("未处于标记缺陷状态，功能禁用");
                return;
            }
            //ClearRuler();
            //ClearArea();
            rec2 = !rec2;
            
            if (!rec2)
            {
                ClearRuler();
                ClearArea();
            }
            else
            {
                if (defcoords[0] != -1 && (LineorRec != 0))
                {
                    clearall();
                }
                LineorRec = 0; classflag = LineorRec;
                IsHlm = false;
                SetDispalyState();
                IsAw = false;
                Ishbiao = false;
                Isvbiao = false;
                ClearRuler();
                ClearArea();
                rec2 = true;
            }
        }
        private void Rectangle3_Click(object sender, RoutedEventArgs e)
        {
            if (!MarkSwitch)
            {
                MessageBox.Show("未处于标记缺陷状态，功能禁用");
                return;
            }
            //ClearRuler();
            //ClearArea();
            rec3 = !rec3;
            
            if (!rec3)
            {
                ClearRuler();
                ClearArea();
            }
            else
            {
                if (defcoords[0] != -1 && (LineorRec != 0))
                {
                    clearall();
                }
                LineorRec = 0; classflag = LineorRec;
                IsHlm = false;
                SetDispalyState();
                IsAw = false;
                Ishbiao = false;
                Isvbiao = false;
                ClearRuler();
                ClearArea();
                rec3 = true;
            }
        }

        private void type1_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            if (!MarkSwitch)
            {
                MessageBox.Show("未处于标记缺陷状态，功能禁用");
                return;
            }
            round1 = !round1;
            if(round1)
            {
                ClearRuler();
                ClearArea();
                round1 = true;                
                ResetAllSwitchtoFalseEXCEPT();                
            }
            if (!round1)
            {
                ClearRuler();
                ClearArea();
            }
        }

        private void clearall()
        {
            ClearRuler();
            ClearArea();
            cleardefectline();
            clearlines();
            
            currentline = null;vline = null;
            defectline = null;
            Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null;
            Rectline5 = null; Rectline6 = null; Rectline7 = null; Rectline8 = null;
            ImgProcessFlag = false;firstLoadFlag = false;bcifProcessFlag = false; DefectShow = false;
            ImageEnhanceNum = 0;
            IsIH1 = false;IsIH2 = false;IsIH3 = false;IsIH4 = false;IsIH5 = false;IsIH6 = false;
            Slider_Bright.Value = 0;Slider_Contrast.Value = 0;
            Resetpinjieshow();
            Resetdefcoords();
            if (pinjie)
            {
                RefreshImgDisplayPintu(ref imgPinJieResult16);
            }
            else
            {               
                RefreshImgDisplay(ref dcmImage);
            }
        }
        private void ClearArea_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            clearall(); 
        }
        private void ClearArea()
        {
            Rectline5 = null; Rectline6 = null; Rectline7 = null; Rectline8 = null;
            e1 = null; e2 = null; e3 = null; e4 = null; e6 = null; e8 = null; e03 = null; e04 = null; e05 = null; e06 = null; e07 = null;
            rec1 = false; rec2 = false; rec3 = false;
            round1 = false;
            DValue = null;
        }


        #endregion

        private void zhifangtu_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (dcmImage == null) return;
            histgram demo1 = new histgram();
            demo1.Title = "图像直方图";
            demo1.Inithistogram(iPixelData);
            demo1.ShowDialog();
        }

        public static Image<Gray, ushort> imgPinJieResult16, imgPinJieResult16XY;

        private void btn_pinjie_Click(object sender, RoutedEventArgs e)//一次拼5张，减小内存占用
        {

            try
            {
                //s5 = "0";
                //SaveTxt();

                if (MarkSwitch)
                {
                    MessageBox.Show("请先退出缺陷标记模式");
                    return;
                }
                if (listbox.ItemsSource == null)
                {
                    MessageBox.Show("请导入文件");
                    return;
                }
                DefectShow = false;
                IsSp = !IsSp;
                CancelType = null;
                //MessageBoxResult mr = MessageBox.Show("是否按照当前旋转、灰度等属性进行拼接？", "提示", MessageBoxButton.OKCancel);
                //if (mr == MessageBoxResult.OK)
                //{
                if (IsSp)
                {
                    SetCutSizeOnPintu demo = new SetCutSizeOnPintu();
                    demo.Title = "输入裁切宽度";
                    demo.ShowDialog();
                    if (!pintuContinue)
                    {
                        IsSp = !IsSp;
                        return;
                    }
                    if (IsSd)
                    {
                        flagtext.Visibility = Visibility.Collapsed;
                        flagtext.IsEnabled = false;
                        IsSd = false;
                    }
                    n0 = 0;
                    pinjie = true;
                    // }
                    //else { return; }
                    ResetAllSwitchtoFalseEXCEPT();
                    clearlines();
                    currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                    CurrentText = null;


                    OpenFileDialog openFileDlg = new OpenFileDialog();
                    openFileDlg.Multiselect = true;
                    //openFileDlg.Filter = "图片格式(*.dcm;*.DICON;*.dcn)|*.dcm;*.DICON";
                    openFileDlg.Filter = "图像文件|*.dcm;*.dcn;*.DICONDE|所有文件|*.*";
                    openFileDlg.ShowDialog();
                    picName.Clear();
                    for (int i = 0; i < openFileDlg.FileNames.Length; i++)
                    {
                        //MessageBox.Show(openFileDlg.FileNames[i]);
                        string fileName = openFileDlg.FileNames[i].Substring(openFileDlg.FileNames[i].LastIndexOf("\\") + 1);
                        picName.Add(fileName);
                    }



                    int x = picName.Count;
                    //if (x > 5) x = 5;

                    //第一幅图像的宽高减去裁切宽度

                    int firstwidth = BaseBps.PixelWidth - 2 * cutSize;
                    int firstheight = BaseBps.PixelHeight - 2 * cutSize;
                    //Bitmap resultBitmap = new Bitmap(firstwidth * x, firstheight);
                    //imgPinJieResult16 = new Image<Gray, ushort>(firstwidth*x, firstheight);

                    Bitmap resultBitmap = new Bitmap(firstwidth * picName.Count, firstheight);
                    imgPinJieResult16 = new Image<Gray, ushort>(firstwidth * picName.Count, firstheight);

                    //遍历当前目录中的每一张图片
                    for (int i = 0; i < picName.Count; i++)
                    {
                        //Image<Gray, ushort> sources = GetImage(picName[i]);
                        //Bitmap sourceBitmap = sources.Bitmap;
                        //int tmpwidth = sources.Bitmap.Width - 2 * cutSize;//当前图像的宽高减去裁切像素
                        //int tmpheight = sources.Bitmap.Height - 2 * cutSize;
                        //if (tmpheight != firstheight || tmpwidth != firstwidth)//当前图像和第一张图像的尺寸不一致则退出
                        //{
                        //    MessageBox.Show("图像尺寸不匹配！");
                        //    return;
                        //}
                        //System.Drawing.Point point = new System.Drawing.Point(cutSize, cutSize);
                        //System.Drawing.Size size = new System.Drawing.Size(tmpwidth, tmpheight);
                        //System.Drawing.Rectangle rl = new System.Drawing.Rectangle(point, size);
                        //drawInstance.DrawImage(ref resultBitmap, ref sourceBitmap, rl, System.Drawing.GraphicsUnit.Pixel, firstwidth * i, 0);

                        ///////////////////////////////////////////////////////////////////////////////////////////

                        Image<Gray, ushort> sources = GetImage(picName[i]);

                        System.Drawing.Rectangle rectsource;
                        System.Drawing.Rectangle rectresult;
                        //if (s5=="0"||s5=="180")
                        //{
                            rectsource = new System.Drawing.Rectangle(cutSize, cutSize, sources.Cols - 2 * cutSize, sources.Rows - 2 * cutSize);
                            rectresult = new System.Drawing.Rectangle(firstwidth * i, 0, firstwidth, firstheight);
                        //}
                        //else
                        //{
                        //    rectsource = new System.Drawing.Rectangle(cutSize, cutSize, sources.Cols - 2 * cutSize, sources.Rows - 2 * cutSize);
                        //    rectresult = new System.Drawing.Rectangle(firstwidth * i, 0, firstheight, firstwidth);
                        //}

                        sources.ROI = rectsource;
                        imgPinJieResult16.ROI = rectresult;
                        sources.CopyTo(imgPinJieResult16);
                        sources.ROI = System.Drawing.Rectangle.Empty;
                        imgPinJieResult16.ROI = System.Drawing.Rectangle.Empty;
                    }
                    

                    if (pintu != null)
                    {
                        pintu.Dispose();
                        pintu = null;
                    }
                    pintu = imgPinJieResult16.Convert<Gray, byte>();
                    imgPinJieResult16XY = imgPinJieResult16;

                    RefreshImgDisplayPintu(ref imgPinJieResult16);
                    tlt.X = 0;
                    tlt.Y = 0;
                    Scroll.ScrollToHome();
                    meangrayTB.Text = null;
                    currentgrayTB.Text = null;
                    if (s != null) s.Background = basecolor;
                    s = listbox.ItemContainerGenerator.ContainerFromIndex(0) as ListBoxItem;
                    s.Background = selectcolor;
                    listbox.ScrollIntoView(listbox.Items[0]);
                    listbox.SelectedIndex = -1;
                    SaveFileDialog saveFileDialog2 = new SaveFileDialog();
                    saveFileDialog2.Filter = "DICONDE(*.DICONDE)|*.DICONDE";
                    saveFileDialog2.RestoreDirectory = true;
                    saveFileDialog2.ShowDialog();
                    string path = saveFileDialog2.FileName;
                    if(IsGm)
                        excelPath = CreateGrayExcel(path,0);
                    if (IsGs)
                        excelPath = CreateGrayExcel(path, 1);
                    string fileExtension = System.IO.Path.GetExtension(path).ToUpper();
                    //path = path + $"(保存的第" + savenumber + $"个副本).dcm";

                    ushort[] rImg = new ushort[imgPinJieResult16XY.Width * imgPinJieResult16XY.Height];
                    unsafe
                    {
                        ushort* ptr = (ushort*)imgPinJieResult16XY.Mat.DataPointer;
                        for (int i = 0; i < rImg.Length; i++)
                        {

                            rImg[i] = ptr[i];
                        }
                    }
                    Utils.SaveDicomFile(rImg, path, imgPinJieResult16XY.Width, imgPinJieResult16XY.Height);


                    showArea = true;
                    winchange = true;
                    //s5 = "0";
                    //SaveTxt();


                }
                else
                {
                    pinjie = false;
                    DefectShow = false;
                    clearall();
                    ResetAllSwitchtoFalseEXCEPT(ref winchange);
                    IsAddDefect(false);
                    if (pintu != null)
                    {
                        pintu.Dispose();
                        pintu = null;
                    }
                    ThreadPool.QueueUserWorkItem(new WaitCallback(ref this.SmallPicture));
                    meangrayTB.Text = meangray;
                    RefreshImgDisplay(ref dcmImage);
                    GetAllImagePath(filepath);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("请重新选择图像！");
                IsSp = false;
                return;
            }


        }
        private Image<Gray, ushort> GetImage(string path)
        {
            DicomFile dcmFile = DicomFile.Open(filepath + "\\" + path);
            DicomDataset dcmDataSet = dcmFile.Dataset;
            DicomImage dcmImage = new DicomImage(dcmDataSet);
            dcmImage.WindowCenter = wincenter;
            dcmImage.WindowWidth = winwidth;
            //图像宽小于高，则旋转
            Image<Gray, ushort> imgGU;
            imgGU = Utils.DicomImage2UShortEmguImage(dcmImage);
            ////Image<Gray, ushort> imgGU = new Image<Gray, ushort>((Bitmap)dcmImage.RenderImage());
            //imgGU = imgProcess.ProcessImgGrayUshort16(imgGU);
            //Image<Bgr, byte> imgbgr = SetImgDirection16(imgGU);
            if (isHSwitch) imgGU._Flip(Emgu.CV.CvEnum.FlipType.Horizontal);
            if (isVSwitch) imgGU._Flip(Emgu.CV.CvEnum.FlipType.Vertical);
            if (rotate != 0) imgGU = imgGU.Rotate(rotate, new Gray(0), false);



            //Image<Gray, ushort> img = new Image<Gray, ushort>(imgbgr.Bitmap);

            //////图像宽小于高，则旋转
            ////Image<Gray, ushort> imgGU = new Image<Gray, ushort>((Bitmap)dcmImage.RenderImage());
            ////imgGU = imgProcess.ProcessImgGrayUshort16(imgGU);
            ////Image<Bgr, byte> imgbgr = SetImgDirection16(imgGU);
            ////Image<Gray, ushort> img = new Image<Gray, ushort>(imgbgr.Bitmap);        
            return imgGU;
        }
        private void Saveq()//保存缺陷至数据库
        {
            if (NumofDefs==0)
            {
                MessageBox.Show("请标定缺陷位置");
                return;
            }

            DefectInform inform = new DefectInform();
            inform.Title = "缺陷信息";
            inform.ShowDialog();

            if (!issave) return;

            if (isQueadd)
            {
                ModelInfo modelInfo = new ModelInfo();
                modelInfo.PicName = Picname;
                int id;
                for (id = 0; id < models.Count; id++)
                {
                    if (modelInfo.PicName.CompareTo(models[id].PicName) < 0)
                    {
                        break;
                    }
                }
                modelInfo.ID = id + 1;
                for (int i = id; i < models.Count; i++)
                {
                    models[i].ID = i + 2;
                }
                modelInfo.Ping = Ping;

                //double x, y;
                //x = double.Parse(BeginPosition.Substring(1, BeginPosition.IndexOf(",") - 1));
                //y = double.Parse(BeginPosition.Substring(BeginPosition.IndexOf(",") + 1, BeginPosition.Length - BeginPosition.IndexOf(",") - 2));
                //modelInfo.Position_b = new Vector(x, y);
                //x = double.Parse(EndPosition.Substring(1, EndPosition.IndexOf(",") - 1));
                //y = double.Parse(EndPosition.Substring(EndPosition.IndexOf(",") + 1, EndPosition.Length - EndPosition.IndexOf(",") - 2));
                //modelInfo.Position_e = new Vector(x, y);
                modelInfo.Qsize = QueSize;
                modelInfo.Qthick = Thick;
                modelInfo.TypeName = Qtype;
                //增加三个变量保存信息
                modelInfo.Qtype = LineorRec;//int类型，1直线，0矩形；
                //x = double.Parse(Qposb.Substring(1, Qposb.IndexOf(",") - 1));
                //y = double.Parse(Qposb.Substring(Qposb.IndexOf(",") + 1, Qposb.Length - Qposb.IndexOf(",") - 2));
                //modelInfo.Qpos_b = new Vector(x, y);
                //x = double.Parse(Qpose.Substring(1, Qpose.IndexOf(",") - 1));
                //y = double.Parse(Qpose.Substring(Qpose.IndexOf(",") + 1, Qpose.Length - Qpose.IndexOf(",") - 2));
                //modelInfo.Qpos_e = new Vector(x, y);
                modelInfo.InputA = InputA;
                modelInfo.InputB = InputB;
                coords = null;
                for(int i=0;i<defcoords.Length;i++)
                {
                    if (defcoords[i] == -1) break;
                    coords = coords + defcoords[i].ToString()+',';
                }                
                modelInfo.Coordinates = coords;
                if (id < models.Count) models.Insert(id, modelInfo);
                else models.Add(modelInfo);
                dataGridModel.ItemsSource = null;
                dataGridModel.ItemsSource = models;
            }
            else
            {
                models[dgidx].PicName = Picname;
                models[dgidx].Ping = Ping;

                //double x, y;
                //x = double.Parse(BeginPosition.Substring(1, BeginPosition.IndexOf(",") - 1));
                //y = double.Parse(BeginPosition.Substring(BeginPosition.IndexOf(",") + 1, BeginPosition.Length - BeginPosition.IndexOf(",") - 2));
                //models[dgidx].Position_b = new Vector(x, y);
                //x = double.Parse(EndPosition.Substring(1, EndPosition.IndexOf(",") - 1));
                //y = double.Parse(EndPosition.Substring(EndPosition.IndexOf(",") + 1, EndPosition.Length - EndPosition.IndexOf(",") - 2));
                //models[dgidx].Position_e = new Vector(x, y);
                models[dgidx].Qsize = QueSize;
                models[dgidx].Qthick = Thick;
                models[dgidx].TypeName = Qtype;
                //增加三个变量保存信息
                models[dgidx].Qtype = LineorRec;//int类型，1直线，0矩形；
                //x = double.Parse(Qposb.Substring(1, Qposb.IndexOf(",") - 1));
                //y = double.Parse(Qposb.Substring(Qposb.IndexOf(",") + 1, Qposb.Length - Qposb.IndexOf(",") - 2));
                //models[dgidx].Qpos_b = new Vector(x, y);
                //x = double.Parse(Qpose.Substring(1, Qpose.IndexOf(",") - 1));
                //y = double.Parse(Qpose.Substring(Qpose.IndexOf(",") + 1, Qpose.Length - Qpose.IndexOf(",") - 2));
                //models[dgidx].Qpos_e = new Vector(x, y);
                models[dgidx].InputA = InputA;
                models[dgidx].InputB = InputB;
                coords = null;
                for (int i = 0; i < defcoords.Length; i++)
                {
                    if (defcoords[i] == -1) break;
                    coords = coords + defcoords[i].ToString() + ',';
                }
                models[dgidx].Coordinates = coords;
                dataGridModel.ItemsSource = null;
                dataGridModel.ItemsSource = models;
            }            
            modelchange = true;
            clearall();            
        }

        private void Save_Click(object sender, RoutedEventArgs e)//修改
        {
            if (!MarkSwitch)
            {
                MessageBox.Show("未处于标记缺陷状态，功能禁用");
                return;
            }
            Qtype = null;
            QueSize = null;
            Thick = null;
            Ping = null;
            InputA = null;
            InputB = null;
            Saveq();
        }

        private void dataGridModel_SelectionChanged(object sender, SelectionChangedEventArgs e)//修改
        {
            //// MessageBox.Show("1");
            DataGrid dg = sender as DataGrid;
            dgidx = dg.SelectedIndex;
        }


        private void btn_Delete_Click(object sender, RoutedEventArgs e)//修改
        {        
            if (dgidx >= models.Count | dgidx < 0) return;
            models.RemoveRange(dgidx, 1);
            for (int i = dgidx; i < models.Count; i++)
            {
                models[i].ID = i + 1;
            }
            dataGridModel.ItemsSource = null;
            dataGridModel.ItemsSource = models;
            modelchange = true;           
            meangrayTB.Text = meangray;
            RefreshImgDisplay(ref dcmImage);           
        }

        private void btn_Update_Click(object sender, RoutedEventArgs e)//修改
        {
            if (dgidx >= models.Count | dgidx < 0) return;
            ModelInfo modelInfo = models[dgidx];
            Qtype = modelInfo.TypeName;
            //BeginPosition = "(" + modelInfo.Position_b.X.ToString() + "," + modelInfo.Position_b.Y.ToString() + ")";
            //EndPosition = "(" + modelInfo.Position_e.X.ToString() + "," + modelInfo.Position_e.Y.ToString() + ")";
            QueSize = modelInfo.Qsize;
            Thick = modelInfo.Qthick;
            Ping = modelInfo.Ping;
            Picname = modelInfo.PicName;
            LineorRec = modelInfo.Qtype;//int类型，1直线，0矩形；
            //Qposb = "(" + modelInfo.Qpos_b.X.ToString() + "," + modelInfo.Qpos_b.Y.ToString() + ")";
            //Qpose = "(" + modelInfo.Qpos_e.X.ToString() + "," + modelInfo.Qpos_e.Y.ToString() + ")";
            InputA = modelInfo.InputA;
            InputB = modelInfo.InputB;
            isQueadd = false;
            Saveq();
        }       

        private void dataGridModel_MouseDoubleClick(object sender, MouseButtonEventArgs e)//修改
        {
            IsSp = false;
            pinjie = false;
            if (pintu != null)
            {
                pintu.Dispose();
                pintu = null;
            }
            ResetAllSwitchtoFalseEXCEPT(ref winchange);
            IsAddDefect(false);                                                                                       
            DefectShow = true;
            ClearRuler();ClearArea();
            if (dgidx >= models.Count | dgidx < 0) return;
            //tlt.X = 0;
            //tlt.Y = 0;
            string SelectPic;
            pictureName = models[dgidx].PicName;
            if (!DefectView)
            {
                SelectPic = filepath.Trim() + "\\" + pictureName;
            }
            else
            {
                SelectPic = filepath.Trim().Substring(0, filepath.Trim().LastIndexOf("评定图像")) + "\\" + models[dgidx].PicName;
            }
            int idx = picName.IndexOf(pictureName);
            //n0 = idx;
            if (s != null) s.Background = basecolor;
            s = listbox.ItemContainerGenerator.ContainerFromIndex(idx) as ListBoxItem;
            s.Background = selectcolor;
            listbox.ScrollIntoView(listbox.Items[idx]);
            listbox.SelectedIndex = -1;
            dcmFile = DicomFile.Open(SelectPic);
            dcmDataSet = dcmFile.Dataset;
            dcmImage = new DicomImage(dcmDataSet);
            iPixelData = dcmImage._pixelData;            
            dcmImage.WindowWidth = winwidth;
            dcmImage.WindowCenter = wincenter;            
            LoadCoords(models[dgidx].Coordinates);
            classflag = models[dgidx].Qtype;
            InitImgBoxSize(ref dcmImage);
            RefreshImgDisplay(ref dcmImage);
            clearlines();
            CancelKey.Clear();
        }

        private void btn_huanyuan_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null || pinjie) 
            {
                clearall();
                imgbgrPpj = null;
                IsAddDefect(false);
                ResetAllSwitchtoFalseEXCEPT(ref winchange);
                imgProcess.ResetImgProcessArgsTotally();
                RefreshImgDisplayPintu(ref imgPinJieResult16);
            }
            else
            {
                clearall();
                imgbgrP = null;
                IsAddDefect(false);
                ResetAllSwitchtoFalseEXCEPT(ref winchange);
                imgProcess.ResetImgProcessArgsTotally();
                RefreshImgDisplay(ref dcmImage);
            }
            
            
        }


        #region 图像信息显示
        private void btn_xinxi_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            information t = new information();
            t.Title = "图像信息";
            t.ShowDialog();

        }
        #endregion
        /// <summary>
        /// 找焊缝或管道的连通域
        /// </summary>
        /// <param name="rImg"></param> //输入16位图像
        /// <param name="contours"></param> //找到的所有连通域轮廓
        /// <param name="listid"></param>  //有效连通域的ID
        /// <param name="listrect"></param>  //有效连通域的外接矩形
        private void findhangfeng(Image<Gray, ushort> rImg, out VectorOfVectorOfPoint contours, out List<int> listid, out List<System.Drawing.Rectangle> listrect)
        {
            contours = new VectorOfVectorOfPoint();
            listid = new List<int>();
            listrect = new List<System.Drawing.Rectangle>();
            var bimg = new Image<Gray, ushort>(dcmImage.Width, dcmImage.Height);
            /////假设找焊缝或管道是水平的，进行垂直滤波，找出灰度比上下低262的点
            CvInvoke.Blur(rImg, bimg, new System.Drawing.Size(1, 128), new System.Drawing.Point(-1, -1), BorderType.Replicate);//
            var imgb1 = new Image<Gray, byte>(dcmImage.Width, dcmImage.Height);
            bimg = bimg.Sub(new Gray(262));
            //double minva=0, maxva=0;
            //System.Drawing.Point minlo = new System.Drawing.Point(0, 0), maxlo = new System.Drawing.Point(0, 0);
            //CvInvoke.MinMaxLoc(bimg, ref minva, ref maxva, ref minlo, ref maxlo);
            //double ap = 65535 / (maxva - minva);
            //bimg = bimg.Sub(new Gray(minva));
            //bimg = bimg.Mul(ap);
            //CvInvoke.MinMaxLoc(bimg, ref minva, ref maxva, ref minlo, ref maxlo);
            CvInvoke.Compare(rImg, bimg, imgb1, CmpType.LessThan);
            /////开闭运算
            Mat element;
            element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(10, 10), new System.Drawing.Point(-1, -1));
            CvInvoke.MorphologyEx(imgb1, imgb1, MorphOp.Close, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
            element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(15, 15), new System.Drawing.Point(-1, -1));
            CvInvoke.MorphologyEx(imgb1, imgb1, MorphOp.Open, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));

            //string path1 = filepath.Trim() + "\\buimg.tif";
            //bimg.Save(path1);
            /////找连通域轮廓
            VectorOfRect hierarchy = new VectorOfRect();            
            CvInvoke.FindContours(imgb1, contours, hierarchy, RetrType.Ccomp, ChainApproxMethod.ChainApproxNone);


            //string path2 = filepath.Trim() + "\\rezhi.jpg";
            //imgb1.Save(path2);

            int idx = 0;
            for (; idx >= 0; idx = hierarchy[idx].X)/////遍历外轮廓
            {
                System.Drawing.Rectangle ri = CvInvoke.BoundingRectangle(contours[idx]);
                if(ri.Left<10 && ri.Top>10 && ri.Right>rImg.Cols-10 && ri.Bottom<rImg.Rows-10)/////记录有效连通域，条件横穿图像
                {
                    int j = 0;
                    for (; j < listid.Count; j++) /////对有效连通域排序，位置上排前面
                    {
                        if (ri.Top < listrect[j].Top) break;
                    }
                    listid.Insert(j, idx);
                    listrect.Insert(j, ri);
                }                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void verticallengthmeasure_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            if (IsVlm == false)
            {
                IsVlm = !IsVlm;
                circenter.Clear();
                cirrad.Clear();
                imgQualityEvaluation.FindPorosityDefect(ref dcmImage, ref circenter, ref cirrad);
                //////显示气孔                        
                for (int i = 0; i < circenter.Count; i++)
                {
                    DefectInfo info = new DefectInfo(3, circenter[i].X, circenter[i].Y, (int)cirrad[i], 0, null);
                    defectInfo.Add(info);
                    //NumofDefs++;
                }
                drawNeeded = true;
                RefreshImgDisplay(ref dcmImage);
            }
            else
            {
                IsVlm = !IsVlm;
                circenter.Clear();
                cirrad.Clear();
                //imgQualityEvaluation.FindPorosityDefect(ref dcmImage, ref circenter, ref cirrad);
                ////////显示气孔                        
                //for (int i = 0; i < circenter.Count; i++)
                //{
                //    DefectInfo info = new DefectInfo(3, circenter[i].X, circenter[i].Y, (int)cirrad[i], 0, null);
                //    defectInfo.Add(info);

                //}
                //drawNeeded = true;
                RefreshImgDisplay(ref dcmImage);
            }
            
        }

        private void btn_liangdu_Click(object sender, RoutedEventArgs e)
        {
            //Thread thread = new Thread(showContent);
            //thread.Start();
        }
        private void showContent()
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));
                this.label1.Content = "明天周五了！";
            });
        }

        private void markinfo_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            if(DefectView&&!DefectShow)
            {
                MessageBox.Show("当前仅限浏览，如需修改请打开数据库中单个缺陷");
            }
            //if (DefectShow)
            //{
            //    clearall();
            //}       
            IsAddDefect(!IsAd);           
           
            if (MarkSwitch)
            {
                ClearRuler();
                ClearArea();
                cleardefectline();
                ResetAllSwitchtoFalseEXCEPT();                
                CancelType = "def";
            }
            else
            {
                ClearRuler();
                ClearArea();
                cleardefectline();
                if (!pinjie)
                {
                    ResetAllSwitchtoFalseEXCEPT(ref winchange);
                }
                else
                {
                    ResetAllSwitchtoFalseEXCEPT();
                }
                rec1 = false;rec2 = false;rec3 = false;
            }           
        }
      
        

        private void initialsize_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            double prw, prh, nw, nh, ptr;
            Scroll.ScrollToHome();
            if (imgbox.Width > Scroll.Width) prw = (imgbox.Width - Scroll.Width) / 2;
            else prw = 0;
            if (imgbox.Height > Scroll.Height) prh = (imgbox.Height - Scroll.Height) / 2;
            else prh = 0;
            if (pinjie == false)
            {
                imgbox.Width = BaseBps.PixelWidth;
                imgbox.Height = BaseBps.PixelHeight;
                ptr = trans;
                trans = imgbox.Height / BaseWinHeight;
            }
            else
            {
                imgbox.Height = BaseBps.PixelHeight;
                int x = picName.Count;
                //if (x > 5) x = 5;
                imgbox.Width = BaseBps.PixelWidth * x;
                ptr = trans;
                trans = imgbox.Height / BaseWinHeight;
                //if (BaseWinWidth > BaseWinHeight)
                //{
                //    imgbox.Height = BaseBps.PixelHeight;
                //    imgbox.Width = BaseBps.PixelWidth * picName.Count;
                //    ptr = trans;
                //    trans = imgbox.Height / BaseWinHeight;
                //}
                //else
                //{
                //    imgbox.Height = BaseBps.PixelHeight;
                //    imgbox.Width = BaseBps.PixelWidth * picName.Count;
                //    ptr = trans;
                //    trans = imgbox.Height / BaseWinWidth;
                //}
            }
            if (imgbox.Width > Scroll.Width) nw = (imgbox.Width - Scroll.Width) / 2;
            else nw = 0;
            if (imgbox.Height > Scroll.Height) nh = (imgbox.Height - Scroll.Height) / 2;
            else nh = 0;
            double MARrate = trans / ptr;
            tlt.X = (prw + tlt.X) * MARrate - nw;
            tlt.Y = (prh + tlt.Y) * MARrate - nh;

        }

        private void adaptivesize_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            Scroll.ScrollToHome();
            tlt.X = 0;
            tlt.Y = 0;
            if (pinjie == false)
            {
                AdjustImageBoxSize(BaseBps);
                trans = imgbox.Height / BaseWinHeight;
            }
            else
            {
                //if (BaseWinWidth > BaseWinHeight)
                //{
                //    imgbox.Height = 512;
                //    imgbox.Width = 512 * (BaseBps.PixelWidth / BaseBps.PixelHeight) * picName.Count;
                //    trans = imgbox.Height / BaseWinHeight;
                //}
                //else
                //{
                //    imgbox.Height = 640;
                //    imgbox.Width = 640 * (BaseBps.PixelHeight / BaseBps.PixelWidth) * picName.Count;
                //    trans = imgbox.Height / BaseWinHeight;
                //    //trans = imgbox.Height / BaseWinWidth;
                //}
                imgbox.Height = BaseWinHeight;
                int x = picName.Count;
                //if (x > 5) x = 5;
                imgbox.Width = BaseWinWidth * x;
                trans = imgbox.Height / BaseWinHeight;
            }
        }

        private void sizeup_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            double prw, prh, nw, nh;
            Scroll.ScrollToHome();
            if (imgbox.Width > Scroll.Width) prw = (imgbox.Width - Scroll.Width) / 2;
            else prw = 0;
            if (imgbox.Height > Scroll.Height) prh = (imgbox.Height - Scroll.Height) / 2;
            else prh = 0;
            double MARrate = 1.2, MaxRate = 10, MinRate = 1;
            if (trans >= MinRate * (1 / MARrate) && trans <= MaxRate)
            {

                imgbox.Width *= MARrate;
                imgbox.Height *= MARrate;
            }
            else
            {
                return;
            }
            //if (pinjie == false) { trans = imgbox.Height / BaseWinHeight; }
            //else
            //{
            //    if (BaseWinWidth > BaseWinHeight) { trans = imgbox.Height / BaseWinHeight; }
            //    else { trans = imgbox.Height / BaseWinWidth; }
            //}
            trans = imgbox.Height / BaseWinHeight;
            if (imgbox.Width > Scroll.Width) nw = (imgbox.Width - Scroll.Width) / 2;
            else nw = 0;
            if (imgbox.Height > Scroll.Height) nh = (imgbox.Height - Scroll.Height) / 2;
            else nh = 0;
            tlt.X = (prw + tlt.X) * MARrate - nw;
            tlt.Y = (prh + tlt.Y) * MARrate - nh;

        }
        private void sizedown_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            double prw, prh, nw, nh;
            Scroll.ScrollToHome();
            if (imgbox.Width > Scroll.Width) prw = (imgbox.Width - Scroll.Width) / 2;
            else prw = 0;
            if (imgbox.Height > Scroll.Height) prh = (imgbox.Height - Scroll.Height) / 2;
            else prh = 0;
            double MARrate = 1.2, MaxRate = 10, MinRate = 1;
            if (trans >= MinRate && trans <= MaxRate * MARrate)
            {
                imgbox.Width *= 1 / MARrate;
                imgbox.Height *= 1 / MARrate;
            }
            else
            {
                return;
            }
            //if (pinjie == false) { trans = imgbox.Height / BaseWinHeight; }
            //else
            //{
            //    if (BaseWinWidth > BaseWinHeight) { trans = imgbox.Height / BaseWinHeight; }
            //    else { trans = imgbox.Height / BaseWinWidth; }
            //}
            trans = imgbox.Height / BaseWinHeight;
            if (imgbox.Width > Scroll.Width) nw = (imgbox.Width - Scroll.Width) / 2;
            else nw = 0;
            if (imgbox.Height > Scroll.Height) nh = (imgbox.Height - Scroll.Height) / 2;
            else nh = 0;
            tlt.X = (prw + tlt.X) / MARrate - nw;
            tlt.Y = (prh + tlt.Y) / MARrate - nh;
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;
            if (CancelKey.Count != 0)
            {
                string CancelType = CancelKey.Last();
                CancelKey.RemoveAt(CancelKey.Count - 1);
                if (CancelType == "ss")
                {
                    RefreshImgDisplay(ref dcmImage);
                    CancelType = null;
                }
                else if (CancelType == "def" && defcoords[0] != -1 && (!DefectView || DefectShow))
                {
                    for (int i = 0; i < defcoords.Length - 1; i++)
                    {
                        if (defcoords[8 * i] == -1)
                        {
                            for (int j = 1; j <= 8; j++)
                            {
                                defcoords[8 * i - j] = -1;
                            }
                            NumofDefs = NumofDefs - 1;
                            if (pinjie == false)
                            {
                                RefreshImgDisplay(ref dcmImage);
                            }
                            else
                            {
                                for (int j = 1; j <= 8; j++)
                                {
                                    pinjieshow[8 * i - j] = -1;
                                }
                                RefreshImgDisplayPintu(ref imgPinJieResult16);
                                //Image<Bgr, byte> grayd = DrawDefect(new Image<Bgr, byte>(pintu.Bitmap), 0);
                                //bitSourse = imgFormatTransform.ImageToBitmapSource(grayd);
                                //imgbox.Source = bitSourse;
                            }
                            break;
                        }
                    }
                }
                else if (CancelType == "text" && !(SaveTexts.First().Equals(null)) && !pinjie)
                {
                    for (int i = 0; i < 3; i++) { SaveTexts.RemoveAt(SaveTexts.Count - 1); }
                    RefreshImgDisplay(ref dcmImage);
                    if (SaveTexts.Count == 0) CancelType = null;
                }
                else
                {
                    bool isSave = false;
                    switch (CancelType)
                    {
                        case "contrastUp":
                            imgProcess.AdjustContrastRatio(-1);
                            break;
                        case "contrastDown":
                            imgProcess.AdjustContrastRatio(+1);
                            break;
                        case "brightUp":
                            imgProcess.AdjustBrightRatio(-1);
                            break;
                        case "brightDown":
                            imgProcess.AdjustBrightRatio(+1);
                            break;
                        case "sharpen":
                            imgProcess.AdjustSharpnessRatio(-1);
                            break;
                        case "histEqualize":
                            imgProcess.SwitchOnHistEqualize(false);
                            break;                        
                    }
                    switch (CancelType)
                    {
                        case "inverse":
                            IsNg = !IsNg;
                            isInverse = !isInverse;
                            imgProcess.SwitchOnInverse(isInverse);
                            break;
                        case "fudiao":
                            IsSc = !IsSc;
                            isfudiao = !isfudiao;
                            imgProcess.SwitchOnFudiao(isfudiao);
                            break;
                        case "rotateLeft":
                            rotate += 90;
                            if (rotate >= 360) rotate = 0;
                            s5 = rotate.ToString();
                            isSave = true;
                            break;
                        case "rotateRight":
                            rotate -= 90;
                            if (rotate <= -90) rotate = 270;
                            s5 = rotate.ToString();
                            isSave = true;
                            break;
                        case "HSwitch":
                            if (rotate % 180 == 0)
                            {
                                isHSwitch = !isHSwitch;
                            }
                            else
                            {
                                isVSwitch = !isVSwitch;
                            }
                            if (isHSwitch) { s2 = "1"; }
                            else { s2 = "0"; }
                            if (isVSwitch) { s3 = "1"; }
                            else { s3 = "0"; }
                            isSave = true;
                            break;
                        case "VSwitch":
                            if (rotate % 180 == 0)
                            {
                                isVSwitch = !isVSwitch;
                            }
                            else
                            {
                                isHSwitch = !isHSwitch;
                            }
                            if (isHSwitch) { s2 = "1"; }
                            else { s2 = "0"; }
                            if (isVSwitch) { s3 = "1"; }
                            else { s3 = "0"; }
                            isSave = true;
                            break;
                    }
                    if(isSave)SaveTxt();
                    RefreshImgDisplayPintu(ref imgPinJieResult16);
                }                
            }
        }

        private void AddWords_Click(object sender, RoutedEventArgs e)
        {   if (dcmImage == null) return;
            if(!IsAw)
            {               
                ResetAllSwitchtoFalseEXCEPT(ref showLine);
                IsAw = true;       
            }
            else
            {
                IsAw = false;
                SetDispalyState(ref showArea, ref winchange);
            }            
            SaveWords = null;
        }
        private void Auto_Click(object sender, RoutedEventArgs e)
        {
            IsAt = !IsAt;
        }
        private void hbiao_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;

            Ishbiao = !Ishbiao; //取反
            if (!Ishbiao)//Ishbiao原为true时,现为false
            {
                clearHRulerLine_Axis();//移除实例HRulerLine_Axis、并注销名字
                clearHRulerLine_Ticks();//移除所有刻度元素HRulerLine_Ticks[ti]、并注销名字
                clearHRulerLines();//无条件对水平标尺上的所有线段逆实例化（外加HRulerLine_Ticks数组）
                clearHTickNum();//移除所有的文本框元素HTickNum[i]、并注销名
                clearHTextBlock();//无条件对水平标尺上的所有数值文本框逆实例化
            }

            if (Isvbiao != false)
            {
                Isvbiao = false;
                clearVRulerLine_Axis();//移除实例VRulerLine_Axis、并注销名字
                clearVRulerLine_Ticks();//移除所有刻度元素VRulerLine_Ticks[ti]、并注销名字
                clearVRulerLines();//无条件对纵向标尺上的所有线段逆实例化（外加VRulerLine_Ticks数组）
                clearVTickNum();//移除所有的文本框元素VTickNum[i]、并注销名字
                clearVTextBlock();//无条件对纵向标尺上的所有数值文本框逆实例化
            }
        }
        private void vbiao_Click(object sender, RoutedEventArgs e)
        {
            if (dcmImage == null) return;

            Isvbiao = !Isvbiao;//取反
            if (!Isvbiao)//Isvbiao原为true时,现为false
            {
                clearVRulerLine_Axis();//移除实例VRulerLine_Axis、并注销名字
                clearVRulerLine_Ticks();//移除所有刻度元素VRulerLine_Ticks[ti]、并注销名字
                clearVRulerLines();//无条件对纵向标尺上的所有线段逆实例化（外加VRulerLine_Ticks数组）
                clearVTickNum();//移除所有的文本框元素VTickNum[i]、并注销名
                clearVTextBlock();//无条件对纵向标尺上的所有数值文本框逆实例化
            }

            if (Ishbiao != false)
            {
                Ishbiao = false;
                clearHRulerLine_Axis();//移除实例HRulerLine_Axis、并注销名字
                clearHRulerLine_Ticks();//移除所有刻度元素HRulerLine_Ticks[ti]、并注销名字
                clearHRulerLines();//无条件对水平标尺上的所有线段逆实例化（外加HRulerLine_Ticks数组）
                clearHTickNum();//移除所有的文本框元素HTickNum[i]、并注销名
                clearHTextBlock();//无条件对水平标尺上的所有数值文本框逆实例化
            }
        }


        private void thickMeasure_Click(object sender, RoutedEventArgs e)
        {
            if (pinjie == true)
            {
                MessageBox.Show("拼接后不允许修改！");
                return;
            }
            //clearall();
            IsTm = !IsTm;
            if (IsTm)
            {
                IsAddDefect(false);
                ResetAllSwitchtoFalseEXCEPT(ref showLine);
                IsTm = true;                
                clearlines();
                cleardefectline();
                currentline = null; Rectline1 = null; Rectline2 = null; Rectline3 = null; Rectline4 = null; vline = null;
                CurrentText = null;
            }
            else
            {
                SetDispalyState(ref showArea,ref winchange);
                if (CancelType == "ss")
                {
                    RefreshImgDisplay(ref dcmImage);
                    CancelType = null;
                }
            }
        }
        private void ResetAllSwitchtoFalseEXCEPT(ref bool s1)//与缺陷登记有关的变量，如添加缺陷IsAd的转换根据需求添加，此处不进行统一转换
        {
            showArea = false;showLine = false;winchange = false;IsAr = false;IsTm = false;
            //显示窗口画框   显示窗口画线     调窗              面积测量     测厚
            IsHlm = false;IsTw = false; IsSnr = false;IsHla = false;IsAw = false; Ishbiao = false; Isvbiao = false;
            //长度测量    双丝检测      峰值信噪比    校准          添加文字      水平尺子         纵向标尺
            s1 = true;
        }
        private void ResetAllSwitchtoFalseEXCEPT()
        {
            showArea = false; showLine = false; winchange = false; IsAr = false; IsTm = false;
            IsHlm = false; IsTw = false; IsSnr = false; IsHla = false; IsAw = false; Ishbiao = false;   Isvbiao = false;       
        }
        private void SetDispalyState()
        {
            showArea = false; showLine = false; winchange = false;
        }
        private void SetDispalyState(ref bool s1)
        {
            showArea = false;showLine = false; winchange = false;s1 = true;
        }
        private void SetDispalyState(ref bool s1, ref bool s2)
        {
            showArea = false; showLine = false; winchange = false; s1 = true;s2 = true;
        }
        private void IsAddDefect(bool arg)
        {
            IsAd = arg; MarkSwitch = arg;
        }
        private void SaveLocalExcel()
          {
              string path;
              if (DefectView)
              {
                  path = filepath.Trim() + "\\缺陷报表.xlsx";
              }
              else
              {
                  path = filepath.Trim() + "\\评定图像\\缺陷报表.xlsx";
              }
              Microsoft.Office.Interop.Excel.Application excelApp;
              Microsoft.Office.Interop.Excel.Workbook excelWB;//创建工作簿（WorkBook：即Excel文件主体本身）
              Microsoft.Office.Interop.Excel.Workbooks excelWBs;
              Microsoft.Office.Interop.Excel.Worksheet excelWS;//创建工作表（即Excel里的子表sheet）

              Microsoft.Office.Interop.Excel.Sheets excelSts;

              excelApp = new Microsoft.Office.Interop.Excel.Application();
              excelApp.Visible = false;

              //设置禁止弹出保存和覆盖的询问提示框
              excelApp.DisplayAlerts = false;
              excelApp.AlertBeforeOverwriting = false;
              if (excelApp == null)
              {
                  throw new Exception("打开Excel应用时发生错误！");
              }
              excelWBs = excelApp.Workbooks;
              //打开一个现有的工作薄
              excelWB = excelWBs.Add(path);
              excelSts = excelWB.Sheets;
              //选择第一个Sheet页
              excelWS = excelSts.get_Item(1);
              Microsoft.Office.Interop.Excel.Range range = (Microsoft.Office.Interop.Excel.Range)excelWS.get_Range("A8", "I60");
              range.ClearContents();
              range.Font.Color = System.Drawing.Color.Black;
              for (int i = 8; i < 8 + models.Count(); i++)
              {
                  for (int j = 1; j <= 9; j++)
                  {
                      if (j == 1) excelWS.Cells[i, j] = (i - 7).ToString();
                      if (j == 5) excelWS.Cells[i, j] = models[i - 8].TypeName;
                      if (j == 7) excelWS.Cells[i, j] = models[i - 8].Qthick;
                      if (j == 8) excelWS.Cells[i, j] = models[i - 8].Ping;
                  }
              }
              try
              {
                  excelWB.Saved = true;
                  excelWB.SaveCopyAs(filepath.Substring(0, filepath.Length - 1) + "\\评定图像\\缺陷报表.xlsx");
              }
              catch (Exception ex)
              {
                  MessageBox.Show("导出文件失败!" + ex.Message);
              }
              excelWB.Close();
              excelWBs.Close();
              excelApp.Quit();              
              //excelWB.SaveAs(filepath.Trim() + "\\评定图像\\缺陷报表1.xlsx");
        }
        public string CreateGrayExcel(string filePath, int mode)
        {
            if (mode == 0)
            {
                excelPath = filePath.Substring(0, filePath.LastIndexOf('.')) + "-灰度数据.xlsx";
            }
            else
                excelPath = filePath.Substring(0, filePath.LastIndexOf('.')) + "-矩形灰度频次数据.xlsx";

            if (File.Exists(excelPath)) return excelPath;
            object Nothing = System.Reflection.Missing.Value;
            var app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = false;
            Microsoft.Office.Interop.Excel.Workbook workBook = app.Workbooks.Add(Nothing);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.Sheets[1];
            worksheet.Name = "Work";
            //headline
            //worksheet.Cells[1, 1] = "灰度值";
            worksheet.SaveAs(excelPath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing);
            workBook.Close(false, Type.Missing, Type.Missing);
            app.Quit();

            return excelPath;
        }
        public void SaveGrayExcel(string excelPath, double[] imgs, int n)
        {

            Microsoft.Office.Interop.Excel.Application excelApp;
            Microsoft.Office.Interop.Excel.Workbook excelWB;//创建工作簿（WorkBook：即Excel文件主体本身）
            Microsoft.Office.Interop.Excel.Workbooks excelWBs;
            Microsoft.Office.Interop.Excel.Worksheet excelWS;//创建工作表（即Excel里的子表sheet）

            Microsoft.Office.Interop.Excel.Sheets excelSts;

            excelApp = new Microsoft.Office.Interop.Excel.Application();
            excelApp.Visible = false;

            //设置禁止弹出保存和覆盖的询问提示框
            excelApp.DisplayAlerts = false;
            excelApp.AlertBeforeOverwriting = false;
            if (excelApp == null)
            {
                throw new Exception("打开Excel应用时发生错误！");
            }
            excelWBs = excelApp.Workbooks;
            //打开一个现有的工作薄
            excelWB = excelWBs.Add(excelPath);
            excelSts = excelWB.Sheets;
            //选择第一个Sheet页
            excelWS = excelSts.get_Item(1);
            int cols = excelWS.Columns.Count;
            int rows = excelWS.Rows.Count;
            int col = 1;
            //excelWS.Cells[1, 1] = "灰度值";
            while (excelWS.Cells[1, col].Value == "灰度值")
                col++;

            excelWS.Cells[1, col] = "灰度值";
            for (int i = 2; i < n; i++)
            {
                excelWS.Cells[i, col] = (int)imgs[i];

            }

            try
            {
                excelWB.Saved = true;
                excelWB.SaveCopyAs(excelPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("导出文件失败!" + ex.Message);
            }
            excelWB.Close();
            excelWBs.Close();
            excelApp.Quit();
            //excelWB.SaveAs(filepath.Trim() + "\\评定图像\\缺陷报表1.xlsx");
        }
        public void SaveGrayValueandCountsExcel( String excelPath, int[] grayCounts,  int valueCounts)
        {
            Microsoft.Office.Interop.Excel.Application excelApp;
            Microsoft.Office.Interop.Excel.Workbook excelWB;//创建工作簿（WorkBook：即Excel文件主体本身）
            Microsoft.Office.Interop.Excel.Workbooks excelWBs;
            Microsoft.Office.Interop.Excel.Worksheet excelWS;//创建工作表（即Excel里的子表sheet）

            Microsoft.Office.Interop.Excel.Sheets excelSts;

            excelApp = new Microsoft.Office.Interop.Excel.Application();
            excelApp.Visible = false;

            //设置禁止弹出保存和覆盖的询问提示框
            excelApp.DisplayAlerts = false;
            excelApp.AlertBeforeOverwriting = false;
            if (excelApp == null)
            {
                throw new Exception("打开Excel应用时发生错误！");
            }
            excelWBs = excelApp.Workbooks;
            //打开一个现有的工作薄
            excelWB = excelWBs.Add(excelPath);
            excelSts = excelWB.Sheets;
            //选择第一个Sheet页
            excelWS = excelSts.get_Item(1);
            int cols = excelWS.Columns.Count;
            int rows = excelWS.Rows.Count;
            int col = 1;
            //excelWS.Cells[1, 1] = "灰度值";
            while (excelWS.Cells[1, col].Value == "灰度值"|| excelWS.Cells[1, col].Value == "灰度频次")
                col++;

            excelWS.Cells[1, col] = "灰度值";
            excelWS.Cells[1, col+1] = "灰度频次";
            int index = 2;
            for (int i = 1; i < 65535; i++)
            {
                if (grayCounts[i] != 0)
                {
                    excelWS.Cells[index, col] = i;
                    excelWS.Cells[index, col+1] = grayCounts[i];
                    index++;
                }
                


            }

            try
            {
                excelWB.Saved = true;
                excelWB.SaveCopyAs(excelPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("导出文件失败!" + ex.Message);
            }
            excelWB.Close();
            excelWBs.Close();
            excelApp.Quit();
        }
    }
}