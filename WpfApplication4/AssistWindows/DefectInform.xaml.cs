﻿using ImageView;
using System.Windows;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// DefectInform.xaml 的交互逻辑
    /// </summary>
    public partial class DefectInform : Window
    {
        public DefectInform()
        {
            InitializeComponent();
            //this.Closing += Window_Closing;
            name.Text = MainWindow.Picname;            
            inputa.Text = MainWindow.InputA;
            inputb.Text = MainWindow.InputB;
            qtype.Text = MainWindow.Qtype;
            qsize.Text = MainWindow.QueSize;
            houdu.Text = MainWindow.Thick;
            pingji.Text = MainWindow.Ping;
            MainWindow.issave = false;
        }

        //private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    MainWindow.issave = false;
        //    e.Cancel = false; 
        //}

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //BeginPosition, EndPosition, QueSize, Thick, Ping, Picname, Qtype, Qposb, Qpose;
            //if (String.IsNullOrEmpty(qtype.Text.ToString()) == true || String.IsNullOrEmpty(qsize.Text.ToString()) == true || String.IsNullOrEmpty(houdu.Text.ToString()) == true || String.IsNullOrEmpty(pingji.Text.ToString()) == true
            //    || String.IsNullOrEmpty(inputa.Text.ToString())|| String.IsNullOrEmpty(inputb.Text.ToString()))
            //{
            //    MessageBox.Show("请补充缺陷信息");
            //    return;
            //}
            if(!inputa.Text.Equals(null)) MainWindow.InputA = inputa.Text;
            if (!inputb.Text.Equals(null)) MainWindow.InputB = inputb.Text;
            if (!qtype.Text.Equals(null)) MainWindow.Qtype = qtype.Text;
            if (!qsize.Text.Equals(null)) MainWindow.QueSize = qsize.Text;
            if (!houdu.Text.Equals(null)) MainWindow.Thick = houdu.Text;
            if (!(pingji.Text==null)) { MainWindow.Ping = pingji.Text; }
            //else { MainWindow.Ping = ""; }
            MainWindow.issave = true;
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {            
            MainWindow.issave = false;
            this.Close();
        }

    }
}
