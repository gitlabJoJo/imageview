﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using Microsoft.Win32;
using System.Threading;
using System.Diagnostics;
using Dicom;
using Dicom.Imaging;
using System.Drawing.Imaging;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using System.Runtime.InteropServices;
namespace dcmview.AssistWindows
{
    /// <summary>
    /// chart.xaml 的交互逻辑
    /// </summary>
    public partial class thicknessChart : Window
    {
        int shuangsi;
        public thicknessChart()
        {
            InitializeComponent();
        }
        public void InitChart(double[] datac, int left, int right)
        {
            //定义图表区域
            chartAttMed.ChartAreas.Clear();
            ChartArea chartArea1 = new ChartArea("C1");
            chartAttMed.ChartAreas.Add(chartArea1);

            //定义存储和显示点的容器
            chartAttMed.Series.Clear();
            Series series1 = new Series("pixelvalue");
            series1.ChartArea = "C1";
            series1.YValueType = ChartValueType.Int32;
            series1.XValueType = ChartValueType.Int32;


            double[] cut = new double[right - left + 1];
            for (int j = left; j <= right; j++)
            {
                cut[j - left] = datac[j];
            }
            //设置图表显示样式
            series1.ChartType = SeriesChartType.Line;            
            chartAttMed.ChartAreas[0].AxisX.Minimum = left;
            chartAttMed.ChartAreas[0].AxisX.Maximum = right;
            chartAttMed.ChartAreas[0].AxisX.Title = "像素数";
            chartAttMed.ChartAreas[0].AxisY.Minimum = cut.Min();
            chartAttMed.ChartAreas[0].AxisY.Maximum = cut.Max();
            chartAttMed.ChartAreas[0].AxisY.Title = "像素值";

            //chartAttMed.Series[0].ChartType = SeriesChartType.Line;
            //chartAttMed.ChartAreas[0].AxisY.Interval = 1;
            chartAttMed.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartAttMed.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            //设置标题




            series1.Color = System.Drawing.Color.Red;
            for (int i = 0; i < cut.Length - 1; i++)
            {
                series1.Points.AddXY((double)(i + left), cut[i]);
            }
            chartAttMed.Series.Add(series1);                       
      
        }       
        public void showThickness(BitmapSource a,double thickness1,double thickness2)
        {
            shuangsicut.Source = a;
            length.Text = a.PixelWidth.ToString();
            width.Text = a.PixelHeight.ToString();
            thickness1Text.Text = Math.Round(thickness1, 2).ToString() + "mm";
            thickness2Text.Text = Math.Round(thickness2, 2).ToString() + "mm";
        }

        private void confirm_Click(object sender, RoutedEventArgs e)
        {
           

            MessageBox.Show("保存成功！");
            this.Close();
           
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
