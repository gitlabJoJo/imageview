﻿using System.Linq;
using System.Windows;
using System.Windows.Forms.DataVisualization.Charting;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// histgram.xaml 的交互逻辑
    /// </summary>
    public partial class histgram : Window
    {
        public histgram()
        {
            InitializeComponent();
        }
        
        public void Inithistogram(Dicom.Imaging.Render.IPixelData iPixelData)
        {

            int[] map1 = new int[65536];

            for (int x = 4; x <= iPixelData.Width - 5; x++)
            {
                for (int y = 4; y <= iPixelData.Height - 5; y++)
                {
                    int a = (int)iPixelData.GetPixel(x, y);
                    map1[a] = map1[a] + 1;                    
                }
            }
            
            //map1[0] = 0;


            //定义图表区域
            chartAttMed.ChartAreas.Clear();
            ChartArea chartArea1 = new ChartArea("C1");
            chartAttMed.ChartAreas.Add(chartArea1);
            chartAttMed.Titles.Add("图像直方图");

            //定义存储和显示点的容器
            chartAttMed.Series.Clear();
            Series series1 = new Series("AllValue");
            series1.ChartArea = "C1";
            series1.YValueType = ChartValueType.Int32;
            series1.XValueType = ChartValueType.Int32;

            //设置图表显示样式
            series1.ChartType = SeriesChartType.Column;
            chartAttMed.ChartAreas[0].AxisX.Minimum = 0;
            chartAttMed.ChartAreas[0].AxisX.Maximum = 65535;
            chartAttMed.ChartAreas[0].AxisX.Title = "像素值";
            chartAttMed.ChartAreas[0].AxisY.Minimum = map1.Min();
            chartAttMed.ChartAreas[0].AxisY.Maximum = map1.Max();
            chartAttMed.ChartAreas[0].AxisY.Title = "像素数";
            //chartAttMed.ChartAreas[0].AxisX.Interval = 100;

            chartAttMed.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartAttMed.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;

            //绘图
            series1.Color = System.Drawing.Color.Red;
            for (int i = 0; i <= map1.Length - 1; i++)
            {
                series1.Points.AddXY(i, map1[i]);
            }
            chartAttMed.Series.Add(series1);

            //MessageBox.Show("峰值："+map1.Max().ToString() + " "+"0值：" + map1[0].ToString() + " "+"3值：" +map1[3].ToString()+" "+"pixel(4,4):"+iPixelData.GetPixel(4,4));
        }
    }
}
