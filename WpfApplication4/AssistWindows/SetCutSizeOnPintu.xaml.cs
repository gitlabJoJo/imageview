﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ImageView;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// SetCutSizeOnPintu.xaml 的交互逻辑
    /// </summary>
    public partial class SetCutSizeOnPintu : Window
    {       
        public SetCutSizeOnPintu()
        {
            InitializeComponent();
            SizeText.Text = MainWindow.cutSize.ToString();            
        }        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try 
            {
                MainWindow.cutSize = int.Parse(SizeText.Text);
                MainWindow.pintuContinue = true;
                this.Close();
            }
            catch
            {
                MainWindow.pintuContinue = false;
                MessageBox.Show("请输入有效的数字");
            }
        }        
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
