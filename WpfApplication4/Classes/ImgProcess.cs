﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using ImageView;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dcmview.Classes
{
    class ImgProcess//数字图像处理相关方法，请将该类型方法集成到此类中
    {
        private int brightRatio, contrastRatio, sharpRatio;
        private bool isFudiao, isInverse, isHistEqualize;
        private bool isImageEnhance;

       
        //原来的亮度调整函数，将16位转化为8位，然后再进行处理，最后将8位结果转化为16位
        /*private void BrightAdjust(ref Image<Gray,ushort> img)
         {
             Image<Gray, byte> grayc=new Image<Gray, byte>(img.Bitmap);
             Bitmap myBitmap = grayc.Bitmap;            
             BitmapData srcdat = myBitmap.LockBits(new System.Drawing.Rectangle(System.Drawing.Point.Empty, myBitmap.Size), ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format8bppIndexed); // 锁定位图
             unsafe
             {
                 int value = brightRatio * 3;
                 byte* pix = (byte*)srcdat.Scan0; // 像素首地址
                ushort* pix1 = (ushort*)grayc.Mat.DataPointer;
                 for (int i = 0; i < srcdat.Stride * srcdat.Height; i++)
                 {
                     if (pix[i] + brightRatio < 255 && pix[i] + brightRatio>=0)
                     {
                         pix[i] = (byte)(pix[i] + brightRatio);
                     }
                     else
                     {
                         pix[i] = pix[i]-128>=0? (byte)255 : (byte)0;
                     }
                 }
                 myBitmap.UnlockBits(srcdat); // 解锁
             }            
             grayc = new Image<Gray, byte>(myBitmap);
             img = new Image<Gray, ushort>(grayc.Bitmap);
         }*/

        private void BrightAdjust(ref Image<Gray, byte> img)
        {

            img = img.Add(new Gray(brightRatio / 255));                       //亮度


        }


        private void ContrastAdjust(ref Image<Gray, byte> img)
        {
            Image<Gray, float> img1, img2, img3, img4;
            //Image<Gray,Int32> img1, img2, img3,img4;

            double rate = contrastRatio > 0 ? Math.Pow(1.1, contrastRatio) : Math.Pow(0.9, Math.Abs(contrastRatio));
            img1 = img.Convert<Gray, float>();
            img1 = img1.Sub(new Gray(128)).Mul(rate).Add(new Gray(128)).Max(0).Min(255);
            //img3 = img2.Mul(rate);
            //img4 = img3.Add(new Gray(128));
            //img4 = img4.Max(0);
            img = img1.Convert<Gray, byte>();




        }
        public void SharpnessAdjust(ref Image<Gray, ushort> img, int sharpRatio)
        {
            Image<Gray, float> img1;
            Image<Gray, float> img2;

            img1 = img.Convert<Gray, float>();
            img2 = img1.Laplace(3);
            //CvInvoke.Imshow("00",img2/255);
            img2 = img1.AddWeighted(img2, 1, -0.2 * sharpRatio, 0);
            img2 = img2.Max(0);
            img2 = img2.Min(65535);
            img = img2.Convert<Gray, UInt16>();


        }
        private void Fudiao(ref Image<Gray, byte> img)
        {
            Image<Gray, byte> grayc = new Image<Gray, byte>(img.Bitmap);
            Bitmap myBitmap = grayc.Bitmap;
            BitmapData srcdat = myBitmap.LockBits(new System.Drawing.Rectangle(System.Drawing.Point.Empty, myBitmap.Size), ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format8bppIndexed); // 锁定位图
            unsafe
            {
                byte* pix = (byte*)srcdat.Scan0; // 像素首地址
                for (int i = 0; i < (srcdat.Stride) * (srcdat.Height - 4) - 4; i++)
                {

                    int n = pix[i] + pix[i + srcdat.Stride + 1] - pix[i + srcdat.Stride * 2 + 2] - pix[i + srcdat.Stride * 3 + 3] + 128;
                    if (n < 0)
                    {
                        pix[i] = 0;
                    }
                    else if (n > 255)
                    {
                        pix[i] = 255;
                    }
                    else
                    {
                        pix[i] = (byte)n;
                    }
                }
                myBitmap.UnlockBits(srcdat); // 解锁
            }
            img = new Image<Gray, byte>(myBitmap);
            // CvInvoke.Imshow("111", img);
        }
        public void Inverse(ref Image<Gray, byte> img)
        {
            /*
            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {                    
                    img[j, i] =new Gray(255 - img[j, i].Intensity);
                }
            }  */
            Image<Gray, byte> imgout = new Image<Gray, byte>(img.Width, img.Height, new Gray(255));
            img = imgout - img;

        }
        private void HistEqualize(ref Image<Gray, ushort> img)
        {

            Size size = new Size(4, 4);
            CvInvoke.CLAHE(img, 10, size, img);



        }

        /// <summary>
        /// 第一种算法对比度稍差，适用于较小的图像（大卷积核）：基于Retinex
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public Image<Gray, byte> ImageEnhance1(ref Image<Gray, ushort> img)
        {
            Image<Gray, float> imgS, imgI, imgR, img2, imgI1, imgI2, imgI3, imgSharp,imgC;          //R=logS-logI
            Image<Gray, byte> imgout = new Image<Gray, byte>(img.Size);
            Image<Gray, byte> imgSharp1 = new Image<Gray, byte>(img.Size);
            imgI = new Image<Gray, float>(img.Size);
            imgI1 = new Image<Gray, float>(img.Size);
            imgI2 = new Image<Gray, float>(img.Size);
            imgI3 = new Image<Gray, float>(img.Size);

            //获得R图像

            imgS = img.Convert<Gray, float>();
            imgS = imgS.Add(new Gray(1));
            double kernelx = imgI.Width / 3;
            double kernely = imgI.Height / 3;
            if ((int)kernelx % 2 == 0) kernelx++;
            if ((int)kernely % 2 == 0) kernely++;
            Size sizeG = new Size((int)kernelx, (int)kernely);

            //多尺度
            CvInvoke.GaussianBlur(imgS, imgI1, sizeG, 5, 5, BorderType.Replicate);
            CvInvoke.GaussianBlur(imgS, imgI2, sizeG, 20, 20, BorderType.Replicate);
            CvInvoke.GaussianBlur(imgS, imgI3, sizeG, 100, 100, BorderType.Replicate);
            imgI = imgI1.Mul(0.6) + imgI2.Mul(0) + imgI3.Mul(0.4);
            imgR = imgS.Log() - imgI.Log();

            //进行预处理,去掉和均值相差较远的值
            //MCvScalar meanPre = new MCvScalar();  
            //MCvScalar varPre = new MCvScalar();
            //CvInvoke.MeanStdDev(imgR, ref meanPre, ref varPre);
            //double meanPre0 = meanPre.V0*1.5;
            //double varPre0 = varPre.V0*1.5;
            //imgR = imgR.Add(new Gray(meanPre0 + varPre0)).Max(0).Sub(new Gray(meanPre0 + varPre0));//imgR中有一些-8左右的值，对灰度值映射影响很大
            //imgR = imgR.Sub(new Gray(meanPre0 + varPre0)).Min(0).Add(new Gray(meanPre0 + varPre0));//imgR中有一些-8左右的值，对灰度值映射影响很大
            double aaa = 1.2;
            imgR = imgR.Add(new Gray(aaa)).Max(0).Sub(new Gray(aaa));//imgR中有一些-8左右的值，对灰度值映射影响很大
            imgR = imgR.Sub(new Gray(aaa)).Min(0).Add(new Gray(aaa));//imgR中有一些-8左右的值，对灰度值映射影响很大

            //进行映射
            MCvScalar mean = new MCvScalar();
            MCvScalar var = new MCvScalar();
            CvInvoke.MeanStdDev(imgR, ref mean, ref var);
            double mean0 = mean.V0;
            double var0 = var.V0;
            double scaleYS = 1.9;
            double l1 = mean0 - scaleYS * var0;
            double l2 = mean0 + scaleYS * var0;

            l2 -= l1;
            l1 *= 0.9;
            imgR = imgR.Sub(new Gray(l1));
            imgR = imgR.Max(l1);
            imgR = imgR.Min(l2);
            img2 = new Image<Gray, float>(img.Width, img.Height, new Gray(l2));
            CvInvoke.Divide(imgR, img2, imgR, 255, DepthType.Default);
            //imgR = imgR.Max(0).Min(255);
            ///调整对比度
            double contrastRatio = 3.5;
            double rate = Math.Pow(1.1, contrastRatio);
            imgout = imgR.Convert<Gray, byte>();
            imgC = imgout.Convert<Gray, float>();
            imgC = imgC.Sub(new Gray(128)).Mul(rate).Add(new Gray(128)).Max(0).Min(255);
            imgout = imgC.Convert<Gray, byte>();
            ///顶帽算法增强边缘
            Mat aa = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(2, 2));
            imgout = imgout + imgout.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Tophat, aa, new Point(1, 1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(255, 0, 0, 255));
            ///锐化
            imgC = imgout.Convert<Gray, float>();
            imgSharp = imgC.Laplace(3);
            imgR = imgC.AddWeighted(imgSharp, 1, -0.2 * 2, 0);
            imgR = imgR.Max(0).Min(255);
            imgout = imgR.Convert<Gray, byte>();
            return imgout;

        }
        
        
        /// <summary>
        /// 第二种方法需要修改
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public Image<Gray, byte> ImageEnhance2(ref Image<Gray, ushort> img)
        {          
            Image<Gray, float> imgS, imgI, imgR, img2, imgI1,imgI2,imgI3 ;          //R=logS-logI
            Image<Gray, byte> imgout = new Image<Gray, byte>(img.Size);
            Image<Gray, byte> imgout1 = new Image<Gray, byte>(img.Size);
            Image<Gray, float> imgS1 = new Image<Gray, float>(img.Width ,img.Height,new Gray(65535));
            imgI = new Image<Gray, float>(img.Size);
            imgI1 = new Image<Gray, float>(img.Size);
            imgI2 = new Image<Gray, float>(img.Size);
            imgI3 = new Image<Gray, float>(img.Size);

            //获得R图像
            imgS = img.Convert<Gray, float>().Add(new Gray(1));
            imgout = imgS.Convert<Gray, byte>();
            var size = new Size(271, 271);
            CvInvoke.GaussianBlur(imgout, imgout1, size, 6, 6);
            int r = 5;
            imgout += r * (imgout - imgout1); //高反差保留算法
            Mat aa = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(2, 2));
            imgout = imgout + imgout.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Tophat, aa, new Point(1, 1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(255, 0, 0, 255));

            return imgout;
        }
        
        
        /// <summary>
        /// 第三种方法卷积核较小，能够呈现图像的细节，但整体层次感较差：基于Retinex
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public Image<Gray, byte> ImageEnhance3(ref Image<Gray, ushort> img)
        {
            Image<Gray, float> imgS, imgI, imgR, img2, imgI1, imgI2, imgI3, imgSharp;          //R=logS-logI
            Image<Gray, byte> imgout = new Image<Gray, byte>(img.Size);
            imgI = new Image<Gray, float>(img.Size);
            imgI1 = new Image<Gray, float>(img.Size);
            imgI2 = new Image<Gray, float>(img.Size);
            imgI3 = new Image<Gray, float>(img.Size);

            //获得R图像
            imgS = img.Convert<Gray, float>();
            imgS = imgS.Add(new Gray(2));


            double kernelx = imgI.Width / 50;
            double kernely = imgI.Height / 50;
            if ((int)kernelx % 2 == 0) kernelx++;
            if ((int)kernely % 2 == 0) kernely++;
            Size sizeG = new Size((int)kernelx, (int)kernely);

            CvInvoke.GaussianBlur(imgS, imgI1, sizeG, 8, 8, BorderType.Replicate);   //多尺度
            CvInvoke.GaussianBlur(imgS, imgI2, sizeG, 50, 50, BorderType.Replicate);
            CvInvoke.GaussianBlur(imgS, imgI3, sizeG, 200, 200, BorderType.Replicate);

            imgI = imgI1.Mul(0.3) + imgI2.Mul(0.1) + imgI3.Mul(0.6);
            imgR = imgS.Log() - imgI.Log();
            imgR = imgR.Add(new Gray(5)).Max(0).Sub(new Gray(5));//imgR中有一些-8左右的值，对计算平均值和方差影响很大
            imgR = imgR.Sub(new Gray(5)).Min(0).Add(new Gray(5));//imgR中有一些-8左右的值，对计算平均值和方差影响很大
            double minval = 0, maxval = 0;
            Point minloc = default;
            Point maxloc = default;
            CvInvoke.MinMaxLoc(imgR, ref minval, ref maxval, ref minloc, ref maxloc);

            //进行映射
            MCvScalar mean = new MCvScalar();
            MCvScalar var = new MCvScalar();
            CvInvoke.MeanStdDev(imgR, ref mean, ref var);
            double mean0 = mean.V0;
            double var0 = var.V0;
            double scaleYS = 1.5;
            double l1 = mean0 - scaleYS * var0;
            double l2 = mean0 + scaleYS * var0;

            imgR = imgR.Sub(new Gray(l1));
            img2 = new Image<Gray, float>(img.Width, img.Height, new Gray(l2 - l1));
            CvInvoke.Divide(imgR, img2, imgR, 255, DepthType.Default);
            ///锐化
            imgSharp = imgR.Laplace(3);
            imgR = imgR.AddWeighted(imgSharp, 1, -0.2 * 0.5, 0);
            //imgS = imgS.Max(0);
            //imgS = imgS.Min(65535);

            imgR = imgR.Max(0);
            imgR = imgR.Min(255);
            ///直方图均衡化
            Size size = new Size(4, 4);

            imgout = imgR.Convert<Gray, byte>();
            CvInvoke.CLAHE(imgout, 10, size, imgout);
            return imgout;
        }
        
        
        /// <summary>
        /// 第四种方法，对比度很好，细节也很好
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public Image<Gray, byte> ImageEnhance4(ref Image<Gray, ushort> img)
        {
            Image<Gray, byte> imgS,imgMed,imgCha;
            Image<Gray, byte> imgout = new Image<Gray, byte>(img.Size);
            Image<Gray, byte> imgCla = new Image<Gray, byte>(img.Size);
            Image<Gray, byte> imgRes = new Image<Gray, byte>(img.Size);

            imgS = img.Convert<Gray,byte>().Mul(1.1);
            imgMed = imgS.SmoothMedian(21);
            imgCha = imgS - imgMed+imgS ;

            Size sizeC = new Size(8, 8);
            CvInvoke.CLAHE(imgCha ,10,sizeC, imgCla);
            //imgRes = imgCla.smo
            imgRes = imgCla;
            

            imgout =imgRes.Convert<Gray, byte>();
            return imgout;
        }
        
        
        /// <summary>
        /// 第五种方法，跟第四种效果其实差不多，但是略有差别
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public Image<Gray, byte> ImageEnhance5(ref Image<Gray, ushort> img)
        {
            Image<Gray, float> imgS, imgI, imgR, img2, imgI1, imgI2, imgI3, imgSharp, imgC;          //R=logS-logI
            Image<Gray, byte> imgout = new Image<Gray, byte>(img.Size);
            imgI = new Image<Gray, float>(img.Size);
            imgI1 = new Image<Gray, float>(img.Size);
            imgI2 = new Image<Gray, float>(img.Size);
            imgI3 = new Image<Gray, float>(img.Size);

            //获得R图像
            imgS = img.Convert<Gray, float>();
            //调窗
            double mindicom1 = 0, maxdicom1 = 0;
            Point minloc = default;Point maxloc = default;
            CvInvoke.MinMaxLoc(imgS, ref mindicom1, ref maxdicom1, ref minloc, ref maxloc);
            double winwidth0 = maxdicom1 - mindicom1;
            double wincenter0 = (maxdicom1 + mindicom1) / 2;
            double maxva = wincenter0 + winwidth0 / 2;
            double minva = wincenter0 - winwidth0 / 2;
            Image<Gray, ushort> imgs = img.Min(maxva);
            imgs = imgs.Max(minva);

            double ap = 1 / (maxva - minva);
            double shi = -ap * minva;
            imgS = imgs.ConvertScale<float>(ap, shi);

            imgR = imgS.Mul(255).Add(new Gray(2));


            Size size = new Size(5, 5);
            imgout = imgR.Convert<Gray, byte>();
            CvInvoke.CLAHE(imgout, 5, size, imgout);

            imgC = imgout.Convert<Gray, float>();
            imgSharp = imgC.Laplace(3);
            imgR = imgC.AddWeighted(imgSharp, 1, -0.1, 0);
            imgR = imgR.Max(0).Min(255);
            imgout = imgR.Convert<Gray, byte>();
            Mat aaa = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(2, 2));
            imgout = imgout + imgout.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Tophat, aaa, new Point(1, 1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(255, 0, 0, 255));
            return imgout;
        }

        /// <summary>
        /// 第六种方法，需要修改
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public Image<Gray, byte> ImageEnhance6(ref Image<Gray, ushort> img)
        {
            Image<Gray, float> imgS, img2,imgSharp;          //R=logS-logI
            Image<Gray, float> imgR = new Image<Gray, float>(img.Size);
            Image<Gray, float> imgC = new Image<Gray, float>(img.Size);
            Image<Gray, byte> imgout = new Image<Gray, byte>(img.Size);
            Image<Gray, byte> imgout1 = new Image<Gray, byte>(img.Size);
            Image<Gray, float> imgI = new Image<Gray, float>(img.Size);
            Image<Gray, float> imgL = new Image<Gray, float>(img.Size);
            Image<Gray, float> imgL1 = new Image<Gray, float>(img.Size);
            Image<Gray, float> imgL2 = new Image<Gray, float>(img.Size);
            


            //获得R图像
            imgS = img.Convert<Gray, float>();
            imgI = imgS.Add(new Gray(1));


            ////获得最大值
            double minval = 0, maxval = 0;
            Point minloc = default; Point maxloc = default;
            CvInvoke.MinMaxLoc(imgS, ref minval, ref maxval, ref minloc, ref maxloc);
            MCvScalar mean1 = new MCvScalar();
            MCvScalar var1 = new MCvScalar();
            CvInvoke.MeanStdDev(imgS, ref mean1, ref var1);

            //对数变换
            Image<Gray, float> imgM = new Image<Gray, float>(img.Width, img.Height, new Gray(maxval));
            CvInvoke.Divide(imgS.Add(new Gray(1)), imgM.Add(new Gray(1)), imgI,65535, DepthType.Default);

            //获得最大值
            double minvali = 0, maxvali = 0;
            Point minloci = default; Point maxloci = default;
            CvInvoke.MinMaxLoc(imgS, ref minvali, ref maxvali, ref minloci, ref maxloci);

            //灰度映射
            Image<Gray, float> imgM0 = new Image<Gray, float>(img.Width, img.Height, new Gray(maxvali-minvali ));
            CvInvoke.Divide(imgI.Sub(new Gray(minvali)), imgM0, imgR, 65535, DepthType.Default);
            //imgout = imgI.Convert<Gray, byte>();

            //梯度域算法
            double[,] arr = new double[3,3];
            for (int i = 1; i < img.Height-1; i++)
            {
                for(int j=1;j< img.Width-1;j++)
                {
                    double ave = (Convert.ToInt32(imgR.Data.GetValue(i - 1, j - 1, 0)) + Convert.ToInt32(imgR.Data.GetValue(i - 1, j , 0)) + Convert.ToInt32(imgR.Data.GetValue(i - 1, j + 1, 0)) +
                        Convert.ToInt32(imgR.Data.GetValue(i , j - 1, 0)) + Convert.ToInt32(imgR.Data.GetValue(i , j - 1, 0)) + Convert.ToInt32(imgR.Data.GetValue(i , j + 1, 0)) +
                        Convert.ToInt32(imgR.Data.GetValue(i +1, j - 1, 0)) + Convert.ToInt32(imgR.Data.GetValue(i + 1, j - 1, 0)) + Convert.ToInt32(imgR.Data.GetValue(i + 1, j + 1, 0))) / 9;

                    arr[0, 0] = 1 / (1 + Math.Abs(Convert.ToInt32(imgR.Data.GetValue(i - 1, j - 1, 0)) - ave));
                    arr[0, 1] = 1 / (1 + Math.Abs(Convert.ToInt32(imgR.Data.GetValue(i - 1, j , 0)) - ave));
                    arr[0, 2] = 1 / (1 + Math.Abs(Convert.ToInt32(imgR.Data.GetValue(i - 1, j + 1, 0)) - ave));
                    arr[1, 0] = 1 / (1 + Math.Abs(Convert.ToInt32(imgR.Data.GetValue(i , j - 1, 0)) - ave));
                    arr[1, 1] = 1 / (1 + Math.Abs(Convert.ToInt32(imgR.Data.GetValue(i , j , 0)) - ave));
                    arr[1, 2] = 1 / (1 + Math.Abs(Convert.ToInt32(imgR.Data.GetValue(i , j + 1, 0)) - ave));
                    arr[2, 0] = 1 / (1 + Math.Abs(Convert.ToInt32(imgR.Data.GetValue(i + 1, j - 1, 0)) - ave));
                    arr[2, 1] = 1 / (1 + Math.Abs(Convert.ToInt32(imgR.Data.GetValue(i + 1, j , 0)) - ave));
                    arr[2, 2] = 1 / (1 + Math.Abs(Convert.ToInt32(imgR.Data.GetValue(i + 1, j + 1, 0)) - ave));
                    double valueE = -(arr[0, 0] * Math.Log(arr[0, 0]) + arr[0, 1] * Math.Log(arr[0, 1]) + arr[0, 2] * Math.Log(arr[0, 2]) + arr[1, 0] * Math.Log(arr[1, 0]) +
                        arr[1, 1] * Math.Log(arr[1, 1]) + arr[1, 2] * Math.Log(arr[1, 2]) + arr[2, 0] * Math.Log(arr[2, 0]) + arr[2, 1] * Math.Log(arr[2, 1]) +
                        arr[2, 2] * Math.Log(arr[2, 2])) / 9;
                    //valueE *= 65535;

                    //imgout1.Data[i, j, 0] = (byte)valueE;
                    imgC.Data.SetValue((float)valueE , i, j, 0);


                }
            }

            //imgC = imgC / 65535;
            CvInvoke.Divide(imgL.Add(new Gray(1)), imgC.Add(new Gray(0.0001)), imgI, 1, DepthType.Default);
            imgI=imgI.Sub(new Gray(30)).Min(0).Add(new Gray(30));
            //获得最大值
            double minvalii = 0, maxvalii = 0;
            Point minlocii= default; Point maxlocii = default;
            CvInvoke.MinMaxLoc(imgI, ref minvalii, ref maxvalii, ref minlocii, ref maxlocii);

            //灰度映射
            Image<Gray, float> imgM0i = new Image<Gray, float>(img.Width, img.Height, new Gray(maxvalii - minvalii));
            CvInvoke.Divide(imgI.Sub(new Gray(minvalii)), imgM0i, imgI, 65535, DepthType.Default);
            //imgout = imgI.Convert<Gray, byte>();
            //Mat aaa = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(2, 2));
            //imgout1= imgout1+imgout1.MorphologyEx(Emgu.CV.CvEnum.MorphOp.Tophat, aaa, new Point(1, 1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(255, 0, 0, 255));
            imgout=imgI.Convert<Gray, byte>();
            return imgout;
        }


        /// <summary>
        /// 基于16位图像的锐化与直方图均衡化操作
        /// </summary>
        /// <param name="imgInput"></param>
        /// <returns></returns>
        public Image<Gray, ushort> ProcessImgGrayUshort16(Image<Gray, ushort> imgInput)
        {
            Image<Gray, ushort> img = imgInput;
            if (sharpRatio != 0) SharpnessAdjust(ref img, sharpRatio);
            if (isHistEqualize) HistEqualize(ref img);
            //if (isImageEnhance) ImageEnhance(ref img);
            return img;
        }

        /// <summary>
        /// 基于8位图像的b亮度，c对比度，i反色，f浮雕处理
        /// </summary>
        /// <param name="imgInput"></param>
        /// <param name="isInverse"></param>
        /// <param name="isFudiao"></param>
        /// <returns></returns>
        public Image<Bgr, byte> ProcessImgGrayUshort8(Image<Bgr, byte> imgInput, bool isInverse, bool isFudiao)
        {
            Image<Gray, byte> img = imgInput.Convert<Gray, byte>();
            Image<Bgr, byte> imgout = new Image<Bgr, byte>(img.Width, img.Height);
            if (contrastRatio != 0) ContrastAdjust(ref img);
            if (brightRatio != 0) BrightAdjust(ref img);
            if (isFudiao) Fudiao(ref img);
            if (isInverse) { Inverse(ref img); }

            imgout = img.Convert<Bgr, byte>();
            return imgout;
        }

        public void SetImageEnhance(bool arg)
        {
            isImageEnhance = arg;
            
        }
        public void ResetImgProcessArgsPartly()
        {
            brightRatio = 0; contrastRatio = 0; sharpRatio = 0;
            isFudiao = false;
            isHistEqualize = false;
        }
        public void ResetImgProcessArgsTotally()
        {
            brightRatio = 0; contrastRatio = 0; sharpRatio = 0;
            //isFudiao = false; isInverse = false; 
            isHistEqualize = false; isImageEnhance = false;
        }
        public void AdjustBrightRatio(int variation)
        {
            brightRatio = variation;
        }
        public void AdjustContrastRatio(int variation)
        {
            contrastRatio = variation;
        }

        public void AdjustSharpnessRatio(int variation)
        {
            sharpRatio += variation;
            if (sharpRatio < 0) sharpRatio = 0;
        }
        public void SwitchOnFudiao(bool arg)
        {
            isFudiao = arg;
        }
        public void SwitchOnInverse(bool arg)
        {
            isInverse = arg;
        }
        public void SwitchOnHistEqualize(bool arg)
        {
            isHistEqualize = arg;
        }
        public static Image BytesToImage(byte[] buffer)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream(buffer);
            Image image = System.Drawing.Image.FromStream(ms);
            return image;
        }



    }
}
