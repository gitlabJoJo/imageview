﻿using ImageView;
using System;
using System.Windows;
using System.Windows.Controls;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// jiaozhun.xaml 的交互逻辑
    /// </summary>
    public partial class jiaozhun : Window
    {
        public jiaozhun()
        {
            InitializeComponent();
        }
        private void jiaozhunTextbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            #region//异常处理
            if (String.IsNullOrEmpty(jiaozhunTextbox.Text.ToString()) == true)
            {                
                return;
            }
            int tmpc;
            if (!int.TryParse(jiaozhunTextbox.Text, out tmpc))
            {
                MessageBox.Show("请输入数字");
                jiaozhunTextbox.Text = null;
                return;
            }
            #endregion

            MainWindow.help = Double.Parse(jiaozhunTextbox.Text);

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
    }
}
