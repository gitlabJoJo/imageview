﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dcmview.Classes
{
    class DefectInfo
    {
        private int defectClass;
        private int x1, y1, x2, y2;
        private string text;

        public DefectInfo(int Class,int x1,int y1,int x2,int y2,string text)
        {
            defectClass = Class;
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.text = text;
        }
        public int GetDefectClass()
        {
            return defectClass;
        }
        public int Getx1()
        {
            return x1;
        }
        public int Getx2()
        {
            return x2;
        }
        public int Gety1()
        {
            return y1;
        }
        public int Gety2()
        {
            return y2;
        }
        public string GetText()
        {
            return text;
        }
    }
}
