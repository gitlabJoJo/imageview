﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// ChartGray.xaml 的交互逻辑
    /// </summary>
    public partial class ChartGray : Window
    {
        public ChartGray()
        {
            InitializeComponent();
        }
        public void InitChart(double[] datac, int left, int right)
        {
            //定义图表区域
            chartAttMed.ChartAreas.Clear();
            ChartArea chartArea1 = new ChartArea("C1");
            chartAttMed.ChartAreas.Add(chartArea1);
            string a = null;
            //for (int i = 1; i < ratec.Length; i++)
            //{
            //    shuangsi = i;
            //    shuangsilevel.Text = "D" + i.ToString();
            //    a = a + ("D" + i.ToString() + ": " + ratec[i - 1].ToString() + "%    ");
            //    if (ratec[i - 1] < 20) break;
            //    //if (ratec[i] != 0)
            //    //{
            //    //   shuangsi = j;
            //    //   shuangsilevel.Text="D"+j.ToString();
            //    //   a=a+("D"+j.ToString()+": "+ratec[i].ToString()+"%    ");
            //    //   j++;                   
            //    //}
            //}
            //chartAttMed.Titles.Add(a);

            //定义存储和显示点的容器
            chartAttMed.Series.Clear();
            Series series1 = new Series("pixelvalue");
            series1.ChartArea = "C1";
            series1.YValueType = ChartValueType.Int32;
            series1.XValueType = ChartValueType.Int32;


            double[] cut = new double[right - left + 1];
            for (int j = left; j <= right; j++)
            {
                cut[j - left] = datac[j];
            }
            //设置图表显示样式
            series1.ChartType = SeriesChartType.Line;
            chartAttMed.ChartAreas[0].AxisX.Minimum = left;
            chartAttMed.ChartAreas[0].AxisX.Maximum = right;
            chartAttMed.ChartAreas[0].AxisX.Title = "像素数";
            chartAttMed.ChartAreas[0].AxisY.Minimum = cut.Min();
            chartAttMed.ChartAreas[0].AxisY.Maximum = cut.Max();
            chartAttMed.ChartAreas[0].AxisY.Title = "像素值";

            //chartAttMed.Series[0].ChartType = SeriesChartType.Line;
            //chartAttMed.ChartAreas[0].AxisY.Interval = 1;
            chartAttMed.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartAttMed.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            //设置标题




            series1.Color = System.Drawing.Color.Red;
            for (int i = 0; i < cut.Length - 1; i++)
            {
                series1.Points.AddXY((double)(i + left), cut[i]);
            }
            chartAttMed.Series.Add(series1);

        }
        public void showGray(BitmapSource a)
        {
            Graycut.Source = a;
            //length.Text = a.PixelWidth.ToString();
            //width.Text = a.PixelHeight.ToString();
        }
    }
}
