﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using Microsoft.Win32;
using System.Threading;
using System.Diagnostics;
using Dicom;
using Dicom.Imaging;
using System.Drawing.Imaging;
namespace dcmview.Classes
{
    public class DicomObj
    {
        private DicomFile dcmfile;
        private DicomDataset dcmdateset;
        private DicomImage dcmimg;
        public DicomObj(string fileName)
        {
            dcmfile = DicomFile.Open(fileName);
            dcmdateset = dcmfile.Dataset;
            dcmimg = new DicomImage(dcmdateset);
        }
        ~DicomObj()
        {
        }
        public System.Drawing.Image Image()
        {
            return dcmimg.RenderImage();
        }
        public double WindowCenter
        {
            get
            {
                return dcmimg.WindowCenter;
            }
            set
            {
                dcmimg.WindowCenter = value;
            }
        }
        public double WindowWidth
        {
            get
            {
                return dcmimg.WindowWidth;
            }
            set
            {
                dcmimg.WindowWidth = value;
            }
        }

    }
}
