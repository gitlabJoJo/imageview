﻿using Dicom;
using Dicom.Imaging;
using Dicom.Imaging.Render;
using Dicom.IO.Buffer;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace dcmview.Classes
{
    class Utils
    {
        public static Image<Emgu.CV.Structure.Gray, ushort> DicomImage2UShortEmguImage(DicomImage dicomImage)
        {
            //var DataU16 = new GrayscalePixelDataU16(dicomImage.Width, dicomImage.Height, dicomImage.PixelData.BitDepth, dicomImage.PixelData.GetFrame(0));
            //ushort[] temp = new ushort[DataU16.Data.Length];
            //var r = new Image<Gray, ushort>(DataU16.Width, DataU16.Height);
            //unsafe
            //{
            //    ushort* ptr = (ushort*)r.Mat.DataPointer;
            //    for (int i = 0; i < temp.Length; i++)
            //    {
            //        ptr[i] = DataU16.Data[i];
            //    }
            //}
            var DataU16 = new GrayscalePixelDataU16(dicomImage.Width, dicomImage.Height, dicomImage.PixelData.BitDepth, dicomImage.PixelData.GetFrame(0));
            GCHandle d = GCHandle.Alloc(DataU16.Data, GCHandleType.Pinned);
            var r = new Image<Gray, ushort>(DataU16.Width, DataU16.Height, 2, d.AddrOfPinnedObject());
            // 释放资源
            return r;
        }

        public static async Task SaveDicomFile(ushort[] img, string path, int width,int height)
        {
            await Task.Run(() =>
            {
                byte[] byteImg = new byte[img.Length * 2]; 
                // 拷贝到byteImg
                for (int j = 0; j < img.Length; j++)
                {
                    byteImg[j * 2] = (byte)(img[j] & 255);
                    byteImg[j * 2 + 1] = (byte)(img[j] >> 8);
                }

                MemoryByteBuffer buffer = new MemoryByteBuffer(byteImg);
                DicomDataset dataset = new DicomDataset();
                string da = DateTime.Now.ToString("yyyyMMdd");
                string ti = DateTime.Now.ToString("HHmmss");
                dataset.Add(DicomTag.PhotometricInterpretation, PhotometricInterpretation.Monochrome2.Value);//PhotometricInterpretation.Monochrome2.Value
                dataset.Add(DicomTag.Rows, (ushort)height);
                dataset.Add(DicomTag.Columns, (ushort)width);
                dataset.Add(DicomTag.BitsAllocated, (ushort)16);
                dataset.Add(DicomTag.SOPClassUID, "RXray");
                dataset.Add(DicomTag.SOPInstanceUID, "RXray" + da + ti);
                dataset.Add(DicomTag.Modality, "DR");
                dataset.Add(DicomTag.WindowCenter, "32768");
                dataset.Add(DicomTag.WindowWidth, "65536");
                dataset.Add(DicomTag.AcquisitionDate, DateTime.Today);
                dataset.Add(DicomTag.AcquisitionDateTime, da + ti);
                dataset.Add(DicomTag.AcquisitionTime, ti);
                DicomPixelData pixelData = DicomPixelData.Create(dataset, true);
                pixelData.BitsStored = 16;
                pixelData.SamplesPerPixel = 1;
                pixelData.HighBit = 15;
                pixelData.PixelRepresentation = 0;
                pixelData.PlanarConfiguration = 0;
                pixelData.AddFrame(buffer);
                DicomFile dicomfile = new DicomFile(dataset);
                try
                {
                    dicomfile.Save(path);
                }
                catch (Exception)
                {
                    MessageBox.Show("请重新选择文件");
                    return;
                    //throw;
                }


            });
        }



    }
}
