﻿using ImageView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// GetShuangsiWidth.xaml 的交互逻辑
    /// </summary>
    public partial class GetShuangsiWidth : Window
    {
        bool continue1 = false;
        public GetShuangsiWidth()
        {
            InitializeComponent();
            textBox.Text = MainWindow.hi1.ToString();
            this.Closing += getshuangsiwidth_close;
        }
        private void getshuangsiwidth_close(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (continue1) { e.Cancel = false; }
            else
            {
                MainWindow.shuangsiCountinue = false;
            }
        }

        private void confirm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainWindow.hi1 = int.Parse(textBox.Text);
                MainWindow.shuangsiCountinue = true;
                continue1 = true;
                this.Close();
            }
            catch { MessageBox.Show("输入不是有效数字"); }

        }

        private void cansel_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.shuangsiCountinue = false;
            this.Close();
        }
    }
}
