﻿using ImageView;
using System.Windows;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// GetSaveString.xaml 的交互逻辑
    /// </summary>
    public partial class GetSaveString : Window
    {
        public GetSaveString()
        {
            InitializeComponent();            
            //this.Closing += getsavestring_close;
        }
        //private void getsavestring_close(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    if (continue1) { e.Cancel = false; }
        //    else
        //    {
        //        MainWindow.shuangsiCountinue = false;
        //    }
        //}
        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.SaveWords = inputtext.Text;
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {            
            this.Close();
        }
    }
}
