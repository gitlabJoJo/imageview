﻿using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;

namespace dcmview.Classes
{
    class Draw//Draw类负责所有绘图操作，接收参数包括输入图片，绘制类型，图上坐标等相关参数，请将绘图方法集成到此类中
    {       
        public void DrawLine(ref Image<Bgr, byte> Img, System.Drawing.Color color, int x1, int y1, int x2, int y2, int width = 3)
        {
            Graphics gps = System.Drawing.Graphics.FromImage(Img.Bitmap);
            gps.DrawLine(new System.Drawing.Pen(color, width), x1, y1, x2, y2);
        }
        public void DrawRectangle(ref Image<Bgr, byte> Img, System.Drawing.Color color, int x1, int y1, int recWidth, int recHeight, int width = 3)
        {
            Graphics gps = System.Drawing.Graphics.FromImage(Img.Bitmap);
            gps.DrawRectangle(new System.Drawing.Pen(color, width), x1, y1, recWidth, recHeight);
        }
        public void DrawText(ref Image<Bgr, byte> Img, System.Drawing.Color color, Font font, string texts, int x1, int y1, int width = 3)
        {
            Graphics gps = System.Drawing.Graphics.FromImage(Img.Bitmap);
            
            gps.DrawString(texts, font, new SolidBrush(color), x1, y1);
        }
        public void DrawCircle(ref Image<Bgr, byte> Img, System.Drawing.Color color, int x1, int y1,int radius, int width = 3)
        {
            Graphics gps = System.Drawing.Graphics.FromImage(Img.Bitmap);
            gps.DrawEllipse(new System.Drawing.Pen(color, width), x1-radius,y1-radius,2*radius,2*radius);         
        }

        public void DrawImage(ref Image<Bgr, byte> destImg, ref Image<Gray, byte> sourceImg, 
            System.Drawing.Rectangle rec, System.Drawing.GraphicsUnit unit,int x1,int y1)
        {
            Graphics gps = System.Drawing.Graphics.FromImage(destImg.Bitmap);
            gps.DrawImage(sourceImg.Bitmap, x1, y1, rec, unit);
        }
        public void DrawImage(ref Image<Bgr, byte> destImg, ref Image<Gray, ushort> sourceImg,
            System.Drawing.Rectangle rec, System.Drawing.GraphicsUnit unit, int x1, int y1)
        {
            Graphics gps = System.Drawing.Graphics.FromImage(destImg.Bitmap);
            gps.DrawImage(sourceImg.Bitmap, x1, y1, rec, unit);
        }
        public void DrawImage(ref Bitmap destbitmap, ref Bitmap sourcebitmap,
            System.Drawing.Rectangle rec, System.Drawing.GraphicsUnit unit, int x1, int y1)
        {
            Graphics gps = System.Drawing.Graphics.FromImage(destbitmap);
            gps.DrawImage(sourcebitmap, x1, y1, rec, unit);                     
        }
    }
}
