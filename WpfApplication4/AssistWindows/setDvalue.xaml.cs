﻿using ImageView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// setDvalue.xaml 的交互逻辑
    /// </summary>
    public partial class setDvalue : Window
    {
        bool continue1 = false;
        public setDvalue()
        {
            InitializeComponent();
            this.Closing += getshuangsiwidth_close;
        }
        private void getshuangsiwidth_close(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (continue1) { e.Cancel = false; }
            else
            {
                MainWindow.thicknessContinue = false;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (dText.Text.Length == 0)
            {
                MessageBox.Show("请输入有效的直径值");
                return;
            }
            try
            {
                MainWindow.thickns = int.Parse(dText.Text);
                MainWindow.thicknessContinue = true;
                continue1 = true;
                this.Close();
            }
            catch { MessageBox.Show("输入不是有效数字"); }
        }
    }
}
