﻿using ImageView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// SNR.xaml 的交互逻辑
    /// </summary>
    public partial class SNR : Window
    {
        public SNR()
        {
            InitializeComponent();
        }

        private void close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            string[] strs = new string[]
            { "区域大小:" + size.Text,
              "灰度平均值:" +gray_average.Text,
              "灰度标准化:" +gray_standardization.Text,
              "归一化信噪比:" +normalized_snr.Text };
            MainWindow.snrvalue = normalized_snr.Text;
            MainWindow.meangraytrans = MainWindow.meangray;
            //using (StreamWriter writer = new StreamWriter(@"C:\Users\ZX\Desktop\工艺信息.txt"))//保存目录
            //using (StreamWriter writer = new StreamWriter("工艺信息.txt"))//保存目录
            //{
            //    foreach (string s in strs)
            //    {
            //        writer.WriteLine(s);
            //    }
            //    writer.Close();
            //}
            MessageBox.Show("已保存！");
            this.Close();
        }
    }
}
