﻿using dcmview.AssistWindows;
using Dicom.Imaging;
using Dicom.Imaging.Render;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using ImageView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace dcmview.Classes
{
    class ImgQualityEvaluation//与评定图像质量相关的方法请集成到此类中
    {
        private double clctxy(int x, int n, int y, bool fang)
        {
            double txy = 0;
            double[] imgs = new double[n * 3];
            int K = 0;
            if (fang)//fang控制取点方向，以下条件语句按照选定方向给imgs逐点赋值
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int i = 0; i < n; i++)
                    {
                        imgs[K] =MainWindow.iPixelData.GetPixel(x + i, y + j);
                        K++;
                    }
                }
            }
            else
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int i = 0; i < n; i++)
                    {
                        imgs[K] =MainWindow.iPixelData.GetPixel(x + j, y + i);
                        K++;
                    }
                }
            }
            //showshuansi(imgs, n, 3);
            double[] gx = new double[n - 2];
            double[] gy = new double[n - 2];
            for (int i = 1; i < n - 1; i++)
            {
                gy[i - 1] = imgs[2 * n + i - 1] - imgs[i - 1] + 2 * (imgs[2 * n + i] - imgs[i]) + imgs[2 * n + i + 1] - imgs[i + 1];
                gx[i - 1] = imgs[i + 1] - imgs[i - 1] + 2 * (imgs[i + 1 + n] - imgs[i - 1 + n]) + imgs[i + 1 + 2 * n] - imgs[i - 1 + 2 * n];
            }
            double mx = 0.5 * gx.Max();
            K = 0;
            for (int i = 0; i < n - 2; i++)
            {
                if (gx[i] > mx)
                {
                    K++;
                    txy += gy[i] / gx[i];
                }
            }
            txy /= K;
            return txy;
        }
        public double[] GetImgCutpj(bool XforTrue, int n,int hi1,int x1,int y1,int x2,int y2)
        {
            double[] imgs;
            double txy = 0;
            int hi2 = (hi1 - 1) / 2;
            if (XforTrue) //若横轴较长，按横坐标处理
            {                
                imgs = new double[n * hi1];//截取的图
                if (x2 > x1)
                {
                    txy = (double)(y2 - y1) / (double)(x2 - x1);
                    //txy = clctxy(x1, n, (int)((y1 + y2) / 2), true); //计算比例(double)(y2 - y1) / (double)(x2 - x1)
                    int K = 0;
                    for (int j = -hi2; j <= hi2; j++) //取上下共21点的均值
                    {
                        for (int i = 0; i < n; i++) //按横坐标处理逐点取均值
                        {
                            double dy = y1 + i * txy + j;//起始纵坐标+横坐标*梯度+当前垂直偏置量
                            double dx = x1 + i - j * txy;//起始横坐标+横坐标-
                            int iy = (int)dy;
                            int ix = (int)dx;
                            double ty = dy - iy;
                            double tx = dx - ix;
                            //imgs[K] = (1 - tx) * (1 - ty) * MainWindow.iPixelData.GetPixel(ix, iy) +
                            //           (1 - tx) * ty * MainWindow.iPixelData.GetPixel(ix, iy + 1) +
                            //           (1 - ty) * tx * MainWindow.iPixelData.GetPixel(ix + 1, iy) +
                            //           tx * ty * MainWindow.iPixelData.GetPixel(ix + 1, iy + 1);
                            imgs[K] = (1 - tx) * (1 - ty) * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy, ix, 0)) +
                                       (1 - tx) * ty * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy+1, ix, 0)) +
                                       (1 - ty) * tx * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy, ix+1, 0)) +
                                       tx * ty * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy+1, ix+1, 0));
                            K++;
                        }
                    }
                }
                else
                {
                    txy = (double)(y2 - y1) / (double)(x2 - x1);
                    //txy = clctxy(x2, n, (int)((y1 + y2) / 2), true); //计算比例(double)(y2 - y1) / (double)(x2 - x1)
                    int K = 0;
                    for (int j = -hi2; j <= hi2; j++) //按横坐标处理逐点取均值
                    {
                        for (int i = 0; i < n; i++) //取上下共21点的均值
                        {
                            double dy = y1 - i * txy + j;
                            double dx = x1 - i - j * txy;
                            int iy = (int)dy;
                            int ix = (int)dx;
                            double ty = dy - iy;
                            double tx = dx - ix;
                            imgs[K] = (1 - tx) * (1 - ty) * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy, ix, 0)) +
                                       (1 - tx) * ty * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy + 1, ix, 0)) +
                                       (1 - ty) * tx * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy, ix + 1, 0)) +
                                       tx * ty * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy + 1, ix + 1, 0));
                            K++;
                        }
                    }

                }
            }
            else//若纵轴较长，按纵坐标处理
            {               
                imgs = new double[n * hi1];
                if (y2 > y1)
                {
                    txy = (double)(x2 - x1) / (double)(y2 - y1); //计算比例(double)(x2 - x1) / (double)(y2 - y1)
                    int K = 0;
                    for (int j = -hi2; j <= hi2; j++) //按横坐标处理逐点取均值
                    {
                        for (int i = 0; i < n; i++) //取上下共21点的均值
                        {
                            double dy = y1 + i - j * txy;
                            double dx = x1 + i * txy + j;
                            int iy = (int)dy;
                            int ix = (int)dx;
                            double ty = dy - iy;
                            double tx = dx - ix;
                            imgs[K] = (1 - tx) * (1 - ty) * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy, ix, 0)) +
                                       (1 - tx) * ty * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy + 1, ix, 0)) +
                                       (1 - ty) * tx * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy, ix + 1, 0)) +
                                       tx * ty * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy + 1, ix + 1, 0));
                            K++;
                        }
                    }
                }
                else
                {
                    txy = (double)(x2 - x1) / (double)(y2 - y1);//计算比例(double)(x2 - x1) / (double)(y2 - y1)
                    int K = 0;
                    for (int j = -hi2; j <= hi2; j++) //按横坐标处理逐点取均值
                    {
                        for (int i = 0; i < n; i++) //取上下共21点的均值
                        {
                            double dy = y1 - i - j * txy;
                            double dx = x1 - i * txy + j;
                            int iy = (int)dy;
                            int ix = (int)dx;
                            double ty = dy - iy;
                            double tx = dx - ix;
                            imgs[K] = (1 - tx) * (1 - ty) * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy, ix, 0)) +
                                       (1 - tx) * ty * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy + 1, ix, 0)) +
                                       (1 - ty) * tx * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy, ix + 1, 0)) +
                                       tx * ty * Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(iy + 1, ix + 1, 0));
                            K++;
                        }
                    }
                }
            }
            return imgs;
        }
        public double[] GetImgCut(bool XforTrue, int n, int hi1, int x1, int y1, int x2, int y2)
        {
            double[] imgs;
            double txy = 0;
            int hi2 = (hi1 - 1) / 2;
            if (XforTrue) //若横轴较长，按横坐标处理
            {
                imgs = new double[n * hi1];//截取的图
                if (x2 > x1)
                {
                    txy = (double)(y2 - y1) / (double)(x2 - x1); //计算比例(double)(y2 - y1) / (double)(x2 - x1)
                    int K = 0;
                    for (int j = -hi2; j <= hi2; j++) //取上下共21点的均值
                    {
                        for (int i = 0; i < n; i++) //按横坐标处理逐点取均值
                        {
                            double dy = y1 + i * txy + j;//起始纵坐标+横坐标*梯度+当前垂直偏置量
                            double dx = x1 + i - j * txy;//起始横坐标+横坐标-
                            int iy = (int)dy;
                            int ix = (int)dx;
                            double ty = dy - iy;
                            double tx = dx - ix;
                            //imgs[K] = (1 - tx) * (1 - ty) * MainWindow.iPixelData.GetPixel(ix, iy) +
                            //           (1 - tx) * ty * MainWindow.iPixelData.GetPixel(ix, iy + 1) +
                            //           (1 - ty) * tx * MainWindow.iPixelData.GetPixel(ix + 1, iy) +
                            //           tx * ty * MainWindow.iPixelData.GetPixel(ix + 1, iy + 1);
                            imgs[K] = (1 - tx) * (1 - ty) * MainWindow.iPixelData.GetPixel(ix, iy) +
                                       (1 - tx) * ty * MainWindow.iPixelData.GetPixel(ix, iy + 1) +
                                       (1 - ty) * tx * MainWindow.iPixelData.GetPixel(ix + 1, iy) +
                                       tx * ty * MainWindow.iPixelData.GetPixel(ix + 1, iy + 1);
                            K++;
                        }
                    }
                }
                else
                {
                    txy = (double)(y2 - y1) / (double)(x2 - x1); //计算比例(double)(y2 - y1) / (double)(x2 - x1)
                    int K = 0;
                    for (int j = -hi2; j <= hi2; j++) //按横坐标处理逐点取均值
                    {
                        for (int i = 0; i < n; i++) //取上下共21点的均值
                        {
                            double dy = y1 - i * txy + j;
                            double dx = x1 - i - j * txy;
                            int iy = (int)dy;
                            int ix = (int)dx;
                            double ty = dy - iy;
                            double tx = dx - ix;
                            imgs[K] = (1 - tx) * (1 - ty) * MainWindow.iPixelData.GetPixel(ix, iy) +
                                (1 - tx) * ty * MainWindow.iPixelData.GetPixel(ix, iy + 1) +
                                (1 - ty) * tx * MainWindow.iPixelData.GetPixel(ix + 1, iy) +
                                tx * ty * MainWindow.iPixelData.GetPixel(ix + 1, iy + 1);
                            K++;
                        }
                    }

                }
            }
            else//若纵轴较长，按纵坐标处理
            {
                imgs = new double[n * hi1];
                if (y2 > y1)
                {
                    txy = (double)(x2 - x1) / (double)(y2 - y1); //计算比例(double)(x2 - x1) / (double)(y2 - y1)
                    int K = 0;
                    for (int j = -hi2; j <= hi2; j++) //按横坐标处理逐点取均值
                    {
                        for (int i = 0; i < n; i++) //取上下共21点的均值
                        {
                            double dy = y1 + i - j * txy;
                            double dx = x1 + i * txy + j;
                            int iy = (int)dy;
                            int ix = (int)dx;
                            double ty = dy - iy;
                            double tx = dx - ix;
                            imgs[K] = (1 - tx) * (1 - ty) * MainWindow.iPixelData.GetPixel(ix, iy) +
                                (1 - tx) * ty * MainWindow.iPixelData.GetPixel(ix, iy + 1) +
                                (1 - ty) * tx * MainWindow.iPixelData.GetPixel(ix + 1, iy) +
                                tx * ty * MainWindow.iPixelData.GetPixel(ix + 1, iy + 1);
                            K++;
                        }
                    }
                }
                else
                {
                    txy = txy = (double)(x2 - x1) / (double)(y2 - y1); ;//计算比例(double)(x2 - x1) / (double)(y2 - y1)
                    int K = 0;
                    for (int j = -hi2; j <= hi2; j++) //按横坐标处理逐点取均值
                    {
                        for (int i = 0; i < n; i++) //取上下共21点的均值
                        {
                            double dy = y1 - i - j * txy;
                            double dx = x1 - i * txy + j;
                            int iy = (int)dy;
                            int ix = (int)dx;
                            double ty = dy - iy;
                            double tx = dx - ix;
                            imgs[K] = (1 - tx) * (1 - ty) * MainWindow.iPixelData.GetPixel(ix, iy) +
                                (1 - tx) * ty * MainWindow.iPixelData.GetPixel(ix, iy + 1) +
                                (1 - ty) * tx * MainWindow.iPixelData.GetPixel(ix + 1, iy) +
                                tx * ty * MainWindow.iPixelData.GetPixel(ix + 1, iy + 1);
                            K++;
                        }
                    }
                }
            }
            return imgs;
        }
        public void shuangsi(BitmapSource ShuangsiCut, double[] imgd, int n, int hi) //计算分辨率，n为图像长度，hi为图像宽度
        {
            int nd2 = (int)(n / 2);
            double qian = 0, hou = 0;
            for (int i = 0; i < nd2; i++)
            {
                qian += imgd[i];
                hou += imgd[i + nd2];
            }
            double[] imgs = new double[n];
            if (qian < hou) //正常顺序
            {
                for (int i = 0; i < n; i++)
                {
                    double totol = 0;
                    for (int j = 0; j < hi; j++)
                    {
                        totol += imgd[i + j * n];
                    }
                    imgs[i] = totol / hi;
                }
            }
            else
            {
                for (int i = 0; i < n; i++)
                {

                    double totol = 0;
                    for (int j = 0; j < hi; j++)
                    {
                        totol += imgd[i + j * n];
                    }
                    imgs[n - i - 1] = totol / hi;
                }
            }

            //建立差分矩阵
            double[] diff1 = new double[n - 1];
            for (int i = 0; i < n - 1; i++)
            {
                diff1[i] = imgs[i + 1] - imgs[i];
            }
            //InitChart(diff1, 0, diff1.Length - 1); 
            #region//取第一个双丝的三个峰值valley1,valley2,peak
            double[] rate = new double[13];//记录绘制图表时使用的对比度数值
            double maxm = imgs.Max();
            double minm = imgs.Min();
            if (maxm - minm < 0.005 * 65536)
            {
                //finddkTextbox.Text = "Error";
                MessageBox.Show("请选定正确的双丝区域");
                return;
            }
            double th = minm + 0.1 * (maxm - minm);
            int valley1 = 0, valley2 = 0, peak = 0;

            for (int i = 0; i < diff1.Length - 1; i++)
            {
                if (diff1[i] <= 0 && diff1[i + 1] > 0 && imgs[i + 1] < th)
                {
                    valley1 = i + 1;
                    break;
                }
            }
            for (int j = valley1; j < diff1.Length - 1; j++)
            {
                if (diff1[j] >= 0 && diff1[j + 1] < 0 && imgs[j + 1] > minm + 0.2 * (maxm - minm))
                {
                    peak = j + 1;
                    break;
                }
            }
            for (int k = peak; k < diff1.Length - 1; k++)
            {
                if (diff1[k] <= 0 && diff1[k + 1] > 0 && imgs[k + 1] < th)
                {
                    valley2 = k + 1;
                    break;
                }
            }
            double thv = 0.1 * (valley2 - valley1);
            if (thv < 5) thv = 5;
            if (Math.Abs((valley1 + valley2) / 2 - peak) > thv || valley2 - valley1 < 5 || valley1 == 0 || valley2 == 0 || peak == 0)
            {
                //finddkTextbox.Text = "Error";
                MessageBox.Show("请选定正确的双丝区域");
                return;
            }
            #endregion
            double[] distance = { 5, 5, 5, 5, 4, 4, 4, 4, 3.5, 3.5, 3.5, 3.5, 3.5, 2 };
            double[] fenb = { 800, 630, 500, 400, 320, 250, 200, 160, 130, 100, 80, 63, 50 };
            double Normal = (valley2 - valley1) / 1.6;
            int mid = peak;
            //System.Windows.Forms.MessageBox.Show(valley1.ToString() + "\n" +peak.ToString()+"\n" + valley2.ToString());
            for (int i = 0; i < distance.Length - 1; i++)//2
            {
                double LRound = distance[i] / 2 * Normal;
                double RRound = distance[i + 1] / 2 * Normal;

                //确定中心线位置
                if (i == 0) { }
                else
                {
                    mid = (int)Math.Round(mid + distance[i] * Normal);

                }

                //越界处理
                if (mid >= diff1.Length - 1)
                {
                    MessageBox.Show("截至" + "D" + i.ToString() + "满足条件" + "\n" + "已保存结果" + fenb[i - 1].ToString());
                    //fenbianTextbox.Text = fenb[i - 1].ToString();
                    MainWindow.fenbianlv = fenb[i - 1];

                    break;
                }

                //确定每一个线对的左右边界
                int leftBound, rightBound;
                leftBound = mid - (int)Math.Round(LRound);
                if (leftBound < 1) { leftBound = 1; }
                rightBound = mid + (int)Math.Round(RRound);
                if (rightBound > diff1.Length - 1) { rightBound = diff1.Length - 1; }
                //InitChart(imgs, leftBound, rightBound); 

                double[] cut = new double[rightBound - leftBound + 1];
                for (int j = leftBound; j <= rightBound; j++)
                {
                    cut[j - leftBound] = imgs[j];
                }

                double maxc = cut.Max();
                double minc = cut.Min();
                double thc = minc + 0.3 * (maxc - minc);
                int valleyc1 = 0, valleyc2 = 0, peakc = 0;

                for (int l = leftBound; l < rightBound - 1; l++)
                {
                    if (diff1[l] <= 0 && diff1[l + 1] > 0 && imgs[l + 1] < thc)
                    {
                        valleyc1 = l + 1;
                        break;
                    }
                }
                //valleyc2 = 0;
                for (int l = rightBound - 2; l > leftBound; l--)//leftBound
                {
                    if (diff1[l] <= 0 && diff1[l + 1] > 0 && imgs[l + 1] < thc)
                    {
                        valleyc2 = l + 1;
                        break;
                    }
                }
                double chaf = valleyc2 - valleyc1;
                //System.Windows.Forms.MessageBox.Show(valleyc1.ToString() + "\n" + valleyc2.ToString());
                if (chaf < 2)
                {
                    //finddkTextbox.Text = "D" + (i + 1).ToString() + "不满足条件1";
                    //fenbianTextbox.Text = fenb[i].ToString();
                    //MessageBox.Show("D" + (i + 1).ToString() + "不满足条件" + "\n" + "已保存最佳分辨率");
                    MainWindow.fenbianlv = fenb[i];
                    rate[i] = 0;
                    break;
                }
                chaf = 0.1 * chaf;
                if (chaf < 5) chaf = 5;

                //遍历区间寻找最大值
                double maxp = 0;
                for (int l = valleyc1; l < valleyc2; l++)
                {
                    if (imgs[l] > maxp)
                    {
                        maxp = imgs[l];
                        peakc = l;
                    }
                }
                rate[i] = 100 * Math.Round((maxp - minc) / (maxc - minc), 3);
                if (rate[i] < 20)
                {
                    //finddkTextbox.Text = "D" + (i + 1).ToString() + "不满足条件2";
                    //fenbianTextbox.Text = fenb[i].ToString();
                    //MessageBox.Show("D" + (i + 1).ToString() + "不满足条件" + "\n" + "已保存最佳分辨率");
                    MainWindow.fenbianlv = fenb[i];
                    break;
                }
                //test.Text = valleyc1.ToString() + "  " + peakc.ToString() + "  " + valleyc2.ToString();
                mid = peakc;
            }
            chart demo = new chart();
            demo.Title = "双丝像质计信息";
            demo.InitChart(imgs, rate, 0, n - 1);
            demo.showshuangsi(ShuangsiCut);
            demo.ShowDialog();
        }
        
        public void thicknessCalculate(BitmapSource thickCut, double[] imgd, int n, int hi, double thickns) //计算厚度，n为图像长度，hi为图像宽度
        {
            int nd2 = (int)(n / 2);
            double qian = 0, hou = 0, thickness1 = 0, thickness2 = 0, thres1 = 0.004, thres2 = 0.01;
            for (int i = 0; i < nd2; i++)
            {
                qian += imgd[i];
                hou += imgd[i + nd2];
            }
            double[] imgs = new double[n];
            if (qian > hou) //正常顺序
            {
                for (int i = 0; i < n; i++)
                {
                    double totol = 0;
                    for (int j = 0; j < hi; j++)
                    {
                        totol += imgd[i + j * n];
                    }
                    imgs[i] = totol / hi;
                }
            }
            else
            {
                for (int i = 0; i < n; i++)
                {
                    double totol = 0;
                    for (int j = 0; j < hi; j++)
                    {
                        totol += imgd[i + j * n];
                    }
                    imgs[n - i - 1] = totol / hi;
                }
            }

            double max = imgs.Max();
            double min = imgs.Min();
            double[] imgu = new double[n];
            for (int i = 0; i < n; i++)
            {
                imgu[i] = (imgs[i] - min) / (max - min);
            }
            double[] diff = new double[n - 1];
            for (int i = 1; i < n - 1; i++)
            {
                diff[i] = imgu[i + 1] - imgu[i - 1];
            }
            int[] pos = new int[4];
            for (int i = 1; i < n - 1; i++)
            {
                if (diff[i] < -thres1)
                {
                    pos[0] = i;
                    break;
                }
            }
            for (int i = pos[0] + 1; i < n - 1; i++)
            {
                if (diff[i] >= -thres2)
                {
                    pos[1] = i;
                    break;
                }
            }
            for (int i = n - 2; i > 0; i--)
            {
                if (diff[i] > thres1)
                {
                    pos[3] = i;
                    break;
                }
            }
            for (int i = pos[3] - 1; i > 0; i--)
            {
                if (diff[i] <= thres2)
                {
                    pos[2] = i;
                    break;
                }
            }            
            double thisCorect = thickns / (pos[3] - pos[0]);
            thickness1 = (pos[1] - pos[0]) * thisCorect;
            thickness2 = (pos[3] - pos[2]) * thisCorect;

            thicknessChart demo = new thicknessChart();
            demo.Title = "凹槽区域示意图";
            demo.InitChart(imgs, 0, n - 1);
            demo.showThickness(thickCut, thickness1, thickness2);
            demo.ShowDialog();
        }
        public void SNRCalculator(int x1, int y1, int x2, int y2, int width, int height,
            ref double mp, ref double sp, ref double snrn, ref int maxdicom, ref int mindicom)
        {

            //int SnrAreaWidth = 50, SnrAreaHeight = 50;
            double zp = 0, snrm;
            for (int x = x1; x < x2; x++)
            {
                for (int y = y1; y < y2; y++)
                {
                    if (y > 0 && x > 0)
                    {
                        double pxy = MainWindow.iPixelData.GetPixel(x, y);
                        //double pxy = (double)MainWindow.getGray()
                        //double pxy =MainWindow.img
                        //double pxy = (double)MainWindow.imgPinJieResult16XY.Data.GetValue(x, y, 0);
                        if (pxy > maxdicom) maxdicom = (int)pxy;
                        if (pxy < mindicom) mindicom = (int)pxy;
                        zp += pxy;
                    }

                }
            }
            mp = zp / width / height;//在图中要显示该平均灰度
            zp = 0;
            for (int x = x1; x < x2; x++)
            {
                for (int y = y1; y < y2; y++)
                {
                    if (y > 0 && x > 0)
                    {
                        //double pxy = (double)MainWindow.imgPinJieResult16XY.Data.GetValue(x, y, 0);
                        double pxy = MainWindow.iPixelData.GetPixel(x, y);
                        zp += Math.Pow(pxy - mp, 2);
                    }

                }
            }
            sp = Math.Sqrt(zp / (width * height - 1));//在图中要显示该灰度方差
            snrm = mp / sp;
            snrn = snrm * 88.6 / MainWindow.pixelsize;//在图中要显示该归一化信噪比
        }   
        public void SNRCalculatorpj(int x1, int y1, int x2, int y2, int width, int height,
            ref double mp, ref double sp, ref double snrn, ref int maxdicom, ref int mindicom)
        {
            //int SnrAreaWidth = 50, SnrAreaHeight = 50;
            double zp = 0, snrm;
            for (int x = x1; x < x2; x++)
            {
                for (int y = y1; y < y2; y++)
                {
                    if (y > 0 && x > 0)
                    {
                        double pxy = Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(y,x, 0));
                        //double pxy = MainWindow.iPixelData.GetPixel(x, y);
                        if (pxy > maxdicom) maxdicom = (int)pxy;
                        if (pxy < mindicom) mindicom = (int)pxy;
                        zp += pxy;
                    }

                }
            }
            mp = zp / width / height;//在图中要显示该平均灰度
            zp = 0;
            for (int x = x1; x < x2; x++)
            {
                for (int y = y1; y < y2; y++)
                {
                    if (y > 0 && x > 0)
                    {
                        //string s = MainWindow.imgPinJieResult16XY.Data.GetValue(x, y, 0).ToString ();
                        double pxy = Convert.ToDouble(MainWindow.imgPinJieResult16XY.Data.GetValue(y,x, 0));
                        //double pxy = MainWindow.iPixelData.GetPixel(x, y);
                        zp += Math.Pow(pxy - mp, 2);
                    }

                }
            }
            sp = Math.Sqrt(zp / (width * height - 1));//在图中要显示该灰度方差
            snrm = mp / sp;
            snrn = snrm * 88.6 / MainWindow.pixelsize;//在图中要显示该归一化信噪比       
        }
        public void FindPorosityDefect(ref DicomImage dcmImage, ref List<System.Drawing.Point> circenter,ref List<double> cirrad)
        {
            int Width = dcmImage.Width, Height = dcmImage.Height;
            VectorOfVectorOfPoint contours;
            List<int> listid;
            List<System.Drawing.Rectangle> listrect;
            VectorOfRect hierarchy = new VectorOfRect();
            bool isroate = false;
            Mat element;
            //////将dcm图像转为Image
            var DataU16 = new GrayscalePixelDataU16(Width, Height, dcmImage.PixelData.BitDepth, dcmImage.PixelData.GetFrame(0));
            GCHandle d = GCHandle.Alloc(DataU16.Data, GCHandleType.Pinned);
            var rImg = new Image<Gray, ushort>(DataU16.Width, DataU16.Height, DataU16.Width * 2, d.AddrOfPinnedObject());
            var chuimg = rImg.Copy();
            var imgb1 = new Image<Gray, byte>(Width, Height, new Gray(0));
            var imgb2 = new Image<Gray, byte>(Width, Height);
            var imgb3 = new Image<Gray, byte>(Width, Height);
            var imgm1 = new Image<Gray, ushort>(Width, Height);
            var imgm2 = new Image<Gray, ushort>(Width, Height);
            System.Drawing.Rectangle hangfengr;
            //string path0 = prePicPath.Trim() + "\\raimg.tif";
            //rImg.Save(path0);
            findhangfeng(rImg, out contours, out listid, out listrect, Width, Height);////找焊缝或管道的连通域
            if (listrect.Count == 0)////如果没找到有效连通域，将图像旋转90度后再找一次
            {
                contours.Clear();
                rImg = rImg.Rotate(90, new Gray(0), false);
                isroate = true;
                findhangfeng(rImg, out contours, out listid, out listrect, Width, Height);
            }

            if (listrect.Count == 0)////没找到有效连通域
            {
                MessageBox.Show("找不到焊缝");
                return;
            }
            else if (listrect.Count == 1)////只有一个有效连通域，该连通域即焊缝位置
            {
                //CvInvoke.DrawContours(imgb1, contours, listid[0], new MCvScalar(255), -1);
                //element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(10, 10), new System.Drawing.Point(-1, -1));
                //CvInvoke.MorphologyEx(imgb1, imgb1, MorphOp.Dilate, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
                hangfengr = new System.Drawing.Rectangle(listrect[0].X - 10, listrect[0].Y - 10, listrect[0].Width + 20, listrect[0].Height + 20);
            }
            else  ////有多个有效连通域，这是细管道图像，焊缝位置要在管道区域中再找
            {
                int j = 0;
                for (; j < listrect.Count - 2; j++)
                {
                    System.Drawing.Rectangle ri1 = listrect[j];
                    System.Drawing.Rectangle ri2 = listrect[j + 1];
                    if (ri1.Bottom < ri2.Top)////管道位置在两个相邻连通域之间，要求两个相邻连通有一定距离，且之间没有太多灰度过大的像素
                    {
                        int cw = imgb1.Cols - 20, ch = ri2.Top - ri1.Bottom;
                        System.Drawing.Rectangle ri = new System.Drawing.Rectangle(10, ri1.Bottom, cw, ch);
                        var imgcrop = new Image<Gray, ushort>(cw, ch);
                        imgcrop = rImg.Copy(ri);
                        var imgct = new Image<Gray, ushort>(cw, ch, new Gray(39321));
                        var imgbc = new Image<Gray, byte>(cw, ch);
                        CvInvoke.Compare(imgcrop, imgct, imgbc, CmpType.GreaterThan);
                        if (imgbc.GetAverage().Intensity < 25.5)
                        {
                            break;
                        }
                    }
                }
                /////将管道之间的两个相邻连通域连起来，作为管道区域
                CvInvoke.DrawContours(imgb1, contours, listid[j], new MCvScalar(255), -1);
                CvInvoke.DrawContours(imgb1, contours, listid[j + 1], new MCvScalar(255), -1);
                //Mat element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(10, 1), new System.Drawing.Point(-1, -1));
                //CvInvoke.MorphologyEx(imgb1, imgb1, MorphOp.Dilate, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
                var imgmark = new Image<Gray, byte>(Width + 2, Height + 2, new Gray(0));
                var rect = new System.Drawing.Rectangle(0, 0, 12, imgmark.Rows);
                imgmark.ROI = rect;
                imgmark.SetValue(new Gray(255));
                rect = new System.Drawing.Rectangle(imgb1.Cols - 11, 0, 12, imgmark.Rows);
                imgmark.ROI = rect;
                imgmark.SetValue(new Gray(255));
                imgmark.ROI = System.Drawing.Rectangle.Empty;
                //string path1 = prePicPath.Trim() + "\\mask.jpg";
                //imgm.Save(path1);
                //rect=System.Drawing.Rectangle.Empty;
                CvInvoke.FloodFill(imgb1, imgmark, new System.Drawing.Point(imgb1.Cols / 2, (listrect[j].Bottom + listrect[j + 1].Top) / 2), new MCvScalar(255), out rect, new MCvScalar(0), new MCvScalar(15));
                //////对图像进行膨胀运算，找出局部很黑的地方，将这些地方去掉，减少干扰
                element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(20, 10), new System.Drawing.Point(-1, -1));
                CvInvoke.MorphologyEx(rImg, imgm1, MorphOp.Dilate, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
                imgm2 = imgm1.Mul(0.88);
                CvInvoke.Compare(rImg, imgm2, imgb2, CmpType.LessThan);
                ////string path2 = prePicPath.Trim() + "\\rezhi.jpg";
                ////imgb3.Save(path2);
                //imgb2 = imgb2.And(imgb3);
                imgb2 = imgb2.And(imgb1);
                //imgb3 = imgb2.Copy();
                contours.Clear();
                CvInvoke.FindContours(imgb2, contours, hierarchy, RetrType.Ccomp, ChainApproxMethod.ChainApproxNone);
                for (int idx = 0; idx >= 0; idx = hierarchy[idx].X)
                {
                    double ai = CvInvoke.ContourArea(contours[idx]);
                    if (ai < 400)
                    {
                        CvInvoke.DrawContours(imgb2, contours, idx, new MCvScalar(0), -1);
                    }
                }
                //string path2 = prePicPath.Trim() + "\\rezhi.jpg";
                //imgb2.Save(path2);
                imgm1.Copy(chuimg, imgb2);
                //string path1 = prePicPath.Trim() + "\\raimg.tif";
                //chuimg.Save(path1);
                /////进行横向滤波，找出相对较黑的地方
                CvInvoke.Blur(chuimg, imgm1, new System.Drawing.Size(128, 1), new System.Drawing.Point(-1, -1), BorderType.Replicate);//
                imgm2 = imgm1.Mul(0.99);
                CvInvoke.Compare(chuimg, imgm2, imgb2, CmpType.LessThan);
                imgm2 = imgm1.Sub(new Gray(262));
                CvInvoke.Compare(chuimg, imgm2, imgb3, CmpType.LessThan);
                //string path2 = prePicPath.Trim() + "\\rezhi.jpg";
                //imgb3.Save(path2);
                imgb2 = imgb2.And(imgb3);
                imgb1 = imgb2.And(imgb1);
                //string path2 = prePicPath.Trim() + "\\rezhi.jpg";
                //imgb1.Save(path2);
                element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(5, 5), new System.Drawing.Point(-1, -1));
                CvInvoke.MorphologyEx(imgb1, imgb1, MorphOp.Open, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
                element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(30, 40), new System.Drawing.Point(-1, -1));
                CvInvoke.MorphologyEx(imgb1, imgb1, MorphOp.Close, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
                contours.Clear();
                hierarchy.Clear();
                CvInvoke.FindContours(imgb1, contours, hierarchy, RetrType.Ccomp, ChainApproxMethod.ChainApproxNone);
                int maxh = 0;
                hangfengr = new System.Drawing.Rectangle(0, 0, 20, 20);
                for (int idx = 0; idx >= 0; idx = hierarchy[idx].X)  //高度最大的连通域即焊缝位置
                {
                    System.Drawing.Rectangle ri = CvInvoke.BoundingRectangle(contours[idx]);
                    if (ri.Height > maxh)
                    {
                        maxh = ri.Height;
                        hangfengr = new System.Drawing.Rectangle(ri.X - 10, ri.Y - 10, ri.Width + 20, ri.Height + 20);
                    }
                }
            }

            //////////在焊缝区域中找气孔,先找出局部较亮的区域
            CvInvoke.Blur(chuimg, imgm1, new System.Drawing.Size(30, 30), new System.Drawing.Point(-1, -1), BorderType.Replicate);//
            imgm2 = imgm1.Add(new Gray(256));
            CvInvoke.Compare(chuimg, imgm2, imgb2, CmpType.GreaterThan);
            element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(3, 3), new System.Drawing.Point(-1, -1));
            CvInvoke.MorphologyEx(imgb2, imgb2, MorphOp.Close, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
            element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(5, 5), new System.Drawing.Point(-1, -1));
            CvInvoke.MorphologyEx(imgb2, imgb2, MorphOp.Open, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
            //string path2 = prePicPath.Trim() + "\\rezhi.jpg";
            //imgb2.Save(path2);
            contours.Clear();
            hierarchy.Clear();
            CvInvoke.FindContours(imgb2, contours, hierarchy, RetrType.Ccomp, ChainApproxMethod.ChainApproxNone);

            double minva = 0, maxva = 0;
            System.Drawing.Point minlo = new System.Drawing.Point(0, 0), maxlo = new System.Drawing.Point(0, 0);
            //CvInvoke.MinMaxLoc(rImg, ref minva, ref maxva, ref minlo, ref maxlo);
            double ap = 255 / (maxva - minva);
            double shi = -ap * minva;
            //var gimg = rImg.ConvertScale<byte>(ap, shi);
            //var cimg = new Image<Bgr, byte>(gimg.Width, gimg.Height);
            //CvInvoke.CvtColor(gimg, cimg, ColorConversion.Gray2Bgr);
            //CvInvoke.Rectangle(cimg, hangfengr, new MCvScalar(0, 0, 255), 3);

            for (int idx = 0; idx >= 0; idx = hierarchy[idx].X)////遍历所有连通域
            {
                RotatedRect rotatedRect = CvInvoke.MinAreaRect(contours[idx]);
                System.Drawing.Rectangle ri = CvInvoke.BoundingRectangle(contours[idx]);
                float minrad, maxrad;
                //if (rotatedRect.Size.Width>rotatedRect.Size.Height)
                //{
                //    minrad = rotatedRect.Size.Height;
                //    maxrad = rotatedRect.Size.Width;
                //}
                //else
                //{
                //    maxrad = rotatedRect.Size.Height;
                //    minrad = rotatedRect.Size.Width;
                //}
                if (ri.Width > ri.Height)
                {
                    minrad = ri.Height;
                    maxrad = ri.Width;
                }
                else
                {
                    maxrad = ri.Height;
                    minrad = ri.Width;
                }
                ////有效区域条件一：区域在焊缝区域内，且不会过长或过扁
                if (ri.X > hangfengr.X && ri.Y > hangfengr.Y && ri.Right < hangfengr.Right && ri.Bottom < hangfengr.Bottom && minrad > 6 && minrad > 0.5 * maxrad && ri.Width > 0.8 * ri.Height)//
                {
                    int c = 0;
                    float centx = rotatedRect.Center.X;
                    float centy = rotatedRect.Center.Y;
                    for (int idj = 0; idj < contours[idx].Size; idj++)////分析外轮廓的每一个点，若轮廓内部亮度比外部高15以下，则该轮廓点为无效轮廓点
                    {
                        float pointx = (float)contours[idx][idj].X;
                        float pointy = (float)contours[idx][idj].Y;
                        double wai = 0, nei = 0;
                        if (Math.Abs(pointx - centx) > Math.Abs(pointy - centy))
                        {
                            float bi = (pointy - centy) / (pointx - centx);
                            int si = Math.Sign(pointx - centx);
                            for (int i = 1; i < 4; i++)
                            {
                                int waix = contours[idx][idj].X + si * i;
                                int waiy = contours[idx][idj].Y + (int)(bi * si * i + 0.5);
                                if(waix>=0&&waix< rImg.Width )
                                {
                                    wai += rImg[waiy, waix].Intensity;
                                    int neix = contours[idx][idj].X - si * i;
                                    int neiy = contours[idx][idj].Y - (int)(bi * si * i + 0.5);
                                    nei += rImg[neiy, neix].Intensity;
                                }
                                
                            }
                        }
                        else
                        {
                            float bi = (pointx - centx) / (pointy - centy);
                            int si = Math.Sign(pointy - centy);
                            for (int i = 1; i < 4; i++)
                            {
                                int waiy = contours[idx][idj].Y + si * i;
                                int waix = contours[idx][idj].X + (int)(bi * si * i + 0.5);
                                if (waix >= 0 && waix < rImg.Width)
                                {
                                    
                                    wai += rImg[waiy, waix].Intensity;
                                    int neiy = contours[idx][idj].Y - si * i;
                                    int neix = contours[idx][idj].X - (int)(bi * si * i + 0.5);
                                    nei += rImg[neiy, neix].Intensity;
                                }
                                    
                            }
                        }
                        if ((nei - wai) / 3 < 15)
                        {
                            c++;
                        }
                    }
                    if (c <= 0.2 * contours[idx].Size)//若无效轮廓点小于总数的0.2，则认为是气孔区域
                    {
                        //int realcentx,realcenty;
                        //if (isroate)
                        //{
                        //    realcentx=(int)(centy + 0.5);
                        //    realcenty = rImg.Cols - (int)(centx + 0.5);
                        //}
                        //else
                        //{
                        //    realcentx = (int)(centx + 0.5);
                        //    realcenty = (int)(centy + 0.5);
                        //}
                        //System.Drawing.Point centi = new System.Drawing.Point(realcentx, realcenty);
                        //circenter.Add(centi);
                        //if (minrad>0.9*maxrad)
                        //{
                        //    int radi = (int)((minrad + maxrad) / 4 + 0.5);
                        //    cirrad.Add(radi);
                        //}
                        //else
                        //{
                        //    int radi = (int)(maxrad / 2 + 0.5);
                        //    cirrad.Add(radi);
                        //}
                        /////提取气孔区域子图像，找出最大最小值，并将图像映射到8位图
                        System.Drawing.Rectangle zi = new System.Drawing.Rectangle(ri.X - 10, ri.Y - 10, ri.Width + 20, ri.Height + 20);
                        //System.Drawing.Rectangle zi = new System.Drawing.Rectangle(ri.X, ri.Y, ri.Width + 20, ri.Height + 20);
                        if (zi.X < 0||zi.Y<0||zi.X> rImg.Width ||zi.Y>rImg.Height ) return;  //防止出现x<0之类的乱七八糟的情况
                        Image<Gray, ushort> ziimg= new Image<Gray, ushort> (zi.Width,zi.Height );
                        ziimg = rImg.Copy(zi);
                        CvInvoke.MinMaxLoc(ziimg, ref minva, ref maxva, ref minlo, ref maxlo);
                        ap = 255 / (maxva - minva);
                        shi = -ap * minva;
                        var gimg = ziimg.ConvertScale<byte>(ap, shi);
                        /////提取边缘
                        CvInvoke.Blur(gimg, gimg, new System.Drawing.Size(7, 7), new System.Drawing.Point(-1, -1));//
                        int param1 = 40;//, param2 = 21;
                        var eimg = gimg.Canny(param1, param1 / 2);
                        /////找出位于外轮廓附近的边缘点
                        List<System.Drawing.Point> circont = new List<System.Drawing.Point>();
                        for (int idj = 0; idj < contours[idx].Size; idj++)
                        {
                            float pointx = (float)contours[idx][idj].X;
                            float pointy = (float)contours[idx][idj].Y;
                            if (Math.Abs(pointx - centx) > Math.Abs(pointy - centy))
                            {
                                float bi = (pointy - centy) / (pointx - centx);
                                int si = Math.Sign(pointx - centx);
                                for (int i = -2; i < 6; i++)
                                {
                                    
                                    int waix = contours[idx][idj].X - zi.X + si * i;
                                    int waiy = contours[idx][idj].Y - zi.Y + (int)(bi * si * i + 0.5);
                                    if (waiy >= 0 && waiy < rImg.Height&& waix >= 0 && waix < rImg.Width) {
                                        if (eimg[waiy, waix].Intensity > 128)
                                        {
                                            circont.Add(new System.Drawing.Point(waix + zi.X, waiy + zi.Y));
                                            break;
                                        }
                                    }
                                    
                                }
                            }
                            else
                            {
                                float bi = (pointx - centx) / (pointy - centy);
                                int si = Math.Sign(pointy - centy);
                                for (int i = 1; i < 4; i++)
                                {
                                    int waiy = contours[idx][idj].Y - zi.Y + si * i;
                                    int waix = contours[idx][idj].X - zi.X + (int)(bi * si * i + 0.5);
                                    if (waiy >= 0 && waiy < rImg.Height && waix >= 0 && waix < rImg.Width)
                                    {
                                        if (eimg[waiy, waix].Intensity > 128)
                                        {
                                            circont.Add(new System.Drawing.Point(waix + zi.X, waiy + zi.Y));
                                            break;
                                        }
                                    }
                                        
                                }
                            }
                        }
                        /////对找到的边缘点进行圆拟合
                        int lc = circont.Count;
                        double x1 = 0, x2 = 0, x3 = 0, y1 = 0, y2 = 0, y3 = 0;
                        double x1y1 = 0, x2y1 = 0, x1y2 = 0;
                        for (int i = 0; i < lc; i++)
                        {
                            double x = (double)circont[i].X;
                            double y = (double)circont[i].Y;
                            x1 += x;
                            x2 += x * x;
                            x3 += x * x * x;
                            y1 += y;
                            y2 += y * y;
                            y3 += y * y * y;
                            x1y1 += x * y;
                            x2y1 += x * x * y;
                            x1y2 += x * y * y;
                        }
                        double C = lc * x2 - x1 * x1;
                        double D = lc * x1y1 - x1 * y1;
                        double E = lc * x3 + lc * x1y2 - (x2 + y2) * x1;
                        double G = lc * y2 - y1 * y1;
                        double H = lc * x2y1 + lc * y3 - (x2 + y2) * y1;
                        double a = (H * D - E * G) / (C * G - D * D);
                        double b = (H * C - E * D) / (D * D - G * C);
                        double cc = -(a * x1 + b * y1 + x2 + y2) / lc;
                        int center_x = (int)(a / (-2) + 0.5);//拟合圆中心点坐标与半径
                        int center_y = (int)(b / (-2) + 0.5);
                        double radius = Math.Sqrt(a * a + b * b - 4 * cc) / 2;
                        /////若圆心、半径符合要求则记录改圆为气孔
                        if (center_x > zi.X && center_x < zi.Right && center_y > zi.Y && center_y < zi.Bottom && radius > 3 && radius < maxrad / 2 + 10)
                        {
                            if (isroate)///若之前旋转过图像，则变换一下圆心坐标
                            {
                                int t = center_y;
                                center_y = rImg.Cols - center_x;
                                center_x = t;
                            }
                            ///circenter和cirrad为list全局变量
                            circenter.Add(new System.Drawing.Point(center_x, center_y));
                            cirrad.Add(radius);
                        }
                        //CvInvoke.Circle(cimg, new System.Drawing.Point(center_x, center_y), radius, new MCvScalar(155, 50, 255), 1);
                        //var bzimg = imgb2.Copy(zi);
                        //eimg = eimg.Or(bzimg);
                        //string pathe = prePicPath.Trim() + "\\edge" + idx.ToString() + ".jpg";
                        //eimg.Save(pathe);
                        //VectorOfPoint3D32F circles = new VectorOfPoint3D32F();
                        //CvInvoke.HoughCircles(gimg, circles, HoughType.Gradient, param1, param2, 1, 1, (int)(minrad / 2 - 2), (int)(maxrad / 2 + 5));
                        //while (circles.Size == 0 && param2 > 1)
                        //{
                        //    param2 = param2 - 5;//
                        //    CvInvoke.HoughCircles(gimg, circles, HoughType.Gradient, param1, param2, 1, 1,(int)(minrad / 2 - 2),(int)(maxrad / 2 + 5));//
                        //}
                        //for (int i = 0; i < circles.Size; i++)
                        //{
                        //    //参数定义
                        //    System.Drawing.Point center = new System.Drawing.Point((int)Math.Round(circles[i].X) + zi.X, (int)Math.Round(circles[i].Y) + zi.Y); //定义圆心
                        //    int radius = (int)Math.Round(circles[i].Z);
                        //    //绘制圆心(圆的thickness设置为-1）
                        //    CvInvoke.Circle(cimg, center, 3, new MCvScalar(0, 255, 0), -1);
                        //    //绘制圆轮廓
                        //    CvInvoke.Circle(cimg, center, radius, new MCvScalar(155, 50, 255), 3);
                        //}

                    }
                }
                //else
                //{
                //    CvInvoke.Rectangle(cimg, ri, new MCvScalar(0, 255, 0), 1);
                //    //CvInvoke.Ellipse(cimg, rotatedRect, new MCvScalar(0, 255, 0), 1);
                //}
            }
        }
        private void findhangfeng(Image<Gray, ushort> rImg, out VectorOfVectorOfPoint contours, out List<int> listid, out List<System.Drawing.Rectangle> listrect, int Width, int Height)
        {
            contours = new VectorOfVectorOfPoint();
            listid = new List<int>();
            listrect = new List<System.Drawing.Rectangle>();
            var bimg = new Image<Gray, ushort>(Width, Height);
            /////假设找焊缝或管道是水平的，进行垂直滤波，找出灰度比上下低262的点
            CvInvoke.Blur(rImg, bimg, new System.Drawing.Size(1, 128), new System.Drawing.Point(-1, -1), BorderType.Replicate);//
            var imgb1 = new Image<Gray, byte>(Width, Height);
            bimg = bimg.Sub(new Gray(262));
            //double minva=0, maxva=0;
            //System.Drawing.Point minlo = new System.Drawing.Point(0, 0), maxlo = new System.Drawing.Point(0, 0);
            //CvInvoke.MinMaxLoc(bimg, ref minva, ref maxva, ref minlo, ref maxlo);
            //double ap = 65535 / (maxva - minva);
            //bimg = bimg.Sub(new Gray(minva));
            //bimg = bimg.Mul(ap);
            //CvInvoke.MinMaxLoc(bimg, ref minva, ref maxva, ref minlo, ref maxlo);
            CvInvoke.Compare(rImg, bimg, imgb1, CmpType.LessThan);
            /////开闭运算
            Mat element;
            element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(10, 10), new System.Drawing.Point(-1, -1));
            CvInvoke.MorphologyEx(imgb1, imgb1, MorphOp.Close, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));
            element = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(15, 15), new System.Drawing.Point(-1, -1));
            CvInvoke.MorphologyEx(imgb1, imgb1, MorphOp.Open, element, new System.Drawing.Point(-1, -1), 1, BorderType.Default, new MCvScalar(0));

            //string path1 = prePicPath.Trim() + "\\buimg.tif";
            //bimg.Save(path1);
            /////找连通域轮廓
            VectorOfRect hierarchy = new VectorOfRect();
            CvInvoke.FindContours(imgb1, contours, hierarchy, RetrType.Ccomp, ChainApproxMethod.ChainApproxNone);


            //string path2 = prePicPath.Trim() + "\\rezhi.jpg";
            //imgb1.Save(path2);

            int idx = 0;
            for (; idx >= 0; idx = hierarchy[idx].X)/////遍历外轮廓
            {
                System.Drawing.Rectangle ri = CvInvoke.BoundingRectangle(contours[idx]);
                if (ri.Left < 10 && ri.Top > 10 && ri.Right > rImg.Cols - 10 && ri.Bottom < rImg.Rows - 10)/////记录有效连通域，条件横穿图像
                {
                    int j = 0;
                    for (; j < listid.Count; j++) /////对有效连通域排序，位置上排前面
                    {
                        if (ri.Top < listrect[j].Top) break;
                    }
                    listid.Insert(j, idx);
                    listrect.Insert(j, ri);
                }
            }
        }
    }
}
