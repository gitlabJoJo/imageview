﻿using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using System.Windows;

namespace dcmview.AssistWindows
{
    /// <summary>
    /// HistShowWin.xaml 的交互逻辑
    /// </summary>
    public partial class HistShowWin : Window
    {
        public HistShowWin()
        {
            InitializeComponent();

        }
        public void drawhist(Bitmap bm)
        {

            int[] hist = gethist(bm, bm.Width, bm.Height);
            histshowbox.Image = new Image<Gray, ushort>(bm);
            Graphics g = histshowbox.CreateGraphics();
            System.Drawing.Pen pen1 = new System.Drawing.Pen(System.Drawing.Color.Blue);
            g.Clear(System.Drawing.Color.White);
            //找出最大的数,进行标准化.
            int maxn = hist[0];
            for (int i = 1; i < 256; i++)
                if (maxn < hist[i])
                    maxn = hist[i];

            for (int i = 0; i < 256; i++)
                hist[i] = hist[i] * 250 / maxn;

            g.FillRectangle(new SolidBrush(System.Drawing.Color.White), 0, 0, 255, 255);

            pen1.Color = System.Drawing.Color.Red;
            for (int i = 0; i < 256; i++)
                g.DrawLine(pen1, i, 255, i, 255 - hist[i]);
            //g.DrawString("" + maxn, this.Font, new SolidBrush(Color.Blue), 0, 0);
            //label1.Location = new Point(110, 280);
            //label1.Text = "原图直方图";
        }
        public int[] gethist(Bitmap bm, int iw, int ih)
        {
            int[] h = new int[256];

            for (int j = 0; j < ih; j++)
            {
                for (int i = 0; i < iw; i++)
                {
                    int gray = (bm.GetPixel(i, j)).R;
                    h[gray]++;
                }
            }
            return h;
        }
    }
}
