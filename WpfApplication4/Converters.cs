﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

// 用于绑定的转换器
namespace Converters
{   
    public class SnrStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/snr-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/snr-3.png";
            }
            else
            {
                return "icons/snr-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class Status2BorderBg : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return new SolidColorBrush(Color.FromRgb(220, 222, 223));
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return new SolidColorBrush(Color.FromRgb(0,0,0));
            }
            else
            {
                return new SolidColorBrush(Color.FromRgb(220, 222, 223));
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class TwStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/twinwiremeasure-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/twinwiremeasure-3.png";
            }
            else
            {
                return "icons/twinwiremeasure-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class NgStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/sculpture-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/sculpture-3.png";
            }
            else
            {
                return "icons/sculpture-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ScStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/negative-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/negative-3.png";
            }
            else
            {
                return "icons/negative-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class HlaStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/biaochi-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/biaochi-3.png";
            }
            else
            {
                return "icons/biaochi-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class HlmStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/horizontallengthmeasure-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/horizontallengthmeasure-3.png";
            }
            else
            {
                return "icons/horizontallengthmeasure-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class VlaStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/verticallengthadjust-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/verticallengthadjust-3.png";
            }
            else
            {
                return "icons/verticallengthadjust-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class VlmStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/verticallengthmeasure-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/verticallengthmeasure-3.png";
                //return "icons/verticallengthmeasure_white.png";
            }
            else
            {
                return "icons/verticallengthmeasure-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ArStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/areameasure-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/areameasure-3.png";
            }
            else
            {
                return "icons/areameasure-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class AdStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/markinfo-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/markinfo-3.png";
            }
            else
            {
                return "icons/markinfo-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class SpStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/imgsplice-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/imgsplice-3.png";
            }
            else
            {
                return "icons/imgsplice-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class SdStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/showdefect-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/showdefect-3.png";
            }
            else
            {
                return "icons/showdefect-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class AwStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/AddWords-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/AddWords-3.png";
            }
            else
            {
                return "icons/AddWords-1.png";
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class AtStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/auto-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/auto-3.png";
            }
            else
            {
                return "icons/auto-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class HbStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/horizontallengthadjust-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/horizontallengthadjust-3.png";
            }
            else
            {
                return "icons/horizontallengthadjust-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class VbStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/verticalruler-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/verticalruler-3.png";
            }
            else
            {
                return "icons/verticalruler-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IH1Status2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/ImageEnhance1-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/ImageEnhance1-3.png";
            }
            else
            {
                return "icons/ImageEnhance1-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IH2Status2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/ImageEnhance2-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/ImageEnhance2-3.png";
            }
            else
            {
                return "icons/ImageEnhance2-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class IH3Status2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/ImageEnhance3-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/ImageEnhance3-3.png";
            }
            else
            {
                return "icons/ImageEnhance3-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class IH4Status2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/ImageEnhance4-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/ImageEnhance4-3.png";
            }
            else
            {
                return "icons/ImageEnhance4-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class IH5Status2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/ImageEnhance5-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/ImageEnhance5-3.png";
            }
            else
            {
                return "icons/ImageEnhance5-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class IH6Status2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/ImageEnhance6-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/ImageEnhance6-3.png";
            }
            else
            {
                return "icons/ImageEnhance6-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GmStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/graymeasure-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/graymeasure-3.png";
            }
            else
            {
                return "icons/graymeasure-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class GsStatus2ImageSource : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return "icons/ImageEnhance6-1.png";
            }
            bool isActive = (bool)value;
            if (isActive)
            {
                return "icons/ImageEnhance6-3.png";
            }
            else
            {
                return "icons/ImageEnhance6-1.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}



