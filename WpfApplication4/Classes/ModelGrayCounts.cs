﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;

namespace dcmview.Classes
{
    class ModelGrayCounts
    {
        public ModelGrayCounts()
        {
            _Value=0;
            _Counts = 0;
        }
        
        private int _Value; //1直线，2矩形
        public int Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        private int _Counts; //1直线，2矩形
        public int Counts
        {
            get { return _Counts; }
            set { _Counts = value; }
        }
    }
    
}
